unit uTransUTF;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UPasExt;

type

   { TUTF_Translator }

   TUTF_Translator = class(TPersistent)
     private
       FCodePage: integer;
       FCodes: TWordArray;
       FDoubleQuote: String;
       FFlavor: String;
       FRaiseOnMissing: boolean;
       FSingleQuote: String;
       FUnmappedValue: integer;
       procedure SetCodePage(AValue: integer);
       procedure SetCodes(AValue: TWordArray);
       procedure SetFlavor(AValue: String);
       procedure SetRaiseOnMissing(AValue: boolean);
       procedure MissingCharacter(AValue:Integer);
       procedure SetUnmappedValue(AValue: integer);
     protected
       procedure AddCodePageFlavor(AValue : integer; Flavor : String = '');
       procedure SetCodePageFlavor(AValue : integer; Flavor : String = '');
       function CharToString(C : Char) : String;
     public
       constructor Create(ACodePage : integer = 437);
       destructor Destroy; override;
       function asHTML(ACodePageText : String) : String; virtual;
       function asTEXT(AHTMLText : String) : String; virtual;
     published
       property CodePage : integer read FCodePage write SetCodePage;
       property Codes : TWordArray read FCodes write SetCodes;
       property Flavor : String read FFlavor write SetFlavor;
       property RaiseOnMissing : boolean read FRaiseOnMissing write SetRaiseOnMissing;
       property UnmappedValue : integer read FUnmappedValue write SetUnmappedValue;
       property DoubleQuote : String read FDoubleQuote;
       property SingleQuote : String read FSingleQuote;
   end;

{
var
  UTF : TUTF_Translator;
}

implementation

const
    CodePageData : String =
      'CP,437;' +
        // 20 - 7e are as is or 0020-007e in UTF codes.
        '7f,2302;' +
        '80,00c7;' +
        '81,00fc;' +
        '82,00e9;' +
        '83,00e2;' +
        '84,00e4;' +
        '85,00e0;' +
        '86,00e5;' +
        '87,00e7;' +
        '88,00ea;' +
        '89,00eb;' +
        '8a,00e8;' +
        '8b,00ef;' +
        '8c,00ee;' +
        '8d,00ec;' +
        '8e,00c4;' +
        '8f,00c5;' +
        '90,00c9;' +
        '91,00e6;' +
        '92,00c6;' +
        '93,00f4;' +
        '94,00f6;' +
        '95,00f2;' +
        '96,00fb;' +
        '97,00f9;' +
        '98,00ff;' +
        '99,00d6;' +
        '9a,00dc;' +
        '9b,00a2;' +
        '9c,00a3;' +
        '9d,00a5;' +
        '9e,20a7;' +
        '9f,0192;' +
        'a0,00e1;' +
        'a1,00ed;' +
        'a2,00f3;' +
        'a3,00fa;' +
        'a4,00f1;' +
        'a5,00d1;' +
        'a6,00aa;' +
        'a7,00ba;' +
        'a8,00bf;' +
        'a9,2310;' +
        'aa,00ac;' +
        'ab,00bd;' +
        'ac,00bc;' +
        'ad,00a1;' +
        'ae,00ab;' +
        'af,00bb;' +
        'b0,2591;' +
        'b1,2592;' +
        'b2,2593;' +
        'b3,2503;' + // light 2502, dark 2503
        'b4,252B;' + // light 2524, dark 252b
        'b5,2561;' +
        'b6,2562;' +
        'b7,2556;' +
        'b8,2555;' +
        'b9,2563;' +
        'ba,2551;' +
        'bb,2557;' +
        'bc,255d;' +
        'bd,255c;' +
        'be,255b;' +
        'bf,2513;' + // light 2510, dark 2513
        'c0,2517;' + // light 2514, dark 2517
        'c1,253B;' + // light 2534, dark 253b
        'c2,2533;' + // light 252c, dark 2533
        'c3,2523;' + // light 251c, dark 2523
        'c4,2501;' + // light 2500, dark 2501
        'c5,254b;' + // light 253c, dark 254b
        'c6,255e;' +
        'c7,255f;' +
        'c8,255a;' +
        'c9,2554;' +
        'ca,2569;' +
        'cb,2566;' +
        'cc,2560;' +
        'cd,2550;' +
        'ce,256c;' +
        'cf,2567;' +
        'd0,2568;' +
        'd1,2564;' +
        'd2,2565;' +
        'd3,2559;' +
        'd4,2558;' +
        'd5,2552;' +
        'd6,2553;' +
        'd7,256b;' +
        'd8,256a;' +
        'd9,251b;' + // light 2518, dark 251b
        'da,250f;' + // light 250c, dark 250f

      'b3,2502;' + // light 2502
      'b4,2524;' + // light 2524
      'bf,2510;' + // light 2510
      'c0,2514;' + // light 2514
      'c1,2534;' + // light 2534
      'c2,252c;' + // light 252c
      'c3,251c;' + // light 251c
      'c4,2500;' + // light 2500
      'c5,253c;' + // light 253c
      'd9,2518;' + // light 2518
      'da,250c;' + // light 250c

        'db,2588;' +
        'dc,2584;' +
        'dd,258c;' + // possibles 258c, 285d, 258e, 258f
        'de,2590;' + // possibles 2590, 2595
        'df,2580;' +
        'e0,03b1;' +
        'e1,00df;' +
        'e2,0393;' +
        'e3,03c0;' +
        'e4,03a3;' +
        'e5,03c3;' +
        'e6,00b5;' +
        'e7,03c4;' +
        'e8,03a6;' +
        'e9,0398;' +
        'ea,03a9;' +
        'eb,03b4;' +
        'ec,221e;' +
        'ed,03c6;' +
        'ee,03b5;' +
        'ef,2229;' +
        'f0,2261;' +
        'f1,00b1;' +
        'f2,2265;' +
        'f3,2264;' +
        'f4,2320;' +
        'f5,2321;' +
        'f6,00f7;' +
        'f7,2248;' +
        'f8,00b0;' +
        'f9,2219;' +
        'fa,00b7;' +
        'fb,221a;' +
        'fc,207f;' +
        'fd,00b2;' +
        'fe,25a0;' +
        'ff,00a0;' +
      'CP,437-HTML;' +
        '01,263a;' +
        '02,263b;' +
        '03,2665;' +
        '04,2666;' +
        '05,2663;' +
        '06,2660;' +
        '07,2022;' +
        '08,2fd8;' +
        '0a,25d9;' +
        '0b,2642;' +
        '0c,2640;' +
        '0d,266a;' +
        '0e,266b;' +
        '0f,263c;' +
        '10,25ba;' +
        '11,25c4;' +
        '12,2195;' +
        '13,203c;' +
        '14,00b6;' +
        '15,00a7;' +
        '16,25ac;' +
        '17,21a8;' +
        '18,2191;' +
        '19,2193;' +
        '1a,2192;' +
        '1b,2190;' +
        '1c,221f;' +
        '1d,2194;' +
        '1e,25b2;' +
        '1f,25bc;' +
        '22,0022;' +// remapped "  to easily separate text for tag innards
      'CP,437-DISPLAY;' +
        '00,0000;' + // possibles 0000, 2400
        '01,263a;' + // possibles 263a, 2401
        '02,263b;' +
        '03,2665;' +
        '04,2666;' +
        '05,2663;' +
        '06,2660;' +
        '07,2022;' +
        '08,2fd8;' +
        '09,25cb;' +
        '0a,25d9;' +
        '0b,2642;' +
        '0c,2640;' +
        '0d,266a;' +
        '0e,266b;' +  // possibles 266a, 266b
        '0f,263c;' +
        '10,25ba;' +
        '11,25c0;' +  // possibles 25c0, 25c4
        '12,2195;' +
        '13,203c;' +
        '14,00b6;' +
        '15,00a7;' +
        '16,25ac;' + // possibles 25ac, 2581, 2582, 2583
        '17,21a8;' +
        '18,2191;' +
        '19,2193;' +
        '1a,2192;' +
        '1b,2190;' +
        '1c,221f;' +
        '1d,2194;' +
        '1e,25b2;' +
        '1f,25bc;' +
        '22,0022;' + // remapped "  to easily separate text for tag innards
        '7f,2302;' + // possibles 2302, 2421
        'ff,00a0;' + // possibles 00a0, 2420, 2423
      'CP,437-ALTERNATE;' +
        '00,2400;' +
        '01,2401;' +
        '02,2402;' +
        '03,2403;' +
        '04,2404;' +
        '05,2405;' +
        '06,2406;' +
        '07,2407;' +
        '08,2408;' +
        '09,2409;' +
        '0a,240a;' +
        '0b,240b;' +
        '0c,240c;' +
        '0d,240d;' +
        '0e,240e;' +
        '0f,240f;' +
        '10,2410;' +
        '11,2411;' +
        '12,2412;' +
        '13,2413;' +
        '14,2414;' +
        '15,2415;' +
        '16,2416;' +
        '17,2417;' +
        '18,2418;' +
        '19,2419;' +
        '1a,241a;' +
        '1b,241b;' +
        '1c,241c;' +
        '1d,241d;' +
        '1e,241e;' +
        '1f,2414;' +
        '20,2420;' +
        '22,0022;' + // remapped "  to easily separate text for tag innards
        '7f,2421;' + // possibles 2302, 2421
        'ff,2424;'   // possibles 00a0, 2420, 2422, 2423, 2424, 2425
 ;

{ TUTF_Translator }


procedure TUTF_Translator.SetCodes(AValue: TWordArray);
begin
  if FCodes=AValue then Exit;
  FCodes:=AValue;
end;

procedure TUTF_Translator.SetFlavor(AValue: String);
begin
  if FFlavor=AValue then Exit;
  FFlavor:=AValue;
  SetCodePageFlavor(FCodePage, FFlavor);
end;

procedure TUTF_Translator.SetCodePage(AValue: integer);
begin
  if FCodePage=AValue then Exit;
  FCodePage:=AValue;
  SetCodePageFlavor(FCodePage, FFlavor);
end;

procedure TUTF_Translator.SetRaiseOnMissing(AValue: boolean);
begin
  if FRaiseOnMissing=AValue then Exit;
  FRaiseOnMissing:=AValue;
end;

procedure TUTF_Translator.MissingCharacter(AValue: Integer);
var
  Msg : String;
begin
  Msg :='Unmapped ASCII Character 0x' + HexStr(AValue, 2) + ', for codepage ' +
    IntToStr(CodePage);
  if FRaiseOnMissing then
    raise Exception.Create(Msg)
  else
    WriteLn(Msg);
  FCodes[AValue] := FCodes[UnmappedValue];
end;

procedure TUTF_Translator.SetUnmappedValue(AValue: integer);
begin
  if FUnmappedValue=AValue then Exit;
  FUnmappedValue:=AValue;
end;

procedure TUTF_Translator.AddCodePageFlavor(AValue: integer; Flavor: String);
var
  P, I, V, E : integer;
  X, T : String;
begin
  // if (FCodePage=AValue) then Exit;
  P := 1;
  if Flavor = '' then
    T := 'CP,' + IntToStr(AValue) + ';'
  else
    T := 'CP,' + IntToStr(AValue) + '-' + Uppercase(Flavor) + ';';
  while P < Length(CodePageData) do begin
    X := Copy(CodePageData, P, Pos(';', CodePageData, P + 1)  - P + 1);
    P := P + Length(X);
    if X = T then break;
  end;
  if P >= Length(CodePageData) then
     raise Exception.Create('unsupported code page ' + IntToStr(AValue) + ' ' + Flavor);
  FCodePage:=AValue;
  while (P < Length(CodePageData)) and (Copy(CodePageData, P, 3) <> 'CP,') do begin
    X := Copy(CodePageData, P, Pos(';', CodePageData, P + 1) - P + 1);
    P := P + Length(X);
    I := Pos(',', X);
    if (I <> 3) or (Length(X) <> 8) then
      raise Exception.Create('internal code page ' + IntToStr(AValue) + ' table error');
    Val('$' + Copy(X, 1, 2), I, E);
    if E <> 0 then
      raise Exception.Create('internal code page ' + IntToStr(AValue) + ' id ' + Copy(X, 1, 2) + ' index error');
    Val('$' + Copy(X, 4, 4), V, E);
    if E <> 0 then
      raise Exception.Create('internal code page ' + IntToStr(AValue) + ' id ' + Copy(X, 1, 2) + ' value error')
    else // if FCodes[I] = 0 then
      FCodes[I] := V;
  end;
  FDoubleQuote := CharToString('"');
  FSingleQuote := CharToString(#39);
end;

procedure TUTF_Translator.SetCodePageFlavor(AValue: integer; Flavor: String);
begin
  ClearArray(FCodes, 256);
  AddCodePageFlavor(AValue);
  if Flavor <> '' then
    AddCodePageFlavor(AValue, Flavor);
end;

function TUTF_Translator.CharToString(C: Char): String;
var
  X : integer;
begin
  X := FCodes[Ord(C)];
  if (X=0) then begin
    if (Ord(C) >= 127) then begin
      MissingCharacter(Ord(C));
      X := FCodes[UnmappedValue];
    end;
  end;
  if X = 0 then
    Result:=C
  else
    Result := '&#x' + HexStr(X, 4) + ';';
end;

constructor TUTF_Translator.Create(ACodePage: integer);
begin
  inherited Create;
  FCodePage := 0;
  FFlavor := '';
  FDoubleQuote := '"';
  FSingleQuote := #39;
  FUnmappedValue := $2622;
  FRaiseOnMissing := False;
  SetCodePage(ACodePage);
end;

destructor TUTF_Translator.Destroy;
begin
  inherited Destroy;
end;

function TUTF_Translator.asHTML(ACodePageText: String): String;
var
  I, X : integer;
  C : word;
begin
  I := 1;
  while I <= Length(ACodePageText) do begin
    X := Ord(ACodePageText[I]);
    C := FCodes[X];
    if (C=0) then begin
      if (X >= 127) then begin
        MissingCharacter(X);
        C := UnmappedValue;
      end else begin
        Inc(I);
        Continue;
      end;
    end;
    Insert('&#x' + HexStr(C, 4) + ';', ACodePageText, I);
    I := I + 8;
    Delete(ACodePageText, I, 1);
  end;
  Result := ACodePageText;
end;

function TUTF_Translator.asTEXT(AHTMLText: String): String;
const
    Terms = ';; <>&.,:/\';
var
  I, P, X, J, E : integer;
  H : word;
  S : String;
begin
  I := 1;
  while I <= Length(AHTMLText) do begin
     if AHTMLText[I] = '&' then begin
       P := Length(AHTMLText);
       for J := 1 to Length(Terms) do begin
         X := Pos(Terms[J], AHTMLText, I + 1);
         if (X > 0) and (X < P) then P := X;
       end;
       S := Copy(AHTMLText, I, P- I + 1);
       if HasLeading('&#x', S) and (Length(S)=8) and IsHex(Copy(S, 3, 5)) then begin
          Val('0'+Copy(S, 3, 5), H, E);
          if E = 0 then begin
            for J := Low(FCodes) to High(FCodes) do
              if FCodes[J] = H then begin
                Delete(AHTMLText, I, Length(S));
                Insert(Chr(J), AHTMLText, I);
                S := 'x';
                Break;
              end;
              if (S <> 'x') and (H < 256) then begin
                Delete(AHTMLText, I, Length(S));
                Insert(Chr(H), AHTMLText, I);
                S := 'x';
              end;
          end;
          Inc(I, Length(S));
       end else begin
           Inc(I, Length(S));
           // WriteLn(S);
       end;
     end else
       Inc(I);
  end;
  Result := AHTMLText;
end;

initialization

//  UTF := TUTF_Translator.Create;

finalization

//  if Assigned(UTF) then
//    FreeAndNil(UTF);

end.

