program rbil2html;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, CustApp, uParser, uPasExt;

type

  { TRBILtoHTML }

  TRBILtoHTML = class(TCustomApplication)
  private
    FParser: TRBILParser;
    procedure SetParser(AValue: TRBILParser);
  protected
    procedure DoRun; override;
    property Parser : TRBILParser read FParser write SetParser;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
    procedure WriteVersion; virtual;
  end;

{ TRBILtoHTML }

procedure TRBILtoHTML.SetParser(AValue: TRBILParser);
begin
  if FParser=AValue then Exit;
  FParser:=AValue;
end;

procedure TRBILtoHTML.DoRun;
var
  ErrorMsg: String;
  I, E : integer;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('hsovqlcwb'
    {$IFOPT D+} + 'd' {$ENDIF},
    ['help', 'src', 'out', 'verbose', 'quite', 'level', 'case',
    'words', 'base'
    {$IFOPT D+}, 'debug' {$ENDIF}]);
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  FParser:=TRBILParser.Create;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  if HasOption('v', 'verbose') then begin
    I := 2;
    if GetOptionValue('v', 'verbose') <> '' then begin
      Val(GetOptionValue('v', 'verbose'), I, E);
      if E <> 0 then I := 1;
    end;
    Verbose := I;
  end;

  if HasOption('l', 'level') then begin
    I := 2;
    if GetOptionValue('l', 'level') <> '' then begin
      Val(GetOptionValue('l', 'level'), I, E);
      if E <> 0 then I := 1;
    end;
    FParser.IndexLevel := I;
  end;

  {$IFOPT D+}
  if HasOption('d', 'debug') then
    FParser.Debug := True;
  {$ENDIF}

  if HasOption('q', 'quite') then
    Verbose:=-1;

  if HasOption('c', 'case') then
    FParser.IndexLower := True;

  if HasOption('w', 'words') then
    FParser.SaveIndex := True;

  if HasOption('b', 'base') then
    FParser.BaseURL := GetOptionValue('b', 'base');

  if HasOption('s', 'src') then
    FParser.SrcDir := GetOptionValue('s', 'src');

  if HasOption('o', 'out') then
    FParser.OutDir := GetOptionValue('o', 'out')
  else
    Fparser.OutDir := 'html';

{ add your program here }
  if Verbose >= 0 then begin
    WriteVersion;
    WriteLn('Source: ', FParser.SrcDir);
    WriteLn('Output: ', FParser.OutDir);
    WriteLn('Level ', FParser.IndexLevel, WhenTrue(FParser.IndexLower, '+') + ' indexing');
    if (FParser.BaseURL <> '') then
      WriteLn('Base URL: ', FParser.BaseURL);
  end;

  FParser.DoRun;

  if Verbose >= 0 then begin
    WriteLn('Finished');
  end;

  // stop program loop
  Terminate;
end;

constructor TRBILtoHTML.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
  FParser := TRBILParser.Create;
end;

destructor TRBILtoHTML.Destroy;
begin
  if Assigned(FParser) then FParser.Destroy;
  FParser := nil;
  inherited Destroy;
end;

procedure TRBILtoHTML.WriteHelp;
begin
  { add your help code here }
  WriteVersion;
  WriteLn;
  writeLn('Usage: ', ExtractFileName(ExeName), ' [options]');
  WriteLn;
  WriteLn(#9,'-h, --help', #9, 'Display this help text');
  WriteLn;
  WriteLn(#9,'-v, --verbose', #9, 'Increase program text display one notch');
  WriteLn(#9,'-q, --quite', #9, 'Suppress nearly all text display');
  WriteLn;
  WriteLn(#9,'-s, --src', #9, 'Specify RBIL source base directory.');
  WriteLn(#9,'-o, --out', #9, 'Specify base directory for html output.');
  WriteLn;
  WriteLn(#9,'-l, --level', #9, 'Indexing key generation thoroughness level.');
  WriteLn(#9#9#9, '(more keys - takes longer)');
  WriteLn(#9#9#9, '0 - disabled');
  WriteLn(#9#9#9, '1 - Only use config, glossary and abbr');
  WriteLn(#9#9#9, '2 - Level 1 + Title phrases (default)');
  WriteLn(#9#9#9, '3 - Level 2 + Entire text');
  WriteLn(#9#9#9, '4 - Level 3 + Disable initial pruning');
  WriteLn(#9#9#9, '5 - Level 4 + Disable min count');
  WriteLn;
  WriteLn(#9,'-c, --case', #9, 'Ignore word case for index key generation.');
  WriteLn;
  WriteLn(#9,'-b, --base', #9, 'Prefix doc tree with BASE url above RBIL docs.');
  WriteLn;
  Writeln('If you need a copy of the RBIL sources, there are many mirrors online.');
  WriteLn('However, you can still download an official copy of the Interrupt List');
  Writeln('for free from Ralf Brown' + #39 + 's webpage at Carnegie Mellon University.');
  WriteLn;
  WriteLn('http://www.cs.cmu.edu/~ralf/files.html');
  WriteLn;
end;

procedure TRBILtoHTML.WriteVersion;
begin
    WriteLn(APP_PRODUCTNAME, ' version ', APP_VERSION);
end;

var
  Application: TRBILtoHTML;

{$R *.res}

begin
  Application:=TRBILtoHTML.Create(nil);
  Application.Title:=APP_PRODUCTNAME;
  Application.Run;
  Application.Free;
end.

