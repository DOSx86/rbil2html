unit uTextToHtml;

{$mode objfpc}{$H+}

interface


uses
  Classes, SysUtils, uPasExt, uTransUTF, uDictionary;

const
  HTMLext : String = '.html';
  DOCType : String = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
//DOCTYPE : String = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
  XMLNS : String = '<html xmlns="http://www.w3.org/1999/xhtml">';
  ExternalLink : String = 'external';
  MaxAge : integer = 3600 * 24 ; { In seconds, 1 Hour * 24  = 1 day }

type

  { TTextToHTML }

  { TTextToHTML should never be used on HTML files not created and maintained soley
    by the TTextToHTML! Using this class on HTML files created or modified by other
    utilities or methods could damage the HTML file. Or at a minimum, doing so
    may produce unpredictable results. }

  TTextFunction = function (ARel, AStr : String) : String of object;

  TTextToHTML = class(TPersistent)
    private
      FCSS: String;
      FEmbedCSS: boolean;
      FNoCache: boolean;
      FUTF: TUTF_Translator;
      procedure SetCSS(AValue: String);
      procedure SetEmbedCSS(AValue: boolean);
      procedure SetNoCache(AValue: boolean);
      procedure SetUTF(AValue: TUTF_Translator);

    protected
      procedure GroupCollapse(var S : TStringList; AStart, AEnd : String; AOrigin : integer = 1); virtual;
    public
      constructor Create;
      destructor Destroy; override;
    published
      property UTF : TUTF_Translator read FUTF write SetUTF;
      property NoCache : boolean read FNoCache write SetNoCache;
      property EmbedCSS : boolean read FEmbedCSS write SetEmbedCSS;
      property CSS : String read FCSS write SetCSS;

      function asHTML(AString : String) : String; virtual; overload;
      function asURL(AString : String) : String; virtual; overload;
      function asText(AString : String; asPre : boolean = false) : String; virtual; overload;

      function Preprocess(S : String) : String; virtual; overload;
      procedure Preprocess(var S : TStringList; AOrigin : integer = 0); virtual; overload;
      procedure UnWordWrap(var S : TStringList; AMethod : integer; AOrigin : integer); virtual; overload;
      procedure UnWordWrap(var S : TStringList; AMethod : integer = 1); virtual; overload;
      procedure RemoveEmptyLines(var S : TStringList; AOrigin : integer = 0); virtual;

      function BeginTag(S, AClass : String; ATag, AAttribs : String) : String; virtual; overload;
      function BeginTag(S, AClass : String; ATag : String = 'span') : String; virtual; overload;
      function EndTag(S : String; ATag : String = 'span'):String; virtual;

      function Tag(S, AClass : String; ATag, AAttribs : String):String; virtual; overload;
      function Tag(S, AClass : String; ATag : String = 'span'):String; virtual; overload;

      // Need updated eventualy to ignore text inside of tags < ... >, eventually
      procedure TagAll(var S : TStringList; AOrigin : integer = 0); virtual; overload;
      procedure TagGroup(var S : TStringList; AStart, AEnd, AClass, ATag : String; ANest : boolean; AOrigin : integer = 0 ); virtual; overload;
      procedure TagGroup(var S : TStringList; AStart, AEnd, AClass, ATag : String; AOrigin : integer = 0 ); virtual; overload;
      procedure TagEmail(var S : TStringList; AClass, ATag : String; AOrigin : integer = 0 ); virtual; overload;
      procedure TagURL(var S : TStringList; APrefix, AClass, ATag : String; AOrigin : integer = 0 ); virtual; overload;
      procedure TagHighlight(var S : TStringList; AClass, ATag : String; AOrigin : integer = 0 ); virtual; overload;
      procedure TagDictionary(var S : TStringList; var D : TDictionary; AClass, ATag : String; AOrigin : integer = 0 ); virtual; overload;

      function CreateHTML(AFileName, Title, BodyClass : String; ACanExist : boolean = false):boolean; virtual;
      function InsertBefore(AFileName, AWith, ASeparator, ATag : String; AMustExist : boolean = true): boolean; virtual; overload;

      function AppendHTML(AFileName, AWith, ASeparator : String; AMustExist : boolean = true): boolean; virtual; overload;
      function PrependHTML(AFileName, AWith, ASeparator : String; AMustExist : boolean; AAfterID : String): boolean; virtual; overload;
      function PrependHTML(AFileName, AWith, ASeparator : String; AMustExist : boolean = true): boolean; virtual; overload;
      function AddHead(AFileName, AWith : String; AMustExist : boolean = true): boolean; virtual; overload;
      function AddHeader(AFileName, AHeader : String; AMustExist : boolean = true): boolean; virtual; overload;
      function AddMenu(AFileName, AMenu : String; AMustExist : boolean = true): boolean; virtual; overload;
      function AddFooter(AFileName, AFooter : String; AMustExist : boolean = true): boolean; virtual; overload;

      function FetchComment(AFileName, ACommentID : String; AMustExist : boolean = true) : string; virtual;
      function Comment(AComment : String) : String; overload;
      function Comment(ACommentID, AComment : String) : String; overload;
      procedure ForText (ARel : String; var S: TStringList; AFunc : TTextFunction; const AIgnoreTag, AIgnoreClass : TStringArray); overload; virtual;
      procedure ForText (ARel : String; var S: TStringList; AFunc : TTextFunction); overload; virtual;
  end;

  var
    TextToHTML : TTextToHTML;

implementation

const
  HeaderID = '<div class="page-header">';
  FooterID = '<div class="page-footer">';
  MenuID   = '<div class="page-menu">';

const
   HTMLTrans : array of record
    F, T : String;
  end = (
    (F:'&'; T:'&amp;'),
    (F:'<'; T:'&lt;'),
    (F:'>'; T:'&gt;'),
    (F:'(c)'; T:'&copy;'),
    (F:'(C)'; T:'&copy;'),
    (F:'(tm)'; T:'&trade;'),
    (F:'(TM)'; T:'&trade;'),
    (F:'(r)'; T:'&reg;'),
    (F:'(R)'; T:'&reg;'),
    (F:'(SM)'; T:'&#x2120;'),
    (F:'(P)'; T:'&#x2117;')
//    (F:'|'; T:'&copy;')

  );

   HTMLTransExtra : array of record
    F, T : String;
  end = (
    (F:' '; T:'&nbsp;')
  );

{ TTextToHTML }

procedure TTextToHTML.SetUTF(AValue: TUTF_Translator);
begin
  if FUTF=AValue then Exit;
  FUTF:=AValue;
end;

procedure TTextToHTML.SetEmbedCSS(AValue: boolean);
begin
  if FEmbedCSS=AValue then Exit;
  FEmbedCSS:=AValue;
end;

procedure TTextToHTML.SetCSS(AValue: String);
begin
  if FCSS=AValue then Exit;
  FCSS:=ExpandFileName(AValue);
end;

procedure TTextToHTML.SetNoCache(AValue: boolean);
begin
  if FNoCache=AValue then Exit;
  FNoCache:=AValue;
end;

procedure TTextToHTML.GroupCollapse(var S: TStringList; AStart, AEnd: String; AOrigin : integer);
var
  Line, NextLine : integer;
  B, E, P : integer;
begin
  NextLine := AOrigin;
  while NextLine < S.Count do begin
    Line := NextLine;
    Inc(NextLine);
    if trim(S[Line]) = '' then Continue;
    B := 1;
    repeat
      B := Pos(AStart, S[Line], B);
      if B > 0 then begin
        B := B + Length(AStart);
        P := B;
        repeat
          E := Pos(AEnd, S[Line], P);
          if E < 1 then begin
            P := P + Length(S[Line]) - Length(AEnd);
            if P < 1 then P := 1;
            if NextLine < S.Count then begin
               if trim(S[NextLine]) = '' then break;
               // WriteLn(S[NextLine]);
               S[Line] := S[Line] + #32 + trim(S[NextLine]);
               S.Delete(NextLine);
            end else Exit;
          end;

        until E > 0;
        B := B  + Length(AEnd);
      end;
    until B < 1;
  end;

end;

constructor TTextToHTML.Create;
begin
  inherited Create;
  FUTF := TUTF_Translator.Create(437);
  FUTF.Flavor := 'HTML';
  FNoCache := false;
  FEmbedCSS := false;
end;

destructor TTextToHTML.Destroy;
begin
  inherited Destroy;
end;

function TTextToHTML.asHTML(AString: String): String;
begin
  Result := UTF.asHTML(AString);
end;

function TTextToHTML.asURL(AString: String): String;
var
  I : integer;
begin
  I := 1;
  While I < Length(AString) do begin
    if InRange(Uppercase(AString[I]), 'A', 'Z') or InRange(AString[I], '0', '9') or
    (Pos(AString[I], '$-_.+!*()' + #$27) > 0) then
      Inc(I)
    else begin
      Insert('%' + HexStr(Ord(AString[I]), 2), AString, I);
      Inc(I, 3);
      Delete(AString, I, 1);
    end;
  end;
  Result := AString;
end;

function TTextToHTML.asText(AString: String; asPre : boolean = false): String;
var
  I : integer;
begin
  AString := UTF.asText(AString);
  for I := Low(HTMLTrans) to High(HTMLTrans) do
    AString := ReplaceAll(HTMLtrans[I].T, HTMLtrans[I].F, AString);
  for I := Low(HTMLTransExtra) to High(HTMLTransExtra) do
    AString := ReplaceAll(HTMLtransExtra[I].T, HTMLtransExtra[I].F, AString);
  if not AsPre then begin
    AString := LTrim(ReplaceAll(LF, ' ', ReplaceAll(CR, '', AString)));
    repeat
      I := Length(AString);
      AString := ReplaceAll('  ', ' ', AString);
    until Length(AString) = I;
  end;
  Result := AString;
end;

function TTextToHTML.Preprocess(S: String): String;
var
  I, P, N : integer;
begin
  for I := Low(HTMLTrans) to High(HTMLTrans) do begin
    N := 1;
    while N <= Length(S) do begin
      P := Pos(HTMLTrans[I].F, S, N);
      if P <= 0 then
        N := Length(S) + 1
      else begin
        Delete(S, P, Length(HTMLTrans[I].F));
        Insert(HTMLTrans[I].T, S, P);
        N := P + Length(HTMLTrans[I].T) + 1;
      end;
    end;

  end;
  Result := S;
end;

procedure TTextToHTML.Preprocess(var S: TStringList; AOrigin: integer);
begin
  While AOrigin < S.Count do begin
    S[AOrigin] := Preprocess(S[AOrigin]);
    Inc(AOrigin);
  end;
end;

procedure TTextToHTML.UnWordWrap(var S: TStringList; AMethod : integer; AOrigin: integer);
var
  Indent, LastIndent : integer;
  NextLine, Line : integer;
  TE : boolean;
begin
  // Compact standard types of indented paragraphs
  TE := False;
  if InRange(ABS(AMethod), 10, 19) then begin
    TE := True;
    AMethod := (ABS(AMethod) - 10) * (AMethod div ABS(AMethod));
  end;

  LastIndent:=-1;
  NextLine := AOrigin;
  While NextLine < S.Count do begin
    Line := NextLine;
    Inc(NextLine);
    if Trim(S[Line]) = '' then begin
      LastIndent:=-1;
      Continue;
    end;
    while (Line < S.Count) and (trim(S[Line]) <> '') do begin
      if TE then S[Line] := TabExpand(S[Line]);
      if ABS(AMethod) <> 99 then begin
        Indent:=0;
        while (Indent<Length(S[Line])) and (Pos(S[Line][Indent+1], SPACE + TAB)>0) do
          Inc(Indent);
        case AMethod of
          0 : if (LastIndent <> Indent) then break;
          1 : if (LastIndent - 1 <> Indent) then break;
          2 : if (LastIndent - 1 < Indent)  then break;
          3 : if (LastIndent < Indent)  then break;
          4 : if (LastIndent <= Indent)  then break;
          -1 : if (LastIndent + 1 <> Indent) then break;
          -2 : if (LastIndent + 1 > Indent)  then break;
          -3 : if (LastIndent > Indent)  then break;
          -4 : if (LastIndent >= Indent)  then break;
        end;
      end else if (trim(S[Line-1]) = '' ) then break;
      if Line = AOrigin then Break;
      S[Line - 1] := S[Line - 1] + #32 + Trim(S[Line]);
      S.Delete(Line);
    end;
    if (Line >= S.Count) or (trim(S[Line]) = '') then begin
      LastIndent:=-1;
      Continue;
    end;
    LastIndent:=Indent;
  end;
  // Collapse Lines ending in Hyphen
  NextLine := AOrigin;
  While NextLine < S.Count - 1 do begin
    Line := NextLine;
    Inc(NextLine);
    if Trim(S[Line]) = '' then Continue;
    while (NextLine < S.Count - 1) and (S[Line][Length(S[Line])] = '-') and
    (Trim(S[NextLine]) <> '') do begin
      S[Line] := S[Line] + Trim(S[NextLine]);
      S.Delete(NextLine);
    end;
  end;
  // Collapse Text Groups () {} [] "" ''
  GroupCollapse(S, '(', ')', AOrigin);
  GroupCollapse(S, '{', '}', AOrigin);
  GroupCollapse(S, '[', ']', AOrigin);
  GroupCollapse(S, '"', '"', AOrigin);
  GroupCollapse(S, UTF.DoubleQuote, UTF.DoubleQuote, AOrigin);
end;

procedure TTextToHTML.UnWordWrap(var S: TStringList; AMethod : integer);
begin
  UnWordWrap(S, AMethod, 0);
end;

procedure TTextToHTML.RemoveEmptyLines(var S: TStringList; AOrigin: integer);
begin
  while AOrigin < S.Count do
    if Trim(S.Strings[AOrigin]) = '' then
      S.Delete(AOrigin)
    else
      Inc(AOrigin);
end;

function TTextToHTML.BeginTag(S, AClass: String; ATag, AAttribs: String
  ): String;
begin
  if AClass <> '' then AClass := ' class="' + AClass + '"';
  if AAttribs <> '' then AAttribs := ' ' + AAttribs;
  Result := '<' + ATag + AClass + AAttribs +'>' + S
end;

function TTextToHTML.BeginTag(S, AClass: String; ATag: String): String;
begin
   Result := BeginTag(S, AClass, ATag, '');
end;

function TTextToHTML.EndTag(S: String; ATag: String): String;
begin
  Result := S + '</' + ATag + '>';
end;

function TTextToHTML.Tag(S, AClass: String; ATag, AAttribs: String): String;
begin
  Result := EndTag(BeginTag(S, AClass, ATag, AAttribs), ATag);
end;

function TTextToHTML.Tag(S, AClass: String; ATag: String): String;
begin
  Result := EndTag(BeginTag(S, AClass, ATag), ATag);
end;

procedure TTextToHTML.TagAll(var S: TStringList; AOrigin: integer);
begin
  TagHighlight(S, 'highlight', 'span', AOrigin);
  TagGroup(S, '(', ')', 'parenthesis', 'span', true, AOrigin);
  TagGroup(S, '[', ']', 'bracket', 'span', true, AOrigin);
  TagGroup(S, '{', '}', 'brace', 'span', true, AOrigin);
  TagGroup(S, UTF.DoubleQuote, UTF.DoubleQuote, 'quote', 'span', AOrigin);
  // TagGroup(S, UTF.SingleQuote, UTF.SingleQuote, 'single-quote', 'span', AOrigin);
  TagEmail(S, 'email', 'a', AOrigin);
  TagURL(S, 'ftp://', ExternalLink, 'a', AOrigin);
  TagURL(S, 'http://', ExternalLink, 'a', AOrigin);
  TagURL(S, 'https://', ExternalLink, 'a', AOrigin);
  TagURL(S, 'www.', ExternalLink, 'a', AOrigin);
  TagURL(S, 'ftp.', ExternalLink, 'a', AOrigin);
end;

procedure TTextToHTML.TagGroup(var S: TStringList; AStart, AEnd, AClass,
  ATag: String; ANest: boolean; AOrigin: integer);
var
  I, B, E, C, P : integer;
  W, T :String;
begin
  I := AOrigin;
  while I < S.Count do begin
    B := 1;
    W := S[I];
    repeat
      B := Pos(AStart, W, B);
      if B > 0 then begin
         // Don't parenthesis tag 3-digit phone number prefix
         if (lowercase(AClass) = 'parenthesis') and (pos(AEnd, W, B) = B + 4) then begin

            T := FilterExclude(Copy(W, B + 1, 20), SPACE + '-()');
            T := Copy(T, 1, 10);
            if (Length(T) = 10) and IsNumber(T) then begin
              Inc(B);
              Continue;
            end;
         end;

         T := '<' + ATag + ' class="' + AClass + '">';
         Insert(T,W,B);
         S[I]:=W;
         B:= B + Length(T);
         C := 1;
         repeat
           E := Pos(AEnd, W, B + 1);
           P := B;
           while ANest and (P > 0) and (P <= Length(W)) and ((P < E) or (E < 1)) do begin
             P := Pos(AStart, W, P + 1);
             if (P > 0) and ((P < E) or (E < 1)) then Inc(C);
           end;
           if E > 0 then begin
              Dec(C);
              if C > 0 then begin
                B := E + Length(AEnd) - 1;
                Continue;
              end;
              E := E + Length(AEnd);
              T := '</' + ATag + '>';
              if E <= Length(W) then
                Insert(T,W,E)
              else
                W := W + T;

              S[I]:=W;
              B:= E + Length(T) - 1;
           end else begin
             if I = S.Count - 1 then begin
               Exit
             end else begin
               B:= 0;
               Inc(I);
               W:=S[I];
             end;
           end;
         until C = 0;
      end;
    until B < 1;
    Inc(I);
  end;
end;

procedure TTextToHTML.TagGroup(var S: TStringList; AStart, AEnd,
  AClass, ATag: String; AOrigin: integer);
begin
  TagGroup(S, AStart, AEnd, ACLass, ATag, False, AOrigin);
end;

procedure TTextToHTML.TagEmail(var S: TStringList; AClass, ATag: String;
  AOrigin: integer);
var
  I, P, B, E : integer;
  W, X : String;
begin
  for I := AOrigin to S.Count - 1 do begin
     W := S[I];
     P := 1;
     repeat
       P := Pos('@', W, P);
       if P > 1 then begin
         B := P;
         E := P;
         While (B > 1) and (
           InRange(UpperCase(W[B-1]), 'A', 'Z') or
           InRange(UpperCase(W[B-1]), '0', '9') or
           (Pos(W[B-1], '._-+%=~!') > 0 )
           ) do dec(B);
         While (E < Length(W)) and (
           InRange(UpperCase(W[E+1]), 'A', 'Z') or
           InRange(UpperCase(W[E+1]), '0', '9') or
           (Pos(W[E+1], '._-+%=~!') > 0 )
           ) do Inc(E);
         if W[E] = '.' then Dec(E);
         if (B=P) or (E=P) then begin
           inc(P);
           Continue;
         end;
         if Pos('.', Copy(W, P, E-P+1)) < 1 then begin
           inc(p);
           continue;
         end;
         X := Copy(W, B, E-B+1);
         if (LastPos(HtmlExt, lowercase(X)) = Length(X) - 4) then begin
           inc(p);
           continue;
         end;
         X := '<' + ATag + ' class="' + AClass + '" href="mailto:' + X + '">' +  X + '</' + ATag + '>';
         Insert(X, W, B);
         P := B+Length(X);
         Delete(W, P, E-B+1);
         S[I]:=W;
       end;
     until P < 2;
  end;
end;

procedure TTextToHTML.TagURL(var S: TStringList; APrefix, AClass, ATag: String;
  AOrigin: integer);
var
  I, P, L, T, E : integer;
  W, X, Target : String;
begin
  for I := AOrigin to S.Count - 1 do begin
    W := S.Strings[I];
    P := 1;
    repeat
      P := Pos(Uppercase(APrefix), Uppercase(W), P);
      if P > 0 then begin
        L := Pos(' ', W, P);
        T := Pos(#9, W, P);
        if (T > 0) and (T<L) then L:=T;
        if L < 1 then L := length(W) + 1;
        if W[L-1] = '.' then dec(L);
        for E := P to L do
           if (not InRange(upcase(W[E]), 'A', 'Z')) and (not InRange(W[E], '0', '9'))
           and (pos(W[E], ':./%_&-+=!~') < 1) then break;
        L:=E-P;
        X := Copy(W, P, L);
        if uppercase(Copy(X, 1,4)) = 'WWW.' then begin
          if (P < 3) or (copy(W,P-2,2) <> '//') then
            X := 'http://' + X
          else begin
            Inc(P);
            Continue;
          end;
        end;
        if uppercase(Copy(X, 1,4)) = 'FTP.' then begin
          if (P < 3) or (copy(W,P-2,2) <> '//') then
            X := 'ftp://' + X
          else begin
            Inc(P);
            Continue;
          end;
        end;
        if AClass = ExternalLink then Target := ' target="_blank"' else Target := '';
        X := '<' + ATag + ' class="' + AClass + '" href="' + X + '"' + Target + '>' +  X + '</' + ATag + '>';
        Insert(X, W, P);
        P:=P+Length(X);
        Delete(W, P, L);
        S.Strings[I]:=W;
      end;
    until P < 1;

  end;

end;

procedure TTextToHTML.TagHighlight(var S: TStringList; AClass, ATag: String;
  AOrigin: integer);
var
  I, P, L : integer;
  W, X : String;
begin
  for I := AOrigin to S.Count - 1 do begin
     W := S.Strings[I];
     P := 1;
     repeat
       P := Pos('_', W, P);
       if P > 0 then begin
         if (P = 1) or (W[P-1] = ' ')or (W[P-1] = #9) then begin
           L := 1;
           while (P + L <= Length(W)) do begin
             if Copy(W, P+L, Length(UTF.DoubleQuote)) = UTF.DoubleQuote then begin
               L := L + Length(UTF.DoubleQuote);
               while (P + L < Length(W)) and (Copy(W, P+L, Length(UTF.DoubleQuote)) <> UTF.DoubleQuote) do
                 inc(L);
               L := L + Length(UTF.DoubleQuote);
               continue;
             end;
             if W[P+L] = ' ' then break;
             if (Pos(W[P+L], '.,:;') >0) and
               ((P+L+1 >Length(W)) or (W[P+L+1] = ' ')) then begin
                  break;
               end;
             Inc(L);
           end;
           X := Copy(W, P, L);
           if (L < 3) or (X[Length(X)] <> '_') then begin
             if (Length(X) - Length( replaceAll('_', '', X)) < 2) then begin
               // Must have at least 2 _ characters in string
               Inc(P);
               continue;
             end;
           end;
           X := TextToHtml.Tag(trim(replaceAll('_', ' ', X)), AClass, ATag);
           Insert(X, W, P);
           P := P + Length(X);
           Delete(W, P, L);
           S.Strings[I]:=W;
         end else
           Inc(P);
       end;
     until P <1;
  end;

end;

procedure TTextToHTML.TagDictionary(var S: TStringList; var D: TDictionary;
  AClass, ATag: String; AOrigin: integer);
var
  I, P, N, C : integer;
  W, X, H : String;
begin
  for I := AOrigin to S.Count - 1 do begin
    W := S.Strings[I];
    P := 1;
    if Trim(W) = '' then continue;
    repeat
      if W[P] = '<' then begin
        while (P < Length(W)) and (W[P] <> '>') do Inc(P);
        Inc(P);
        continue;
      end;
      if (Not (IsAlphaNum(W[P]) or (Pos(W[P], D.ExtraWordChars) > 0))) then begin
        Inc(P);
        continue;
      end;
      X := '';
      for C := D.NameMaxWords downto 1 do begin
        X := GetWord(W, C, P, D.ExtraWordChars);
        if CountWords(X, D.ExtraWordChars) <> C then continue;
        // WriteLn(C, ' "', X, '"');
        N := D.Find(X);
        if (N > -1) and (D.Names[N] = X) then begin
          // WriteLn(C, '/', CountWords(X), ':',N,':' , X, '/', D.Names[N]);
          H:=TextToHtml.Tag(X + TextToHtml.Tag(D.Values[N], AClass + '-hint', ATag), AClass, ATag);
          Insert(H, W, P);
          P := P + Length(H)-1;
          Delete(W, P+1, Length(X));
          X := '';
          Break;
        end;
      end;
      if X <> '' then P := P + Length(X) else Inc(P);
    until P >= Length(W);
    S.Strings[I]:=W;
  end;
end;

function TTextToHTML.CreateHTML(AFileName, Title, BodyClass : String; ACanExist: boolean): boolean;
var
  S, C, LCSS : String;
begin
  Result := False;
  AFileName := ExpandFileName(AFileName);
  S := IncludeTrailingPathDelimiter(ExtractFilePath(AFileName));
  if not DirectoryExists(S) then begin
    if not CreateDir(S) then
      raise Exception.Create('Unable to create sub-directory "' + S + '"')
  end;
  if FileExists(AFileName) then begin
    if Not ACanExist then
      raise Exception.Create('Unable to create existing file "' + AFileName + '"')
    else
      Result := True;
  end;
  S := DocType +
    CRLF + '<html>' +
    CRLF + '  <head>' +
    CRLF + '    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />' +
    CRLF + '    <meta content="width=device-width, initial-scale=1" name="viewport" />' +
    CRLF + '    <meta http-equiv="cache-control" content="max-age=' + IntToStr(MaxAge) + '" />';
  if FNoCache then
    S := S + CRLF + '<meta http-equiv="Cache-Control" content="no-store" />';

  if FCSS <> '' then
    LCSS := MakeRelative( AFileName, FCSS )
  else
    LCSS := '';

  if (LCSS <> '') and (not FEmbedCSS) then S := S +
    CRLF + '    <link href="' + LCSS + '" rel="stylesheet" type="text/css" />';

  if Title <> '' then S := S +
    CRLF + '    <title>' + Title+ '</title>';
  if (CSS <> '') and (FEmbedCSS) then begin
    C := '';
    LoadFromFile(ExtractFilePath(AFileName) + LCSS, C);
    S := S  + CRLF + '<style>' + CRLF + C + '</style>' ;
  end;
  S := S + CRLF + '  </head>';
  if BodyClass <> '' then
    S := S + CRLF + '  <body class="' + BodyClass + '">'
  else
    S := S + CRLF + '  <body>';
  S := S +
    CRLF + '  </body>' +
    CRLF + '</html>';

  SaveAsFile(AFileName, S);
  Result := True;
end;

function TTextToHTML.InsertBefore(AFileName, AWith, ASeparator, ATag: String;
  AMustExist: boolean): boolean;
var
  S : TStringList;
  I, H : integer;
begin
  result := false;
  ATag := Trim(Lowercase(ATag));
  if not FileExists(AFileName) then begin
    if AMustExist then
      raise Exception.Create('file ' + AFileName + ' not found')
    else
      if Not CreateHTML(AFileName, '', '', False) then
        Exit;
  end;
  S := TStringList.Create;
  S.LoadFromFile(AFileName, tEncoding.UTF8);
  for I := S.Count - 1 downto 0 do
     if Pos(ATag, Lowercase(S[I]))> 0 then
       break;
  for H := I downto 0 do
     if Pos(FooterID, Lowercase(S[H])) > 0 then
       break;
  if (H > 0) then I := H;
  if (I > 0) then begin
    S.TextLineBreakStyle:=tlbsCRLF;
    S.Insert(I, AWith);
    if ASeparator <> '' then
        S.Insert(I, ASeparator);
    S.SaveToFile(AFileName, tEncoding.UTF8);
  end;
  S.Free;
  if I <=0 then
    raise Exception.Create('error appending ' + AFileName);
  Result := True;
end;

function TTextToHTML.AppendHTML(AFileName, AWith, ASeparator: String;
  AMustExist: boolean): boolean;
begin
 Result := InsertBefore(AFilename, AWith, ASeparator, '</body>', AMustExist);
end;

function TTextToHTML.PrependHTML(AFileName, AWith, ASeparator: String;
  AMustExist: boolean; AAfterID: String): boolean;
var
  S : TStringList;
  I, H : integer;
begin
  result := false;
  if not FileExists(AFileName) then begin
    if AMustExist then
      raise Exception.Create('file ' + AFileName + ' not found')
    else
      if Not CreateHTML(AFileName, '', '',  False) then
        Exit;
  end;
  S := TStringList.Create;
  S.LoadFromFile(AFileName, tEncoding.UTF8);
  I := 0;
  while I < S.Count do
     if Copy(Lowercase(Trim(S[I])), 1, 5) <> '<body' then Inc(I) else break;
  H := I;
  while H < S.Count do
     if Copy(Lowercase(Trim(S[H])),1,Length(HeaderID)) <> HeaderID then
       Inc(H) else break;
  if (H < S.Count) then I:= H;
  while H < S.Count do
     if Copy(Lowercase(Trim(S[H])),1,Length(AAfterID)) <> AAfterID then
       Inc(H) else break;
  if (H < S.Count) then I:= H;
  if (I < S.Count) then begin
    S.TextLineBreakStyle:=tlbsCRLF;
    if I + 1 < S.Count then begin
      if ASeparator <> '' then
        S.Insert(I+1, ASeparator);
      S.Insert(I+1, AWith);
    end else begin
      if ASeparator <> '' then
        S.Add(ASeparator);
      S.Add(AWith);
    end;
    S.SaveToFile(AFileName, tEncoding.UTF8);
  end;
  H := S.Count;
  S.Free;
  if I = H then
    raise Exception.Create('error prepending ' + AFileName);
  Result := True;
end;

function TTextToHTML.PrependHTML(AFileName, AWith, ASeparator: String;
  AMustExist: boolean): boolean;
begin
  Result := PrependHTML(AFileName, AWith, ASeparator, AMustExist, MenuID);
end;

function TTextToHTML.AddHead(AFileName, AWith: String; AMustExist: boolean
  ): boolean;
begin
  Result := InsertBefore(AFilename, AWith, '', '</head>', AMustExist);
end;

function TTextToHTML.AddHeader(AFileName, AHeader: String; AMustExist: boolean
  ): boolean;
begin
  Result := PrependHTML(AFileName, HeaderID + AHeader + '</div>', '', AMustExist, HeaderID);
end;

function TTextToHTML.AddMenu(AFileName, AMenu: String; AMustExist: boolean
  ): boolean;
begin
  Result := PrependHTML(AFileName, MenuID + AMenu + '</div>', '', AMustExist, MenuID);
end;

function TTextToHTML.AddFooter(AFileName, AFooter: String; AMustExist: boolean
  ): boolean;
begin
  Result := AppendHTML(AFileName, FooterID + AFooter + '</div>', '', AMustExist);
end;

function TTextToHTML.FetchComment(AFileName, ACommentID: String;
  AMustExist: boolean): string;
var
  S : String;
  P : integer;
begin
  S := '';
  Result := S;
  if LoadFromFile(AFileName, S, True) <> 0 then exit;
  P := Pos('<!-- ' + ACommentID + ':', S);
  if P < 1 then begin
    if AMustExist then
      Raise Exception.Create(AFileName + ' does not contain comment with ID ' + ACommentID);
    Exit;
  end;
  Delete(S, 1, P + Length(ACommentID) + 6);
  P := Pos('-->', S);
  if P > 0 then
    Delete(S, P, Length(S));
  Result := Trim(S);
end;

function TTextToHTML.Comment(AComment: String): String;
begin
  Result := Comment('', AComment);
end;

function TTextToHTML.Comment(ACommentID, AComment: String): String;
begin
  if (ACommentID <> '') and (AComment <> '') then
    ACommentID := ACommentID + ': ';
  Result := '<!-- ' + ACommentID + AComment + ' -->';
end;

procedure TTextToHTML.ForText(ARel : String; var S: TStringList; AFunc: TTextFunction;
  const AIgnoreTag, AIgnoreClass: TStringArray);
var
  B, E, I : integer;
  ITag, IClass : integer;

  procedure HandleText;
  var
    H, N : String;
  begin
    if IClass + ITag > 0 then Exit;
    H := Copy(S[I], B, E-B);
    N := AFunc(ARel, H);
    if H = N then Exit;
    S[I] := Copy(S[I], 1, B - 1) + N + Copy(S[I], E);
    E := E - Length(H) + Length(N);
 end;

  procedure HandleTag;
  var
    Tag, C : string;
    ID : String;
    BT, ET : boolean;
    P : integer;

  begin
    BT := True;
    ET := False;
    Tag := Copy(S[I], B, E-B);
    ID := ExcludeLeading('<', ExcludeTrailing('>', Lowercase(Trim(Tag))));
    C := '';
    if HasLeading('/', ID) then begin
      ET := True;
      BT := False;
      Delete(ID, 1,1);
    end;
    if HasTrailing('/', ID) then begin
      ET := True;
      ID := Trim(ExcludeTrailing('/', ID));
    end;

    if Length(AIgnoreClass) > 0 then begin
      P := Pos('class="', ID);
      if P > 0 then begin
        C := Getword(ID, 1, P + 7);
      end;
      if (IClass = 0) and InArray(C, AIgnoreClass, false) then
         Inc(IClass);
      if (IClass > 0) and ET then
         Dec(IClass);
    end;

    if Pos(SPACE, ID) > 0 then
      ID := Copy(ID, 1, Pos(SPACE, ID) - 1);

    case ID of
      'br', 'hr', 'wbr', '!--' : ET := True;
    end;

    if InArray(ID, AIgnoreTag,false) then begin
       if BT then Inc(ITag);
       if ET then Dec(ITag);
    end;
  end;

  procedure Process;
  begin
    while I < S.Count do begin
      E := 1;
      repeat
        B := E;
        E := Pos('<', S[I], B);
        if E < 1 then begin
          E := Length(S[I]) + 1;
          HandleText;
        end else begin
          if E > 1 then
            HandleText;
          B := E;
          E := Pos('>',S[I], B);
          if E < 1 then
            E := Length(S[I]) + 1
          else
            Inc(E);
          HandleTag;
        end;
      until (E < 1) or (E > Length(S[I]));
      Inc(I);
    end;
  end;

begin
  if not Assigned(AFunc) then
    Raise Exception.Create('null text process');
  I := 0;
{  Level := 0; }
  ITag := 0;
  IClass := 0;
  Process;
end;

procedure TTextToHTML.ForText(ARel : String; var S: TStringList; AFunc: TTextFunction);
begin
  ForText(ARel, S, AFunc, EmptyStringArray, EmptyStringArray);
end;


initialization

  TextToHTML := TTextToHTML.Create;

finalization

  if Assigned(TextToHTML) then
    FreeAndNil(TextToHTML);

end.

