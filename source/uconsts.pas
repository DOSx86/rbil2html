unit uConsts;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uPasExt;

const
  CONFIG_FILE = 'CONFIG.LST';  // Main config file, processed first.
  USER_FILE   = 'USER.LST';    // User supplemental config, processed second.

  { Unicode haracters used for page navigation icons }
  Navigate_First       : ShortString = '&#x21e4;';
  Navigate_Last        : ShortString = '&#x21e5;';
  Navigate_Previous    : ShortString = '&#x21e0;';
  Navigate_Next        : ShortString = '&#x21e2;';
  Navigate_Up          : ShortString = '&#x21e7;';
  Navigate_Down        : ShortString = '&#x1f447;';
  Navigate_Home        : ShortString = '&#x1f3e0;'; // '&#x1f3da;';
  Navigation_Seperator : ShortString = '&#x2215;';

  // Other Possiblities,  $1d4d0, $1f110, $1f130, $1f150, $1f170, $1f1e6, $0040
  // $24b6, $24d0
  Navigate_AplhaSet    =  $24b6;


  CategorySeparator : String = '--------';

  Homepage_Title  : String = 'HTML Edition';
  Charspage_Title : String = 'Character Tables';

  DOCID_SECTION = 'SECTION';
  DOCID_SRCFILE = 'SOURCE_FILE';
  DOCID_SRCLINE = 'SOURCE_LINE';
  DOCID_TITLE   = 'TITLE';
  DOCID_OUTFILE = 'FILENAME';
  DOCID_SUBFUNC = 'FUNCTION';

  cidStartOfDocument = '#StartOfDocument';
  cidEndOfDocument   = '#EndOfDocument';
  cidNavagation      = 'navigation';
  cidPageHeading     = 'page-heading';

  WebVarChar    = '$';

  MinForIndex = 2;
  LenForIndex = 5;

  IndexSimilar : array of string = (
    'S',
    'D',
    'R',
    'ES',
    'ED',
    'ER'
  );

  PreviousPageLink : String = '&lt;&lt;Previous Page';
  NextPageLink : String = 'Next Page&gt;&gt;';

  RBROWN_LINK = 'http://www.cs.cmu.edu/~ralf/';
  RBIL_REMOTE_LINK = 'http://www.cs.cmu.edu/~ralf/files.html';

  { List of all documents to be processed, Note *.A and *.LST docs are itterated
   *.A through *.Z or until no subsequent letter is found}
  RBIL_Files: array of string = (
    'GLOSSARY.LST',
    'OVERVIEW.LST',
    'INTERRUP.1ST',
    {$IFNDEF DEV}
    'INTERRUP.A',
    {$ENDIF}
    'PORTS.A',
    'FARCALL.LST',
    'MEMORY.LST',
    {$IFNDEF DEV}
    'MSR.LST',
    'SMM.LST',
    'BIBLIO.LST',
    'CMOS.LST',
    'I2C.LST',
    'OPCODES.LST',
    'TABLES.LST',
    '86BUGS.LST',
    'INTERRUP.PRI',
    'RBROWN.TXT',
    'README.NOW',
    'FAQ.LST',
    '_ADVERT.TXT',
    {$ENDIF}
    'NEEDHELP.TXT'
   );

  MORE_Files: array of string = (
    'ADDENDUM.LST',
    'ADDITION.LST',
    'SUPPLMNT.LST',
    'MORE.LST'
   );

  {$IFOPT D+}
  DEBUG_Files: array of string = (
  'GLOSSARY.LST',
  'OVERVIEW.LST',
  'INTERRUP.1ST',
  'INTERRUP.A',
  'PORTS.A',
  'FARCALL.LST',
  'MEMORY.LST',
{  'MSR.LST',
  'SMM.LST',}
  'BIBLIO.LST',
{  'CMOS.LST',
  'I2C.LST',
  'OPCODES.LST',
  'TABLES.LST',
  '86BUGS.LST',
  'INTERRUP.PRI',}
  'RBROWN.TXT',
  'README.NOW',
{  'FAQ.LST',
  '_ADVERT.TXT',}
  'NEEDHELP.TXT'
   );
  {$ENDIF}

  { Raw text documents that are presented retain all formatting }
  RBIL_Docs = ';GLOSSARY.LST;OPCODE.LST;SMM.LST;86BUGS.LST;INTERRUP.PRI;' +
    'RBROWN.TXT;README.NOW;FAQ.LST;_ADVERT.TXT;NEEDHELP.TXT;';

  { Specific Document Titles }
  RBIL_Titles : array of array [0..1]  of string = (
    ('CONTACT_INFO', 'Contact Information'),
    ('GLOSSARY', 'Glossary Terms'),
    ('SMM', 'System-management mode'),
    ('FAQ', 'Frequently Asked Questions'),
    ('86BUGS', '86 Bugs List'),
    ('ADVERT', 'Advertisements'),
    ('INTERRUP', 'Interrupt Primer'),
    ('RBROWN', 'Ralf Brown Programs'),
    ('NEEDHELP', 'Volunteers Needed'),
    ('HISTORY', 'Revision History'),
    ('TABLES', 'Selected Tables')
  );

  { Specific main subdirectory Group Titles, others are TitleCase }
  RBIL_Groups : array of array [0..1] of String = (
    ('cmos', 'CMOS-Memory Map'),
    ('farcall', 'Far Call Interface List'),
    ('interrup', 'Interrupt List'),
    ('i2c', 'I2C Bus Devices'),
    ('memory', 'Memory Map'),
    ('msr', 'Model-specific Registers'),
    ('opcode', 'Opcodes List'),
    ('ports', 'Ports List'),
    ('zindex', 'Browse the Index'),
    ('ztitle', 'Browse by Title'),
    ('zint', 'Browse by Interrupt'),
    ('zaspacer', '')
  );

   { classification subdirectory paths for items under subgroups }
  RBIL_SubCats : array of string = (
    'A,applications',
    'a,accessiblity',
    'B,bios',
    'b,bios_vendor',
    'C,cpu',
    'c,caches',
    'D,dos_kernel',
    'd,io_disk',
    'E,dos_extenders',
    'e,email',
    'F,fax',
    'f,file',
    'G,debuggers',
    'g,games',
    'H,hardware',
    'h,hardware_vendor',
    'I,emulators_ibm',
    'i,system_info',
    'J,japanese',
    'j,joke_programs',
    'K,keyboard',
    'k,compression',
	'l,shells',
	'M,io_mouse',
    'm,memory',
    'N,network',
    'n,io_other',
    'O,other_os',
    'P,io_print',
    'p,power',
	'Q,quarterdeck',
    'R,remote_access',
    'r,runtime',
    'S,io_serial',
    's,sound',
    'T,dos_multitask',
    't,tsr',
    'U,utilities',
    'u,emulators',
    'V,video',
    'v,virus',
    'W,windows',
    'X,bios_expansion',
    'x,nvram',
    'y,security' ,
    '*,other'
  );

  ShortFlags : array of array [0..1] of String = (
    ('U', 'undocumented'),
    ('u', 'partially documented'),
    ('P', 'protected mode'),
    ('R', 'real mode'),
    ('C', 'callout or callback'),
    ('O', 'obsolete')

  );

  IndexInclude : array of String = (
    'Ralf Brown'
  );

  IndexIgnore : array of String = (
    'should',
    'specify'

  );

  { Additional characters in Dictionaries that are considered part of the
  word when tagging }
  DictionaryExtraChars : string = '+-_';

  {$DEFINE SLIMMER}
  UnicodeSkipInfo : String =
    'Some international and null chracters have been omitted.<br>' +
    'Defined for Safari (13.1.2) on macOS High Sierra (10.13.6)';

  UnicodeSkipRanges : array of array[0..1] of integer = (
     {$IFDEF SLIMMER}      ($01, $20),
     ($7f, $80),           ($8d, $8e),
     ($8f, $91),           ($9d, $9e),
     ($a0, $a1),           ($ad, $ae),
     {$ENDIF}
     ($378, $37a),         ($380, $384),         ($38b, $38c),
     ($38d, $38e),         ($3a2, $3a3),
     {$IFDEF SLIMMER}      ($483, $48a),         {$ENDIF}
     ($530, $531),         ($557, $559),
     {$IFDEF SLIMMER}      ($55f, $560),         {$ENDIF}
     ($560, $561),         ($588, $589),         ($58b, $58f),
     ($590, $591),
     {$IFDEF SLIMMER}      ($5c8, $7c0),         {$ELSE}
     ($5c8, $5d0),         ($5eb, $5f0),         ($5f5, $600),
     ($61d, $61e),         ($70e, $70f),         ($74b, $74d),
     ($7b2, $7c0),
     {$ENDIF}
     ($7fb, $800),         ($82e, $830),         ($83f, $840),
     ($85c, $85e),         ($85f, $8a0),         ($8b5, $8b6),
     ($8be, $8d5),         ($8da, $8e0),         ($8e2, $8e3),
     ($8fa, $8fb),

     {$IFDEF SLIMMER}      ($900, $fbe),         {$ELSE}
     ($984, $985),         ($98d, $98f),         ($991, $993),
     // bunch in here
     {$ENDIF}

     ($fcd, $fce),         ($fd9, $1000),

     {$IFDEF SLIMMER}      ($1000, $105a),       {$ELSE}
     ($1022, $1023),       ($1028, $1029),       ($1033, $1036),
     {$ENDIF}

     ($105a, $10a0),

     {$IFDEF SLIMMER}     ($10a0, $13a0),        {$ELSE}
     ($10c6, $10d0),      ($10fd, $1100),        ($115a, $115f),
     ($11a3, $11a8),      ($11fa, $1200),        ($1249, $124a),
     ($124e, $1250),      ($1257, $1258),        ($1259, $125a),
     ($125e, $1260),
     // many more in here
     {$ENDIF}

     ($13f5, $13f8),      ($13fd, $1401),        ($1677, $1680),
     ($169d, $16a0),      ($16f1, $1700),        ($170d, $170e),
     ($1715, $1720),      ($1737, $1740),        ($1754, $1760),
     ($176d, $176e),      ($1771, $1772),        ($1774, $1780),
     ($17de, $17e0),      ($17ea, $17f0),        ($17fa, $1800),
     ($180f, $1810),

     {$IFDEF SLIMMER}      ($1810, $18aa),       {$ENDIF}

     ($18aa, $1900),

     {$IFDEF SLIMMER}      ($1900, $1aa0),       {$ENDIF}

     ($1aae, $1b00),

     {$IFDEF SLIMMER}      ($1b00, $1c80),       {$ENDIF}

     ($1c80, $1d00),       ($1dd0, $1dd3),       ($1dda, $1ddb),
     ($1de7, $1dfe),       ($1f16, $1f18),       ($1f1e, $1f20),
     ($1f46, $1f48),       ($1f4e, $1f50),       ($1f58, $1f59),
     ($1f5a, $1f5b),       ($1f5c, $1f5d),       ($1f5e, $1f5f),
     ($1f7e, $1f80),       ($1fb5, $1fb6),       ($1fc5, $1fc6),
     ($1fd4, $1fd6),       ($1fdc, $1fdd),       ($1ff0, $1ff2),
     ($1ff5, $1ff6),       ($1fff, $2000),

     {$IFDEF SLIMMER}
     ($2000, $2010),       ($2028, $2030),       ($205f, $2070),
     {$ENDIF}

     ($2072, $2074),       ($208f, $2090),       ($2095, $20a0),
     ($20c0, $20d0),       ($20f1, $2100),       ($214f, $2150),
     ($2187, $2189),       ($218a, $2190),       ($23e8, $23e9),
     ($23f4, $23f8),       ($23fb, $2400),       ($2427, $2440),
     ($244b, $2460),       ($269d, $26a0),       ($26b3, $26bd),
     ($26bf, $26c5),       ($26c6, $26c8),       ($26c9, $26ce),
     ($26d0, $26d1),       ($26d2, $26d3),       ($26d5, $26e2),
     ($26e3, $26e9),       ($26eb, $26f0),       ($26f6, $26f7),
     ($26fb, $26fd),       ($26fe, $2701),       ($275f, $2761),
     ($27ce, $27d0),       ($2b4d, $2b50),       ($2b56, $2b90),
     ($2b92, $2c00),       ($2c2f, $2c30),       ($2c5f, $2c60),
     ($2cf4, $2cf9),       ($2d00, $2d30),       ($2d68, $2d6f),
     ($2d71, $2d7f),

     {$IFDEF SLIMMER}      ($2d7f, $2e00),       {$ELSE}
     ($2d97, $2da0),       ($2da7, $2da8),       ($2daf, $2db0),
     ($2db7, $2db8),       ($2dbf, $2dc0),       ($2dc7, $2dc8),
     ($2dcf, $2dd0),       ($2dd7, $2dd8),       ($2ddf, $2de0),
     {$ENDIF}

     ($2e19, $2e1a),       ($2e32, $2e80),

     {$IFDEF SLIMMER}      ($2e80, $2fd6),       {$ELSE}
     ($2e9a, $2e9b),       ($2ef4, $2f00),
     {$ENDIF}

     ($2fd6, $2ff0),       ($2ffc, $3000),

     {$IFDEF SLIMMER}      ($3021, $3248),       {$ENDIF}

     ($3250, $3251),

     {$IFDEF SLIMMER}      ($3260, $32B1),       {$ENDIF}

     {$IFDEF SLIMMER}      ($32c0, $3371),       {$ENDIF}

     ($3377, $337b),

     {$IFDEF SLIMMER}      ($337b, $3380),       {$ENDIF}

     ($33de, $33e0),

     {$IFDEF SLIMMER}      ($33e0, $4db6),       {$ENDIF}

     ($4db6,  $4dc0),

     {$IFDEF SLIMMER}      ($4e00, $9fd0),       {$ENDIF}

     ($9fd0, $a000),       ($a4c7, $a4d0),

     {$IFDEF SLIMMER}      ($a500, $a62c),       {$ENDIF}

     ($a62c, $a640),

     {$IFDEF SLIMMER}      ($a640, $a6f8),       {$ENDIF}

     ($a6f8, $a700),       ($a78f, $a790),       ($a794, $a7a0),
     ($a7ab, $a7f8),

     {$IFDEF SLIMMER}      ($a800, $ab70),       {$ENDIF}

     ($abee, $abf0),       ($abfa, $ac00),

     {$IFDEF SLIMMER}      ($ac00, $d7a4),       {$ENDIF}

     ($d7a4, $d800),       ($d800, $e000),

     {$IFDEF SLIMMER}      ($e000, $e870),       {$ENDIF}

     ($e870, $f800),       ($f800, $f802),       ($f80d, $f81e),
     ($f828, $f83d),       ($f83e, $f840),       ($f850, $f880),
     ($f883, $f8b7),       ($f8c1, $f8e5),

     {$IFDEF SLIMMER}      ($f900, $fa6e),       {$ELSE}
     ($fa2e, $fa30),
     {$ENDIF}

     ($fa6e, $fb00),       ($fb07, $fb0f),

     {$IFDEF SLIMMER}      ($fb0f, $fdfe),       {$ENDIF}

     ($fdfe, $fe10),       ($fe1a, $fe20),       ($fe27, $fe30),
     ($fe53, $fe54),       ($fe67, $fe68),       ($fe6c, $fe70),

     {$IFDEF SLIMMER}      ($fe70, $ff00),       {$ENDIF}

     ($ff00, $ff01),

     {$IFDEF SLIMMER}      ($ff66, $ffe0),       {$ENDIF}

     ($ffe7, $ffe8),       ($ffef, $fff0),

     {$IFDEF SLIMMER}      ($fff0, $fffd),       {$ENDIF}

     ($fffd, $fffe),       ($fffe, $10000),

     ($1000c, $1000d),     ($10027, $10028),     ($1003b, $1003c),
     ($1003e, $1003f),     ($1004e, $10050),     ($1005e, $10080),
     ($100fb, $10100),     ($10103, $10107),     ($10134, $10137),
     ($10144, $10146),     ($10176, $10179),     ($10183, $10184),
     ($10187, $1018a),     ($1018b, $10190),     ($1019c, $10280),
     ($1029d, $102a0),     ($102d1, $10300),     ($1031f, $10320),
     ($10324, $10330),     ($1034b, $10380),     ($1039e, $1039f),
     ($103c4, $103c8),     ($103d6, $10400),     ($1049e, $104a0),
     ($104aa, $10800),     ($10806, $10808),     ($10809, $1080a),
     ($10836, $10837),     ($10839, $1083c),     ($1083d, $1083f),
     ($10856, $10857),     ($10860, $10900),     ($1091c, $1091f),
     ($1093a, $1093f),     ($10940, $10a00),     ($10a04, $10a05),
     ($10a07, $10a0c),     ($10a14, $10a15),     ($10a18, $10a19),
     ($10a34, $10a38),     ($10a3b, $10a3f),     ($10a48, $10a50),
     ($10a59, $10a60),     ($10a80, $10b00),     ($10b36, $10b39),
     ($10b56, $10b58),     ($10b73, $10b78),     ($10b80, $10c00),
     ($10c3f, $10c40),     ($10c49, $11000),     ($11029, $1102a),
     ($1104e, $11052),     ($11070, $11080),     ($110c2, $12000),

     {$IFDEF SLIMMER}      ($12000, $1236f),     {$ENDIF}
     ($1236f, $12400),
     {$IFDEF SLIMMER}      ($12400, $12463),     {$ENDIF}
     ($12463, $12470),
     {$IFDEF SLIMMER}      ($12470, $12474),     {$ENDIF}
     ($12474, $13000),     ($1342f, $16800),     ($16a39, $16f00),
     ($16f45, $16f50),     ($16f7f, $16f8f),     ($16fa0, $1d000),

     ($1d000, $1d100),     ($1d109, $1d110),     ($1d113, $1d11e),
     ($1d11f, $1d121),     ($1d123, $1d12a),     ($1d12c, $1d173),
     {$IFDEF SLIMMER}      ($1d173, $1d17b),     {$ENDIF}
     ($1d17b, $1d192),     ($1d194, $1d1a6),     ($1d1a9, $1d1c7),
     ($1d1cf, $1d300),     ($1d357, $1d360),     ($1d372, $1d400),
     ($1d455, $1d456),     ($1d49d, $1d49e),     ($1d4a0, $1d4a2),
     ($1d4a3, $1d4a5),     ($1d4a7, $1d4a9),     ($1d4ad, $1d4ae),
     ($1d4ba, $1d4bb),     ($1d4bc, $1d4bd),     ($1d4c4, $1d4c5),
     ($1d506, $1d507),     ($1d50b, $1d50d),     ($1d515, $1d516),
     ($1d51d, $1d51e),     ($1d53a, $1d53b),     ($1d53f, $1d540),
     ($1d545, $1d546),     ($1d547, $1d54a),     ($1d551, $1d552),
     ($1d6a6, $1d6a8),     ($1d7ca, $1d7ce),     ($1d800, $1f000),

     ($1f02c, $1f030),     ($1f094, $1f0a0),     ($1f0af, $1f0b1),
     ($1f0bf, $1f0c1),     ($1f0d0, $1f0d1),     ($1f0e0, $1f100),
     ($1f10d, $1f110),     ($1f12a, $1f130),     ($1f14a, $1f14f),
     ($1f16c, $1f170),     ($1f18a, $1f18e),     ($1f190, $1f191),
     ($1f19b, $1f1e6),     ($1f200, $1f201),     ($1f203, $1f210),
     ($1f213, $1f215),     ($1f23b, $1f240),     ($1f249, $1f250),
     ($1f252, $1f300),     ($1f322, $1f324),     ($1f394, $1f396),
     ($1f398, $1f399),     ($1f39c, $1f39e),     ($1f3f1, $1f3f3),
     ($1f3f6, $1f3f7),     ($1f4fe, $1f4ff),     ($1f53e, $1f549),
     ($1f54f, $1f550),     ($1f568, $1f56f),     ($1f570, $1f573),
     ($1f57b, $1f587),     ($1f588, $1f58a),     ($1f58e, $1f590),
     ($1f591, $1f595),     ($1f597, $1f5a4),     ($1f5a6, $1f5a8),
     ($1f5a9, $1f5b1),     ($1f5b3, $1f5bc),     ($1f5bd, $1f5c2),
     ($1f5c5, $1f5d1),     ($1f5d4, $1f5dc),     ($1f5df, $1f5e1),
     ($1f5e2, $1f5e3),     ($1f5e4, $1f5e8),     ($1f5e9, $1f5f3),
     ($1f5f4, $1f5fa),     ($1f650, $1f680),     ($1f6c6, $1f6cb),
     ($1f6d3, $1f6e0),     ($1f6e6, $1f6e9),     ($1f6ea, $1f6eb),
     ($1f6ed, $1f6f0),     ($1f6f1, $1f6f3),     ($1f6f9, $1f700),
     ($1f774, $1f910),     ($1f93b, $1f93c),     ($1f93f, $1f940),
     ($1f946, $1f947),     ($1f94d, $1f950),     ($1f96c, $1f980),
     ($1f998, $1f9c0),     ($1f9c1, $1f9d0),     ($1f9e7, $20000),

     {$IFDEF SLIMMER}      ($20000, $30000)     {$ELSE}
     ($2ce94, $2f000),     ($2f000, $2f804),    ($2fa08, $30000)
     {$ENDIF}

   );

implementation

end.
