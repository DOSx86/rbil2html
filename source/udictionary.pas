unit uDictionary;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uPasExt, avl_tree;

type

 { TDictionary }

  TDictionary = class(TPersistent)
    private
      FExtraWordChars: String;
      // FTree : TAVLTree; // Later for performance on large dictionaries
      FList : TStringList;
      FNameMaxChars,
      FNameMaxWords : integer;
      function GetCount: integer;
      function GetDuplicates: TDuplicates;
      function GetNameMaxChars: integer;
      function GetNameMaxWords: integer;
      function GetNames(index : integer): String;
      function GetSorted: boolean;
      function GetValues(index : integer): String;
      procedure SetDuplicates(AValue: TDuplicates);
      procedure SetExtraWordChars(AValue: String);
      procedure SetList(AValue: TStringList);
      procedure SetSorted(AValue: boolean);
    protected
    public
      constructor Create;
      destructor Destroy; override;
      procedure Clear; virtual;
      function Add(AName, AValue : String) : integer; virtual; overload;
      procedure Delete(AIndex : integer); virtual; overload;
      property Count : integer read GetCount;
      property NameMaxChars : integer read GetNameMaxChars; // Longest name in characters;
      property NameMaxWords : integer read GetNameMaxWords; // Longest name in words
      property ExtraWordChars : String read FExtraWordChars write SetExtraWordChars;
      property Names [index : integer] : String read GetNames;
      property Values [index : integer] : String read GetValues;
      function Find(AName : String) : integer; virtual; overload;
      property Duplicates : TDuplicates read GetDuplicates write SetDuplicates;
      property Sorted : boolean read GetSorted write SetSorted;
    published
  end;

implementation

type

  { TDefinition }

  TDefinition = class(TObject)
    private
      PName, PValue : PString;
      function GetName: String;
      function GetValue: String;
      procedure SetName(AValue: String);
      procedure SetValue(AValue: String);
    protected
    public
      constructor Create(AName, AValue : String);
      destructor Destroy; override;
      property Name : String read GetName write SetName;
      property Value : String read GetValue write SetValue;
    published
  end;


{ TDefinition }

function TDefinition.GetName: String;
begin
  Result := PName^;
end;

function TDefinition.GetValue: String;
begin
  if Assigned(PValue) then
    Result := PValue^
  else
    Result := '';
end;

procedure TDefinition.SetName(AValue: String);
begin
  DisposeStr(PName);
  PName := NewStr(AValue);
end;

procedure TDefinition.SetValue(AValue: String);
begin
  DisposeStr(PValue);
  PValue := NewStr(AValue);
end;

constructor TDefinition.Create(AName, AValue : String);
begin
  inherited Create;
  PName := NewStr(AName);
  PValue := NewStr(AValue);
end;

destructor TDefinition.Destroy;
begin
  if Assigned(PName) then begin
    DisposeStr(PName);
    PName := nil;
  end;
  if Assigned(PValue) then begin
    DisposeStr(PValue);
    PValue := nil;
  end;
  inherited Destroy;
end;

{ TDictionary }

procedure TDictionary.SetList(AValue: TStringList);
begin
  if FList=AValue then Exit;
  FList:=AValue;
end;

procedure TDictionary.SetSorted(AValue: boolean);
begin
  FList.Sorted:=AValue;
end;

function TDictionary.GetCount: integer;
begin
  Result := FList.Count;
end;

function TDictionary.GetDuplicates: TDuplicates;
begin
  Result := FList.Duplicates;
end;

function TDictionary.GetNameMaxChars: integer;
begin
  Result := FNameMaxChars;
end;

function TDictionary.GetNameMaxWords: integer;
begin
  Result := FNameMaxWords;
end;

function TDictionary.GetNames(index : integer): String;
begin
  Result := TDefinition(FList.Objects[index]).Name;
end;

function TDictionary.GetSorted: boolean;
begin
  Result := FList.Sorted;
end;

function TDictionary.GetValues(index : integer): String;
begin
  Result := TDefinition(FList.Objects[index]).Value;
end;

procedure TDictionary.SetDuplicates(AValue: TDuplicates);
begin
  FList.Duplicates:=AValue;
end;

procedure TDictionary.SetExtraWordChars(AValue: String);
begin
  if FExtraWordChars=AValue then Exit;
  FExtraWordChars:=AValue;
end;

constructor TDictionary.Create;
begin
  inherited Create;
  FList := TStringList.Create;
  FList.Sorted := True;
  FNameMaxWords := 0;
  FNameMaxChars := 0;
  FExtraWordCHars := '';
end;

destructor TDictionary.Destroy;
begin
  if Assigned(FList) then FreeAndNil(FList);
  inherited Destroy;
end;

procedure TDictionary.Clear;
begin
  FList.Clear;
  FNameMaxWords := 0;
  FNameMaxChars := 0;
end;

function TDictionary.Add(AName, AValue : String): integer;
var
  C : integer;
begin
  if (AName = '') then begin
    Result := -1;
    Exit;
  end;
  Result := FList.AddObject(AName, TDefinition.Create(AName, AValue));
  if Length(AName) > FNameMaxChars then
    FNameMaxChars := Length(AName);
  C := CountWords(AName, FExtraWordChars);
  if C > FNameMaxWords then
    FNameMaxWords := C;
end;

procedure TDictionary.Delete(AIndex: integer);
begin
  FList.Delete(AIndex);
end;

function TDictionary.Find(AName: String): integer;
var
  AIndex : integer;
begin
  if FList.Find(AName, AIndex) then
    Result := AIndex
  else
    Result := -1;
end;

end.

