unit uCorrect;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uDictionary;

var
  Corrections : TDictionary;

function SpellCorrect(S : String; ASource : String; ALine : integer; PostProc : boolean) : String; overload;
function SpellCorrect(S : String) : String; overload;

implementation

// Probably end up removing TDictionary and just work straight from the
// corrections array... Later.

uses uConsts, uPasExt;

function SpellCorrect(S: String; ASource : String; ALine : integer; PostProc : boolean): String;
var
  I, P : integer;
  C, T, A, B, D : String;
begin
  ASource := IntToStr(ALine) + ',' + ASource;
  for I := 0 to Corrections.Count - 1 do begin
    C := Copy(Corrections.Names[I],1,1);
    if (C <> '-') and (C <> '+') then begin
      if (Not PostProc) then Continue;
      P := Pos(Corrections.Names[I], S);
      if P > 0 then begin
        Insert(Corrections.Values[I], S, P);
        Delete(S, P + Length(Corrections.Values[I]), Length(Corrections.Names[I]));
      end;
    end else begin
      if (C = '+') and (Not PostProc) then Continue;
      if Copy(Corrections.Names[I], 2, Length(Corrections.Names[I])) <> ASource then
        Continue;
      C := Corrections.Values[I];
      P := Pos(',', C);
      if P > 0 then C := Copy(C, 1, P - 1);
      case C of
        'null'   : S := '';
        'delete' : S := #0 + 'delete';
        'insert' : S := #0 + 'insert' + Copy(Corrections.Values[I], P + 1, Length(Corrections.Values[I]));
        'replace',
        'replaceall' : begin
          T := Copy(Corrections.Values[I], P + 1, Length(Corrections.Values[I]));
          P := Pos(',', T);
          if P < 2 then
            raise Exception.create('invalid correction data "' + Corrections.Values[I] + '"');
          D := Copy(T, 1, P - 1);
          Delete(T, 1, P);
          A := Copy(T, 1, Pos(D, T) - 1);
          B := Copy(T, Length(A) + Length(D) + 1, Length(T));
          if C = 'replace' then begin
            P := Pos(A, S);
            if P > 0 then begin
              Insert(B, S, P);
              Delete(S, P + Length(B), Length(A));
            end;
          end else
            S := replaceAll(A, B, S);
        end;
      end;
    end;
  end;
  Result := S;
end;

function SpellCorrect(S: String): String;
begin
  Result := SpellCorrect(S, '', -1, False);
end;


initialization

  Corrections := TDictionary.Create;

finalization

  if Assigned(Corrections) then FreeAndNil(Corrections);

end.

