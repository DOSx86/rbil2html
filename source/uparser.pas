unit uParser;

{DEFINE DEVNEXT}

{$mode objfpc}{$H+}

interface

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils,
  { you can add units after this }
  uPasExt, uDictionary
  ;

{$I version.inc}

const
  Verbose : integer = 1;

type
  TTextProcess = function (Line: integer; S : String) :String of object;

  TFileProcess = procedure (Files : TStringList; ASubDir : String) of object;

  { TRBILParser }

  TRBILParser = class(TPersistent)
  private
    FBaseURL: String;
    {$IFOPT D+}
    FDebug: boolean;
    {$ENDIF}
    FIndexLevel: integer;
    FIndexLower: boolean;
    FRBILDir : String;
    FSaveIndex: boolean;
    FSecCount : integer;
    FGoodLinks, FBadLinks, FNavLinks, FPages : integer;
    FSecLine : integer;
    FPreTitle : String;
    FFooter: String;
    FHeader: String;
    FPreprocess, FPostProcess : boolean;
    FFilename : String;        // Filename of most recently FindSourceFiled file
    FCategory : String;        // Category of current item
    FClass : String;           // Classificatiion of current item
    FGroup : String;           // Group for current class
    FCSSData : String;
    FSection : String;
    FSubFunc : String;
    FRelease_Copyright: string;
    FRelease_Date: string;
    FRelease_Title: string;
    FRelease_Version: string;
    FTitle, FAltTitle : String;// Title for current item
    FIndenting: integer;
    FIndentStr : String;
    FOutDir: TPathString;      // HTML output base directory
    FSrcDir: TPathString;      // Input file base search directory
    FFlags, FFlagsShort,       // Item Flags list
    FCategories : TStringArray;// Item categories list
    FAbbr, FDef : TDictionary;
    FData, FComments, FIgnore : TStringList;
    FSkipMany : boolean;
    FTables, FDocs, FInts, FIndexer, FIntFunc,
    FDocTitles, FWebVars, FWebPages : TDictionary;
    FIdxId, FIdxCnt : integer;
    function GetFooter: String;
    function GetHeader: String;
    procedure SetBaseURL(AValue: String);
    {$IFOPT D+}
    procedure SetDebug(AValue: boolean);
    {$ENDIF}
    procedure SetIndenting(AValue: integer);
    procedure SetIndexLevel(AValue: integer);
    procedure SetIndexLower(AValue: boolean);
    procedure SetOutDir(AValue: TPathString);
    procedure SetRelease_Copyright(AValue: string);
    procedure SetRelease_Date(AValue: string);
    procedure SetRelease_Title(AValue: string);
    procedure SetRelease_Version(AValue: string);
    procedure SetSaveIndex(AValue: boolean);
    procedure SetSrcDir(AValue: TPathString);
  protected
    property Indenting : integer read FIndenting write SetIndenting;
    property Header : String read GetHeader;
    property Footer : String read GetFooter;
    procedure ClearCurrent; virtual;
    function FindSourceFile(AFileName : String; ARaise : boolean = true) : String; virtual;
    function FetchMaxParts (AValue: String) : integer; virtual;
    procedure AnalyzeVersion; virtual;
    function CheckDirectory(APath : String; ASub : String = ''):String; virtual;
    function InterruptSubDir : string; virtual;
    function ScanForTables(ARef : String): integer; virtual;
    function TitleStr (ATitle : String = '') : String; virtual;
    function  TagTitle(ATitle : String = '') : String; virtual;
    procedure TagTables; virtual;
    procedure TagOpcodes(ARef : String); virtual;
    procedure ParseSeeRef(ARef, ARel : String; var ARes, AQuoted : string); virtual;
    procedure TagSeeRef(AStart, AEnd, ARel : String); virtual;
    procedure TagSeeAlso(ARel : String); virtual;
    procedure TagRefItems(ARel : String; var S : TStringList); virtual;
    procedure TagRefInt(ARel : String; var S : TStringList); virtual;
    function FindRef(ARef, AExtra : String) : integer; virtual;
    procedure PrepDirTitles; virtual;
    procedure InitWebVars; virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Indent; virtual;
    procedure Outdent; virtual;
    procedure Message(AValue: String; ACRLF: boolean = true); virtual; overload;
    procedure Message(Level : integer; AValue: String; ACRLF: boolean = true); virtual; overload;
    procedure DoRun; virtual;
    procedure ProcessInterrupts; virtual;
    procedure ProcessFileList(AValue : TStringArray; ARequired : boolean = true); virtual;
    procedure ProcessFile(AValue : String); virtual;

    function IsDivider(AValue : String):boolean; virtual;

    procedure TagAll(FD : TStringList = nil); virtual;

    procedure ProcessSection; virtual;
    procedure CommentSection; virtual;

    procedure IgnoreSection; virtual;
    procedure RawSection(ASubDir : String; PreTag : boolean = true); virtual; overload;
    procedure RawSection(PreTag : boolean = true); virtual; overload;
    procedure SimpleSection(ASubDir : String; AMethod : integer = 1); virtual; overload;
    procedure SimpleSection(AMethod : integer = 1); virtual; overload;


    procedure StandardDoc; virtual;
    procedure RawDoc; virtual;

    procedure CSSDataSection; virtual;
    procedure CreditsSection; virtual;
    procedure CopyrightSection; virtual;
    procedure AddressesSection; virtual;
    procedure TrademarksSection; virtual;
    procedure QuotesSection; virtual;
    procedure FlagsSection; virtual;
    procedure CategoriesSection; virtual;
    procedure TablesSection; virtual;
    procedure TitlesSection; virtual;
    procedure HistorySection; virtual;
    procedure AbbreviationsSection; virtual;
    procedure GlossarySection; virtual;
    procedure NoteSection; virtual;
    procedure WebPageSection; virtual;

    procedure RawTextSection; virtual;
    procedure OpcodesSection; virtual;

    procedure IndexIncludeSection; virtual;
    procedure IndexExcludeSection; virtual;
    procedure CorrectionSection; virtual;

    procedure UnicodeTables; virtual;
    procedure AsciiTable(ACodePage : integer); virtual;
    procedure AsciiTables; virtual;

    procedure ProcessList(var ATable: TStringArray; AColon: integer); virtual;

    procedure ItemBlock; virtual;
    procedure IntItem(AKind : String); virtual;

    function PDDefault(Line:integer; S : String):String; virtual;
    function PDUnchanged(Line:integer; S : String):String; virtual;
    function PDTextBlocks(Line:integer; S : String):String; virtual;

    procedure SimpleHTML(var AData : TStringList; ASubDir : String; ATextProcess : TTextProcess; AParagraphs: boolean); virtual; overload;
    procedure SimpleHTML(ASubDir : String; ATextProcess : TTextProcess; AParagraphs: boolean); virtual; overload;
    procedure SimpleHTML(ATextProcess : TTextProcess; AParagraphs: boolean); virtual; overload;
    procedure SimpleHTML(ATextProcess : TTextProcess = nil); virtual; overload;
    procedure InterruptHTML(ASubDir, AFileName : String); virtual; overload;
    procedure MakeCSS; virtual;

    procedure SetDocTitle(AFileName, ATitle : String); virtual; overload;
    function GetDocTitle(AFileName : String) : string; virtual; overload;
    function GetDocFunc(AFileName : String) : string; virtual; overload;

    procedure ForFiles(AFileProcess : TFileProcess; ARecurse : boolean; ASubDir : string = ''); virtual; overload;
    procedure ForFiles(AFileProcess : TFileProcess; ASubDir : string = ''); virtual; overload;
    procedure FPIndex(Files : TStringList; ASubDir : String);
    procedure FPNavigation(Files : TStringList; ASubDir : String);

    procedure ProcessWebPages(APreIndex : boolean = false); virtual;

    procedure PrepIndexScan; virtual;
    procedure IndexScan; virtual;
    procedure SimplifyIndex; virtual;
    procedure IndexPrune(ASecondPass: boolean = False); virtual;
    procedure IndexTag(AFileName : String; FD : TStringList); virtual;

    procedure MakeExtraViews; virtual;
    procedure MakeByIndex; virtual;
    procedure MakeByInterrupt; virtual;
    procedure MakeByTitle; virtual;
    procedure MakeBrowseBy(ALinks : TDictionary; ASubDir : String); virtual;
    procedure OutputIndexWords; virtual;

    function OnTagRefInt(ARel, AStr : String) : String; virtual;


    procedure PostRun; virtual;
    procedure SetIntFuncData; virtual;
    function GetIntFuncData(ASection : string = '') : String; virtual;

  published
    {$IFOPT D+}
    property Debug : boolean read FDebug  write SetDebug;
    {$ENDIF}
    property SrcDir : TPathString read FSrcDir write SetSrcDir;
    property OutDir : TPathString read FOutDir write SetOutDir;
    property Release_Title : string read FRelease_Title write SetRelease_Title;
    property Release_Version : string read FRelease_Version write SetRelease_Version;
    property Release_Date : string read FRelease_Date write SetRelease_Date;
    property Release_Copyright : string read FRelease_Copyright write SetRelease_Copyright;
    property IndexLevel : integer read FIndexLevel write SetIndexLevel;
    property IndexLower : boolean read FIndexLower write SetIndexLower;
    property SaveIndex : boolean read FSaveIndex write SetSaveIndex;
    property BaseURL : String read FBaseURL write SetBaseURL;
  end;


implementation

uses uConsts, uTextToHtml, uTransUTF, uCorrect, DateUtils;

{ TRBILParser }

procedure TRBILParser.SetOutDir(AValue: TPathString);
begin
  AValue:=ExpandFileName(AValue);
  if FOutDir=AValue then Exit;
  AValue:=CheckDirectory(AValue);
  if AValue <> '' then
     FOutDir := AValue;

  TextToHTML.CSS := AValue + 'rbil-' + WebSafeName(IntToStr(DateTimeToUnix(Now)))+ '.css';
end;

procedure TRBILParser.SetRelease_Copyright(AValue: string);
begin
  if FRelease_Copyright=AValue then Exit;
  FRelease_Copyright:=AValue;
end;

procedure TRBILParser.SetRelease_Date(AValue: string);
begin
  if FRelease_Date=AValue then Exit;
  FRelease_Date:=AValue;
end;

procedure TRBILParser.SetRelease_Title(AValue: string);
begin
  if FRelease_Title=AValue then Exit;
  FRelease_Title:=AValue;
end;

procedure TRBILParser.SetRelease_Version(AValue: string);
begin
  if FRelease_Version=AValue then Exit;
  FRelease_Version:=AValue;
end;

procedure TRBILParser.SetSaveIndex(AValue: boolean);
begin
  if FSaveIndex=AValue then Exit;
  FSaveIndex:=AValue;
end;

procedure TRBILParser.SetIndenting(AValue: integer);
begin
  if FIndenting=AValue then Exit;
  FIndenting:=AValue;
  FIndentStr:='';
  for AValue := 1 to FIndenting do
    FIndentStr:=FIndentStr+'  ';
end;

procedure TRBILParser.SetIndexLevel(AValue: integer);
begin
  if AValue < 0 then AValue := 0;
  if AValue > 5 then AValue := 5;
  if FIndexLevel=AValue then Exit;
  FIndexLevel:=AValue;
end;

procedure TRBILParser.SetIndexLower(AValue: boolean);
begin
  if FIndexLower=AValue then Exit;
  FIndexLower:=AValue;
end;

function TRBILParser.GetFooter: String;
begin
  if FFooter = '' then
     FFooter := '<span class="crbil">' +
       replaceAll(' ', '&nbsp;', FRelease_Title + ' - ' +  FRelease_Version +
         ' ('+ FRelease_Date + ') ' + FRelease_Copyright) +
       ',</span><span class="cbreak"> </span><span class="chtml">' +
       replaceAll(' ', '&nbsp;', TextToHTML.asHTML(TextToHTML.Preprocess(
         'HTML conversion by ' + APP_PRODUCTNAME +
         ' - ' + APP_VERSION +
         ' (r' + SOURCE_REVISION +
         ') ' + APP_LEGALCOPYRIGHT))
         ) +
       '</span>';
  Result := FFooter;
end;

function TRBILParser.GetHeader: String;
begin
  if FHeader = '' then
    FHeader := '<span class="crbil">' +
      FRelease_Title + ' - ' +  FRelease_Version + ' ('+ FRelease_Date + ') ' +
      '</span>';
  Result := FHeader;
end;

procedure TRBILParser.SetBaseURL(AValue: String);
begin
  if FBaseURL=AValue then Exit;
  if AValue <> '' then
     FBaseURL:=IncludeLeading(DirectorySeparator, IncludeTrailingPathDelimiter(AValue))
  else
     FBaseURL:= '';
end;

{$IFOPT D+}
procedure TRBILParser.SetDebug(AValue: boolean);
begin
  if FDebug=AValue then Exit;
  FDebug:=AValue;
  // TextToHTML.EmbedCSS:=FDebug;
  TextToHTML.NoCache:=FDebug;
end;
{$ENDIF}

procedure TRBILParser.SetSrcDir(AValue: TPathString);
begin
  if FSrcDir=AValue then Exit;
  AValue:=ExpandFileName(AValue);
  if not DirectoryExists(AValue) then
    raise Exception.Create('Cannot open directory "' + AValue + '"')
  else
    FSrcDir:=AValue;
end;

procedure TRBILParser.ClearCurrent;
begin
    FCategory := '';
    FClass  := '';
    FTitle := '';
    FGroup := '';
    FSection := '';
    FSubFunc:='';
    FAltTitle := '';
end;

function TRBILParser.FindSourceFile(AFileName: String; ARaise: boolean): String;
begin
  FFileName := uPasExt.LocateFile(IncludeTrailingPathDelimiter(FSrcDir) + AFilename, ARaise);
  Result := FFileName;
end;

function TRBILParser.FetchMaxParts(AValue: String): integer;
var
  T : Text;
  S : String;
  I, E : integer;
begin
  System.Assign(T, AValue);
  Reset(T);
  ReadLn(T, S);
  while (Pos(' ', S) > 0) do Delete(S, 1, Pos(' ', S));
  Val(S, I, E);
  Close(T);
  Result := I;
  if E <> 0 then
    raise Exception.Create('invalid numeric format for part number in ' + AValue);
end;

procedure TRBILParser.AnalyzeVersion;
var
  F, S1 : String;
  T : Text;
begin
  F := LocateFile('INTERRUP.1ST', True);
  System.Assign(T, F);
  Reset(T);
  ReadLn(T, S1);
  ReadLn(T, FRelease_Copyright);
  Close(T);
  FRelease_Copyright :=  SpellCorrect(FRelease_Copyright);
  S1 := SpellCorrect(S1);
  FRelease_Date := Copy(S1, Pos('LAST CHANGE', Uppercase(S1)), Length(S1));
  SetLength(S1, Length(S1) - Length(FRelease_Date));
  FRelease_Date := Trim(FRelease_Date);
  FRelease_Version := Copy(S1, Pos( 'RELEASE', Uppercase(S1)), Length(S1));
  SetLength(S1, Length(S1) - Length(FRelease_Version));
  FRelease_Version := Trim(FRelease_Version);
  FRelease_Title := Trim(S1);
  WriteLn(StringOfChar('-', Length(FRelease_Copyright)));
  WriteLn(FRelease_Title);
  WriteLn(FRelease_Version);
  FPreTitle := 'RBIL' +
   Trim(Copy(FRelease_Version, Pos(' ',FRelease_Version), Length(FRelease_Version))) +
   ' - ';
  WriteLn(FRelease_Date);
  WriteLn(FRelease_Copyright);
  WriteLn(StringOfChar('-', Length(FRelease_Copyright)));
  FRelease_Title     := TextToHTML.asHTML(TextToHTML.Preprocess(FRelease_Title));
  FRelease_Version   := TextToHTML.asHTML(TextToHTML.Preprocess(FRelease_Version));
  FRelease_Date      := TextToHTML.asHTML(TextToHTML.Preprocess(FRelease_Date));
  FRelease_Copyright := TextToHTML.asHTML(TextToHTML.Preprocess(FRelease_Copyright));
  While Pos(' ', FRelease_Date) > 0 do
    FRelease_Date := Trim(Copy(FRelease_Date, Pos(' ', FRelease_Date), Length(FRelease_Date)));
end;

function TRBILParser.CheckDirectory(APath : String; ASub: String): String;
var
  P : integer;
begin
  Result := '';
  APath := IncludeTrailingPathDelimiter( IncludeTrailingPathDelimiter(APath) + ASub);
  P := 1;
  repeat
    P := Pos(DirectorySeparator, APath, P + 1);
    ASub := Trim(Copy(APath,1, P-1));
    if ASub = '' then Continue;
    if (not DirectoryExists(ASub)) and (not CreateDir(ASub)) then
      raise Exception.Create('Unable to create output directory "' + APath + '"')
  until P >= Length(APath);
  if not DirectoryExists(APath) then begin
      raise Exception.Create('Unable to create output directory "' + APath + '"');
      Result := '';
  end else
    Result:=APath;
end;

function TRBILParser.InterruptSubDir: string;
var
  I : integer;
  S : string;
begin
  S := 'unclassified';
  for I := Low(RBIL_SubCats) to High(RBIL_SubCats) do
    if Copy(RBIL_SubCats[I], 1, Length(FCategory) + 1) = FCategory + ',' then begin
      S := Copy(RBIL_SubCats[I], Length(FCategory) + 2, Length(RBIL_SubCats[I]));
      Break;
    end;
  Result := S;
end;

function TRBILParser.ScanForTables(ARef : String): integer;
var
  I, C, P, E : integer;
  S : String;
begin
  I := 1;
  C := 0;
  while I < FData.Count - 1 do begin
    P := Pos('(TABLE ', Uppercase(FData[I]));
    if P > 0 then begin
      E := Pos(')', FData[I], P);
      if E > 0 then begin
        Inc(C);
        S := Copy(FData[I], P + 7, E - P - 7);
        // Val(S, V, X);
        // if X = 0 then
        if Length(S) <> 5 then
          message(1, '"' + ExtractFileName( FFilename) + ' contains an invalid table id "' + S + '"');
        FTables.Add(S, ARef);
      end;
    end;
    Inc(I);
  end;
  Result := C;
end;

function TRBILParser.TitleStr(ATitle: String): String;
var
  S : String;
begin
  if ATitle = '' then
    S := FPreTitle + FTitle
  else
    S := FPreTitle + ATitle;
  if S = FPreTitle then
    S := replaceall(' - ', '', S);
  Result := S;
end;

function TRBILParser.TagTitle(ATitle: String): String;
begin
  Result := '<title>' + TitleStr(ATitle) + '</title>';
end;

procedure TRBILParser.TagTables;
var
  I, P, E, X : integer;
  N, S : String;
begin
  I := 1;
  while I < FData.Count - 1 do begin
    S := FData[I];
    P := Pos('(TABLE ', Uppercase(S));
    if P > 0 then begin
      E := Pos(')', S, P);
      if E > 0 then begin
        Inc(E);
        N := Copy(S, P + 7, E - P - 8);
        if E < Length(S) then
          Insert('</span>', S, E)
        else
          S := S + '</span>';
        Insert('<span class="doc-table" id="table-' + N + '-actual">', S, P);
        FData[I] := S;
        X := I;
        while (X > 1) and (Trim(FData[X]) <> '') and (X + 5 > I) do Dec(X);
        // if (X > 0) then begin // and (Trim(FData[X]) = '') then begin
          FData.Insert(X + 1, '<span class="ref-table" id="table-' + N + '"></span>');
          Inc(I);
        // end;
      end;
    end;
    Inc(I);
  end;
end;

procedure TRBILParser.TagOpcodes(ARef : String);
var
  I, P, E, X : integer;
  N, S : String;
begin
  I := 1;
  while (I < FData.Count) and (GetWord(FData[I],1) <> 'OPCODE') do Inc(I);
  Dec(I);
  while I < FData.Count - 1 do begin
    S := FData[I];
    While (Length(S) > 1) and (S[Length(S)] = ' ') do
     SetLength(S, Length(S) - 1);
    Inc(I);
    if (Length(S) < 10) or (FilterInclude(S, '-') <> S) then
      Continue;
    S := Trim(replaceAll(TAB, SPACE, FData[I]));
    repeat
      N := S;
      S := replaceAll(SPACE + SPACE, SPACE, S);
    until S = N;
    N := Uppercase(GetWord(S,1));
    if N = 'APPENDIX' then break;
    P := 1;
    if N = 'OPCODE' then
      P := Pos(' ', S) + 1;
    N := GetWord(S, 1, P);
    E := P + Length(N);
    N := Lowercase(N);
    if FPreProcess then begin
      S := WhenTrue(P = 1, 'OPCODE ') + S;
      // WriteLn(ARef + '#opcode-' + N + CRLF +  S);
      FInts.Add('o' + N, ARef + '#opcode-' + N + CRLF + S );
    end else begin
      if E < Length(S) then
        Insert('</span>', S, E)
      else
        S := S + '</span>';
      Insert('<span class="doc-opcode" id="opcode-' + N + '-actual">', S, P);
      FData[I] := '<span class="doc-opcode-title">' + S + '</span>';
      X := I;
      while (X > 1) and (Trim(FData[X]) <> '') and (X + 5 > I) do Dec(X);
      FData.Insert(X + 1, '<span class="ref-opcode" id="opcode-' + N + '"></span>');
      Inc(I);
    end;
    Inc(I);
  end;
end;

procedure TRBILParser.ParseSeeRef(ARef, ARel: String; var ARes, AQuoted : string);
var
  P : integer;
  X, CS : String;
  FI : boolean;
begin
  ARes := '';
  P :=  Pos(TextToHTML.UTF.DoubleQuote, ARef);
  if P > 0 then begin
    AQuoted := Copy(ARef, P);
    ARef := Copy(ARef, 1, P - 1);
  end else
    AQuoted := '';
  FI := False;
  CS := '-_';
  While ARef <> '' do begin
    X := Trim(PopWord(ARef, CS));
    P := Pos(' ', ARef);
    if P > 0 then
      SetLength(ARef, P - 1);
    while Pos(Copy(X, 1,1), '/=:@-_') > 0 do Delete(X,1,1);
    if FI then begin
      FI := False;
      case X of
        'AX', 'AH', 'AL',
        'BX', 'BH', 'BL',
        'CX', 'CH', 'CL',
        'DX', 'DH', 'DL',
        'SI',
        'DI',
        'BP',
        'DS',
        'ES'              : begin
          ARes := ARes + Copy(ExtractFileName(ARel),1,2); // ARel's INT number
        end;
      end;
    end;
    case Uppercase(X) of
      'AX', 'AH', 'AL' : X := '';
      'INT' : begin
        FI := True;
        X := '';
      end;
      'CMOS' : X := 'r';
      'CALL' : X := '@';
      'I2C'  : X := 'i';
      'MEM'  : X := 'm';
      'PORT' : begin
         X := 'p';
         CS := '_';
      end;
      'MSR'  : X := 's';
      'OPCODE'  : begin
        if ARef = '' then begin
          Delete(AQuoted, 1, Length(TextToHTML.UTF.DoubleQuote));
          P :=  Pos(TextToHTML.UTF.DoubleQuote, AQuoted);
          if P < 1 then
            P := Length(AQuoted) + 1;
          X := 'o' + copy(AQUoted, 1, P - 1);
          Delete(AQuoted, 1, P + Length(TextToHTML.UTF.DoubleQuote) - 1);
        end else
          X := 'o';  // need to have version for not qouted?;
      end;
    end;
    if X = '' then Continue;
    if (Length(X) mod 2 = 1) and (X[Length(X)] = 'h') then
      SetLength(X, Length(X) -1 );
    ARes := ARes + X;
  end;
  case ARes of
    'r.lst' : ARes := 'cmos.lst';
  end;
  ARes := lowercase(ARes);
end;

procedure TRBILParser.TagSeeRef(AStart, AEnd, ARel: String);
var
  I, P, E, X, EC, V : integer;
  N, S, A, K, Q : String;

  procedure Wrap(AValue, AID, AClass:String);
  var
    U : String;
  begin
    if AValue <> '' then begin
      message(4, FilterExclude(lowercase(AStart), '(:') {+ AClass + ' '} + AID +
        ' -> ' + AValue +  '#' + AID );
      U := '</a></span>';
      Insert(U, S, E);
      Inc(E, Length(U));
      { // Old Version
      U := '<span class="ref-' + AClass + '"><a href="' +
        replaceAll(DirectorySeparator, '..' + DirectorySeparator,
        FilterInclude(ARel, DirectorySeparator)) + AValue +
        WhenTrue(AID, '#' + AID) + '">';
      }
      U := '<span class="ref-' + AClass + '"><a href="' +
        MakeRelative(ARel, AValue +  WhenTrue(AID, '#' + AID)) + '">';
      Insert(U, S, P);
      FData[I] := S;
      Inc(E, Length(U)-1);
      Inc(FGoodLinks);
    end else begin
      message(2, FilterExclude(lowercase(AStart), '(:') {+ AClass + ' '} + AID +
        ' ? unknown');
      Inc(FBadLinks);
    end;
  end;

begin
  I := 1;
  while I < FData.Count - 1 do begin
    S := FData[I];
    P := 1;
    while (P < Length(S)) and (P > 0) do begin
      P := Pos(AStart, Uppercase(S), P);
      if P > 0 then begin
        // References known to exist
        Inc(P, Length(AStart));
        E := 1;
        while (P < Length(S)) and (E > 0) do begin
          if AEnd = '' then
            E := Length(S) + 1
          else
            E := Pos(AEnd, Uppercase(S), P);
          // Current reference thru end of all references known
          if E > 0 then begin
            EC := Length(AEnd);
            if EC < 1 then EC  := 1;
            X := Pos(',', S, P);
            if (X > 0) and (X < E) then begin
              E := X;
              EC := 1;
            end;
            // Entire current reference is known P thru E
            // trim leading spaces
            while Copy(S, P, 1) = ' ' do Inc(P);
            // if starts with also, skip that word
            if uppercase(GetWord(Copy(S, P, 5), 1)) = 'ALSO' then
              Inc(P, 4);
            while Copy(S, P, 1) = ' ' do Inc(P);
            // if ref contains the word AT, break it at that point to lookup
            // as separate references
            X:=Pos(' AT ', Uppercase(Copy(S, P, E - P)));
            if (X > 0) and (X < E) then begin
              E := P + X - 1;
              EC := 4; // AT is 4 chars, will auto-reset
            end;
            // Only current reference to lookup is now P thru E
            N := Copy(S, P, E - P);
            if Trim(N) = '' then begin
              // Nothing to see here, move along.
            end else
            if (Pos('.', N) > 1) and DirectoryExists(FOutDir + Lowercase(GetWord(N, 1))) then begin
              // K := 'INDEX';    // Not needed
              Wrap(IncludeTrailingPathDelimiter(Lowercase(GetWord(N, 1))) + 'index' + HTMLExt, '', 'index');
            end else
            if Copy(N, 1,1) = '#' then begin
              // K := 'TABLE';    // Not needed
              N := Trim(Copy(N, 2));
              A := N;
              V := FTables.Find(Popword(A));
              if V >= 0 then
                A := FTables.Values[V]
              else
                A := ''; // Just so wrap throws a warning message.
              Wrap(A, 'table-' + N, 'table');
            end else begin
              // Figure out what primary group see is refering too.
              case uppercase(GetWord(N, 1)) of
                 'INT', 'MEM', 'CALL', 'PORT', 'MSR', 'I2C', 'CMOS', 'OPCODE' : begin
                   K := Popword(N);
                 end;
              else
                K:=uppercase(GetWord(ARel, 1));
                case K of
                  'MEMORY'   : K := 'MEM';
                  'FARCALL'  : K := 'CALL';
                  'INTERRUP' : K := 'INT';
                  'PORTS'    : K := 'PORT';
                  'CMOS',
                  'MSR',
                  'I2C',
                  'OPCODE'   : begin end;
                else
                  Message(2, 'Confused: Non-standard source link type in "' + ARel + '" for coversion of "' + N + '"' );
                end;
              end;
              // Ok, we know the primary category and stuff.
              // We aren't refering to a table or file. So, we need find the INT
              // type block we need.
              Q := '';
              ParseSeeRef(K + ' ' + N, ARel, A, Q);
              V := FindRef(A, Q);
              if V < 0 then begin
                Message(2, 'Unable to locate link target "' +
                  replaceall(TextToHTML.UTF.DoubleQuote, '"', K + ' ' + N + '" (' + A +
                  WhenTrue (Q, ' ' + Q )) +') for entry "' + FSection + '"' );
                A := '<!-- Could not find reference: ' +
                  replaceall(TextToHTML.UTF.DoubleQuote, '"', K + ' ' + N + ' ' + #$27 + A + #$27 + '') +
                  ' in ' + FSection + ' -->';
                Insert(A, S, E);
                FData[I] := S;
                Inc(E, Length(A));
                Inc(FBadLinks);
              end
              else begin
                A := FInts.Values[V];
                N := PopLine(A);
                PopLine(A);
                Wrap(N + WhenTrue(A, '#sect-' + A ), '', lowercase(K));
              end;
            end;
            P := E+EC- 1;
          end;
          Inc(P);
        end;
      end;
    end;
    Inc(I);
  end;
end;

procedure TRBILParser.TagSeeAlso(ARel: String);
begin
  indent;
  TagSeeRef('SEEALSO: ', '', ARel);
  TagSeeRef('(SEE ', ')', ARel);
  outdent;
end;

procedure TRBILParser.TagRefItems(ARel : String; var S : TStringList);
begin
  if FPreProcess then Exit;
  TagRefInt(ARel, S);
end;

procedure TRBILParser.TagRefInt(ARel : String; var S : TStringList);
const
  IgnoreTags : TStringArray = ('a');
  IgnoreClasses : TStringArray = ('abbrev', 'definition' , 'part-title');
begin
  TextToHtml.ForText(MakeRelative(FOutDir, ARel), S, @OnTagRefInt, IgnoreTags, IgnoreClasses);
end;

function TRBILParser.FindRef(ARef, AExtra: String): integer;
var
  D, T : String;
  I : integer;
  RScore, IScore, EScore : integer;
begin
  AExtra := ReplaceAll(TextToHTML.UTF.DoubleQuote, '', AExtra);
  Result := FInts.Find(ARef);
  if (Result < 0) or (AExtra <> '') then begin
    // WriteLn('Search: ', ARef, ' ', AExtra, ' ', WhenTrue(AExtra, 'best', 'any'), ' match');
    EScore := Length(ARef);
    if AExtra <> '' then
      Inc(EScore, Length(AExtra));
    if Result >= 0 then
      RScore := MatchLength(ARef, FInts.Names[Result]);
    I := 0;
    repeat
      D := FInts.Values[I];
      PopLine(D);
      T := PopLine(D);
      IScore := MatchLength(FInts.Names[I], ARef);

      if  (Pos(AExtra, T) > 0) then
        Inc(IScore, Length(AExtra))
      else if (Pos(Uppercase(AExtra), Uppercase(T)) > 0) then
        Inc(IScore, Length(AExtra) div 2);

      if IScore = Escore then begin
       // WriteLn('(',IScore, ') Exact Match: ', FInts.Names[I], ', ', T);
        Result := I;
        Break;
      end else
      if IScore > RScore then begin
       // WriteLn('(', IScore, ') Better Match: ', FInts.Names[I], ', ', T);
        RScore := IScore;
        Result := I;
      end else
      if IScore > 2 then begin
       // WriteLn('(', IScore, ') Worse Match: ', FInts.Names[I], ', ', T);
      end;
      Inc(I);
    until I >= FInts.Count;
  end;

end;

procedure TRBILParser.PrepDirTitles;
var
  G, I {,X} : integer;
  N, S : String;
begin
  indent;
  Message(4, 'Basic directory name mapping');
  indent;
  FTitle := '';
  FAltTitle := '';
  SetDocTitle('/', Homepage_Title);
  SetDocTitle('chars/', Charspage_Title);
  for G := low(RBIL_Groups) to High(RBIL_Groups) do begin
    SetDocTitle(RBIL_Groups[G][0] + DirectorySeparator, RBIL_Groups[G][1]);
    for I := low(RBIL_SubCats) to high(RBIL_SubCats) do begin
      { N := RBIL_SubCats[I];
      Delete(N, 1, Pos(',', N));
      S := TitleCase(ReplaceAll('_', ' ', N));
      for X := Low(RBIL_Groups_Adj) to High(RBIL_Groups_Adj) do
        S := ReplaceAll(RBIL_Groups_Adj[X,0], RBIL_Groups_Adj[X,1], S);
      N := RBIL_Groups[G][0] + DirectorySeparator + N + DirectorySeparator;
      SetDocTitle(N,S);
      }
       // categories not yet initialized, do this later
      N := RBIL_SubCats[I];
      S := TitleCase(FCategories[ord(N[1])]);
      Delete(N, 1, Pos(',', N));
      N := RBIL_Groups[G][0] + DirectorySeparator + N + DirectorySeparator;
      Message(4, N + ' > ' + S);
      SetDocTitle(N,S);
    end;
  end;
  outdent;
  outdent;
end;

procedure TRBILParser.InitWebVars;
begin
  FWebVars.Clear;
  With FWebVars do begin
    Add(WebVarChar + 'LINK_RBROWN' + WebVarChar,RBROWN_LINK);
    Add(WebVarChar + 'LINK_RBIL_REMOTE' + WebVarChar, RBIL_REMOTE_LINK);
    Add(WebVarChar + 'LINK_PROGRAM' + WebVarChar, SOURCE_URL );
    Add(WebVarChar + 'LINK_LAZARUS' + WebVarChar, 'https://www.lazarus-ide.org' );
    Add(WebVarChar + 'LINK_FPC' + WebVarChar, 'https://www.freepascal.org' );
    Add(WebVarChar + 'PROGRAM_NAME' + WebVarChar,  APP_PRODUCTNAME);
    Add(WebVarChar + 'PROGRAM_VERSION' + WebVarChar, APP_VERSION);
    Add(WebVarChar + 'PROGRAM_REVISION' + WebVarChar, SOURCE_REVISION);
    Add(WebVarChar + 'PROGRAM_COPYRIGHT' + WebVarChar,
      TextToHtml.asHTML(TextToHTML.Preprocess(APP_LEGALCOPYRIGHT)));
    Add(WebVarChar + 'LAZARUS_VERSION' + WebVarChar, LAZARUS_VERSION);
    Add(WebVarChar + 'LAZARUS_REVISION' + WebVarChar, LAZARUS_REVISION);
    Add(WebVarChar + 'FPC_VERSION' + WebVarChar, FPC_VERSION);
    Add(WebVarChar + 'FPC_REVISION' + WebVarChar, FPC_REVISION);
  end;
end;

constructor TRBILParser.Create;
begin
  inherited Create;
  FBaseURL := '';
  FSaveIndex := False;
  FData := TStringList.Create;
  FComments := TStringList.Create;
  FAbbr := TDictionary.Create;
  FAbbr.ExtraWordChars := DictionaryExtraChars;
  FDef := TDictionary.Create;
  FDef.ExtraWordChars := DictionaryExtraChars;
  FTables := TDictionary.Create;
  FInts := TDictionary.Create;
  FInts.Duplicates:=dupAccept;
  FIndexer := TDictionary.Create;
  FIndexer.Duplicates:=dupAccept;
  FIntFunc := TDictionary.Create;
  FIgnore := TStringList.Create;
  FIgnore.Duplicates:=dupIgnore;
  FIgnore.Sorted:=True;
  FDocs := TDictionary.Create;
  FWebVars := TDictionary.Create;
  FWebPages := TDictionary.Create;
  FDocTitles := TDictionary.Create;
  FSrcDir := IncludeTrailingPathDelimiter(ExpandFileName(''));
  FOutDir := '';
  FFileName := '';
  FSecCount := 0;
  FSecLine := 0;
  FSubFunc:='';
  FIndenting := 0;
  FIndentStr := '';
  FRelease_Title := '';
  FRelease_Version := '';
  FRelease_Date := '';
  FRelease_Copyright := '';
  FSkipMany := true;
  ClearArray(FFlags, 256);
  ClearArray(FFlagsShort, 256);
  ClearArray(FCategories, 256);
  ClearCurrent;
  FPreprocess := True;
  FIndexLevel := 2;
  FPostProcess := False;
  FHeader := '';
  FFooter := '';
  FPreTitle := '';
  FAltTitle := '';
  FPages := 0;
  FNavLinks := 0;
  FGoodLinks := 0;
  FBadLinks := 0;
  FCSSDATA := '';
  FIdxID := 0;
  FIdxCnt := 0;
  FIndexLower := False;
  {$IFOPT D+}
  FDebug := False;
  TextToHTML.EmbedCSS:=False;
  TextToHTML.NoCache:=False;
  {$ENDIF}
end;

destructor TRBILParser.Destroy;
begin
  FreeAndNil(FWebPages);
  FreeAndNil(FWebVars);
  FreeAndNil(FData);
  FreeAndNil(FComments);
  FreeAndNil(FAbbr);
  FreeAndNil(FDef);
  FreeAndNil(FInts);
  FreeAndNil(FIndexer);
  FreeAndNil(FDocs);
  FreeAndNil(FTables);
  FreeAndNil(FDocTitles);
  FreeAndNil(FIntFunc);
  FreeAndNil(FIgnore);
  // Not really needed, these should be freed automatically
  SetLength(FFlags, 0);
  SetLength(FFlagsShort, 0);
  SetLength(FCategories, 0);
  inherited Destroy;
end;

procedure TRBILParser.Indent;
begin
  Indenting := Indenting + 1;
end;

procedure TRBILParser.Outdent;
begin
  Indenting := Indenting - 1;
end;

procedure TRBILParser.Message(AValue: String; ACRLF: boolean);
begin
  Message(1, AValue, ACRLF);
end;

procedure TRBILParser.Message(Level: integer; AValue: String; ACRLF: boolean);
begin
  if Verbose >= Level then begin
    if ACRLF then
      WriteLn(FIndentStr, AValue)
    else
      Write(FIndentStr, AValue);
  end;
end;

procedure TRBILParser.DoRun;
begin
  FRBILDir :=
    ExtractFilePath(
      ExcludeTrailingPathDelimiter(
        ExtractFilePath(
          FindSourceFile('INTERRUP.1ST', true)
        )
      )
    );
  PrepIndexScan;
  AnalyzeVersion;
  ProcessInterrupts;
  PrepDirTitles;
  InitWebVars;
  {$IFNDEF DEV}
  FPreprocess := False;
  IndexPrune;
  OutputIndexWords;
  MakeCSS;
  ProcessInterrupts;
  FPostProcess := True;
  UnicodeTables;
  AsciiTables;
  MakeExtraViews;
  ProcessWebPages(True);
  ForFiles(@FPIndex);
  ProcessWebPages(False);
  ForFiles(@FPNavigation);
  {$ENDIF}
   PostRun;
end;

procedure TRBILParser.ProcessInterrupts;
var
  S : String;
begin
  FSecCount := 0;
  S := FindSourceFile(CONFIG_FILE, true);
  if S <> '' then ProcessFile(S);
  S := FindSourceFile(USER_FILE, false);
  if S <> '' then ProcessFile(S);
  {$IFOPT D+}
  if FDebug and (Not FPreProcess) then
    ProcessFileList(DEBUG_Files, True)
  else
  {$ENDIF}
  ProcessFileList(RBIL_Files, True);
  ProcessFileList(MORE_Files, False);
end;

procedure TRBILParser.ProcessFileList(AValue: TStringArray; ARequired: boolean
  );
var
  I, J, C : integer;
  S, E, N : String;
begin
  for I := Low(AValue) to High(AValue) do begin
    C:=0;
    S:=AValue[I];
    E:=Uppercase(ExtractFileExt(S));
    SetLength(S, Length(S) - Length(E));
    ProcessFile(FindSourceFile(AValue[I], ARequired));
    if E = '.A' then begin
       Inc(C);
       for J := 2 to FetchMaxParts(FindSourceFile(S + '.B', ARequired or true)) do begin
          ProcessFile(FindSourceFile(S + '.' + Chr(64 + J), ARequired or true));
          Inc(C);
       end;
    end;
    // Parts that are completely optional and not part o the original RBIL
    if (E = '.A') or (E = '.LST') then begin
        for J := C + 1 to 26 do begin
            N:=FindSourceFile(S + '.' + Chr(64 + J),false);
                if  FileExists(N) then
            ProcessFile(N)
                  else
                        Break;
            end;
    end;
  end;
end;

procedure TRBILParser.ProcessFile(AValue: String);
var
  T : Text;
  S, HS, HH, LC, SS : String;
  X : boolean;
  H : boolean;
  TextType : integer;
  Line : integer;
begin
  // WriteLn(FIndexer.Count);
  if not FileExists(AValue) then Exit;
  Indent;
  if FPreprocess then
    Message(2,'preprocess scan "' + ExtractFileName(AValue) + '"')
  else
    Message(1,'processing "' + ExtractFileName(AValue) + '"');


  ClearCurrent;
  FData.Clear;
  System.Assign(T, AValue);
  Reset(T);
  X := False;
  TextType := 0;
  H := Pos(';' + Uppercase(ExtractFileName(AValue)) + ';', RBIL_Docs) > 0;

  FComments.Clear;
  Line := 0;
  FSecLine := 0;
  HH := '';
  while not EOF(T) do begin
    if HH <> '' then begin
      S := HH;
      HH := '';
    end else
      ReadLn(T, S);
    Inc(Line);
    if TextType = 0 then begin
      SS := S;
      S := SpellCorrect(S, ExtractFileName(AValue), Line, False);
      if S = #0 + 'delete' then Continue;
      if copy(S, 1, 7) = #0 + 'insert' then begin
        FData.Add(Copy(S, 8, Length(S)-7));
        HH := SS;
        continue;
      end;
    end;
    if H and (FData.Count = 0) then begin
      HH := S;
      HS := Trim(Copy(S, 1, Pos('RELEASE', Uppercase(S)) -1));
      S := Uppercase(ExtractFileName(AValue));
      S := Copy(S, 1, Pos('.', S)-1);
      S := CategorySeparator + '!---' + S + CategorySeparator;
    end;

    if IsDivider(S) then begin
      LC := Uppercase(FClass);
      case LC of
        'HTMLTEXT' : TextType := 1;
        'RAWTEXT'  : TextType := 2;
        'CORRECT',
        'ASISTEXT',
        'CSSDATA'  : TextType := 3;
      else
        TextType := 0;
      end;
      if X then begin
        ProcessSection;
        FData.Clear;
        FSecLine := Line;
      end;
      X := True;
    end else
      case TextType of
        1: S := TextToHTML.asHTML(S); // HTML Text
        2, 3 : begin { Don't do anything to it here } end; // RAW & AS-IS Text;
      else
        S:=SpellCorrect(TextToHTML.asHTML(TextToHTML.Preprocess(S)),
          ExtractFileName(AValue), Line, True);
      end;

    if X then begin
      FData.Add(S);
      if H and (HS <> '') then begin
        FData.Add(HS);
        FData.Add('');
        HS:='';
      end;
    end else
      FComments.Add(S);
  end;
  ProcessSection;
  Close(T);
  Outdent;
  SimplifyIndex;
end;

function TRBILParser.IsDivider(AValue: String): boolean;
var
  T : String;
  I : integer;
begin
     Result := (Copy(AValue, 1, Length(CategorySeparator)) = CategorySeparator)
     and (Length(AValue) >= 9);
     if not Result then exit;
     T := AValue;
     while Pos('-', T) > 0 do Delete(T, Pos('-', T), 1);
     Result := Trim(T) <> '';
     if not Result then exit;
     ClearCurrent;
     FCategory := AValue[9];
     if FCategory = '-' then FCategory := '*';
     if FCategory = '!' then
       FClass := Copy(AValue, 13, Length(AValue))
     else
       FClass := Copy(AValue, 9, Length(AValue));
     While (FClass <> '') and (FClass[Length(FClass)] = '-') do
       SetLength(FClass, Length(FClass) - 1);
     FGroup := ExtractFileName(FFileName);
     SetLength(FGroup, Length(FGroup) - Length(ExtractFileExt(FGroup)));
     if FClass = '' then FClass := FGroup;
     while Pos('_', FClass) = 1 do Delete(FClass, 1, 1);
     FSection := LTrim(Rtrim(FClass, SPACE + TAB + '_-'), SPACE + TAB + '_-');
     FTitle:=TitleCase(Lowercase(Trim(Scrunch('-', ' ', FClass, 2))));
     if FCategory <> '!' then
       FClass := InterruptSubDir;
     for I := low(RBIL_TITLES) to High(RBIL_TITLES) do
       if Uppercase(RBIL_TITLES[I][0]) = Uppercase(FTitle) then begin
         FTitle := RBIL_TITLES[I][1];
         Break;
       end;
end;

procedure TRBILParser.TagAll(FD : TStringList);
begin
  if not Assigned(FD) then FD := FData;
  TextToHtml.TagAll(FD, 1);
  TextToHtml.TagDictionary(FD, FDef, 'definition', 'span', 1);
  TextToHtml.TagDictionary(FD, FAbbr, 'abbrev', 'span', 1);
end;

procedure TRBILParser.ProcessSection;
const MC : integer = 0;
begin
  Inc(FSecCount);
  while (FData.Count > 1) and (Trim(FData.Strings[1]) = '') do  FData.Delete(1);
  if FData.Count = 0 then exit;
  if Trim(FData[FData.Count - 1]) <> '' then
    FData.Add(System.StringOfChar(' ', 80))
  else
    FData[FData.Count - 1] := System.StringOfChar(' ', 80);
  IsDivider(FData.Strings[0]);
  if FPreProcess then IndexScan;
  case FCategory of
    '!' : CommentSection;
  else
     if (Length(FCategory) > 0) and (FCategories[Ord(FCategory[1])] <> '') then
        ItemBlock
     else begin
       Inc(MC);
       if MC < 10 then
         Message(3,'Unknown category: ' + FCategory + ' / ' + FData.Strings[0])
       else if MC = 10 then
         Message(3,'Unknown category: (additional messages trucated)');
     end;
  end;

end;

procedure TRBILParser.CommentSection;
begin
  if FComments.Count > 0 then
    RawTextSection;

  if FPreprocess and ( not (
    (FClass = 'ABBREVIATIONS') or
    (FClass = 'GLOSSARY') or
    (FClass = 'FLAGS') or
    (FClass = 'CATEGORIES') or
    (FClass = 'INDEX_INCLUDE') or
    (FClass = 'INDEX_EXCLUDE') or
    (FClass = 'CORRECT') or
    (FClass = 'CSSDATA')
    )) then exit;
  Indent;

  case Uppercase(FClass) of
     'SECTION',
     'ADMIN',
     'FILELIST' : begin {do nothing} end;
  else
     Message(2, 'Section: ' + FClass);
  end;

  case Uppercase(FClass) of
    'INDEX_INCLUDE' : IndexIncludeSection;
    'INDEX_EXCLUDE' : IndexExcludeSection;
     'FLAGS'        : FlagsSection;
     'CATEGORIES'   : CategoriesSection;

     'ABBREVIATIONS': AbbreviationsSection;
     'TRADEMARKS'   : TrademarksSection;
     'QUOTES'       : QuotesSection;
     'HISTORY'      : HistorySection;
     'CREDITS'      : CreditsSection;
     'COPYRIGHT'    : CopyrightSection;
     'ADDRESSES'    : AddressesSection;

     // Many files contain short contact info sections, ignore those.
     // Take first big contact section.
     'CONTACT_INFO' : if FData.Count > 5 then RawSection;

     // I2C.LST contains a BIBLIOGRAPY section that only says (see BIBLIO.lST)
     'BIBLIOGRAPHY' : if FData.Count > 2 then SimpleSection(99);
     'GLOSSARY'     : GlossarySection;
     'SMM'     : begin
         FData.Strings[1] := TextToHtml.Tag(FData.Strings[1], 'header-normal', 'span');
         RawSection;    // Do later more
       end;

     'DISCLAIMER'   : SimpleSection(0);

     'NOTE'         : NoteSection;
     'TABLES'       : TablesSection;

     'TITLES'       : TitlesSection;

     'CONTRIBUTORS' : RawSection(lowercase(FGroup));

     '86BUGS',
     'ADVERT',
     'AVAILABILITY',
     'FAQ',
     'INTERRUP',
     'RBROWN',
     'README',
     'NEEDHELP'     : RawSection;

     'SECTION',
     'ADMIN',
     'FILELIST'     : IgnoreSection;

     'WEBTEXT',
     'RAWTEXT',
     'HTMLTEXT',
     'ASISTEXT'     : WebPageSection;
     'CSSDATA'      : CSSDataSection;
     'CORRECT'   : CorrectionSection;
  else
      Message('Method for handling ' + FClass + ' section is not known.');
  end;
  OutDent;
end;

procedure TRBILParser.IgnoreSection;
begin
  // Section ignored.
end;

procedure TRBILParser.RawSection(ASubDir: String; PreTag: boolean);
begin
  TagAll;
  if PreTag then begin
    FData.Insert(1, '<pre>');
    FData.Add('</pre>');
  end;
  RawDoc;
  if PreTag then begin
    SimpleHTML(ASubDir, @PDUnchanged, false)
  end else
    SimpleHTML(ASubDir, @PDTextBlocks, True);
end;

procedure TRBILParser.RawSection(PreTag : boolean);
begin
  RawSection('', PreTag);
end;

procedure TRBILParser.SimpleSection(ASubDir: String; AMethod: integer);
begin
  TextToHtml.UnWordWrap(FData, AMethod, 1);
  TagAll;
  StandardDoc;
  SimpleHTML(ASubDir, @PDTextBlocks, True);
end;

procedure TRBILParser.SimpleSection(AMethod : integer);
begin
  SimpleSection('', AMethod);
end;

procedure TRBILParser.StandardDoc;
begin
  FData.Insert(1, '<div class="stddoc-outer">' + CRLF +
    '<div class="stddoc-inner">' + CRLF + '<div class="stddoc">');
  FData.Add('</div>' + CRLF + '</div>' + CRLF + '</div>');
end;

procedure TRBILParser.RawDoc;
begin
  FData.Insert(1, '<div class="rawdoc-outer">' + CRLF +
    '<div class="rawdoc-inner">' + CRLF + '<div class="rawdoc">');
  FData.Add('</div>' + CRLF + '</div>' + CRLF + '</div>');
end;

procedure TRBILParser.CSSDataSection;
var
  I : Integer;
begin
  if not FPreProcess then Exit;
  if FCSSDATA <> '' then
    FCSSDATA := FCSSDATA + CRLF;
  FCSSDATA := FCSSDATA +
    '/* ' + ExtractFIleName(FFilename) + ', Line:' + IntToStr(FSecLine) +
    ' */' + CRLF;

  While (FData.Count > 1) and (Trim(FData[1]) = '') do
    FData.Delete(1);
  While (FData.Count > 1) and (Trim(FData[FData.Count - 1]) = '') do
    FData.Delete(FData.Count -1);
//  FData.Delete(1);

  for I := 1 to FData.Count - 1 do
    FCSSDATA := FCSSDATA + TrimRight(FData[I]) + CRLF;
end;

procedure TRBILParser.CreditsSection;
const
  Phase : integer = 0;
var
  I, P : integer;
  X : String;

  function NoBreak(AFirst, ASecond : String) : boolean;
  var
    R, E : integer;
  begin
    NoBreak := True;
    R := Pos(AFirst, FData.Strings[I], P);
    if R < 1 then E := -1 else begin
      E := Pos(ASecond, FData.Strings[I], R);
      if E < 1 then E := Length(FData.Strings[I]);
    end;
    P := Pos(',', FData.Strings[I], P);
    if (P > 0) and (R < P) and (E > P) then
      P := E
    else
      NoBreak:=false;
  end;

begin
  Inc(Phase);
  // FData.Insert(2, 'Phase' + IntToStr(Phase));
  case Phase of
    1 : begin
       FData.Strings[1] := TextToHtml.Tag(FData.Strings[1], 'intro-text');
       I := 20;
        while (I < FData.Count) and (FData.Strings[I] <> '') do Inc(I);
        I := I + 2;
        while (I < FData.Count -1) do begin
          FData.Strings[I] := FData.Strings[I] + ' ' + FData.Strings[I+1];
          FData.Delete(I+1);
        end;
        while I < FData.Count do begin
          P := 1;
          while NoBreak('(', ')') or NoBreak('<', '>') do;
          if P > 0 then begin
             X := Copy(FData.Strings[I], P+1, Length(FData.Strings[I]));
             FData.Strings[I] := Copy(FData.Strings[I], 1, P -1);
             if I < FData.Count - 1 then
               FData.Insert(I + 1, X)
             else
               FData.Add(X);
          end;
          inc(I);
        end;
    end;
    2 : TexttoHTML.UnWordWrap(FData, 99, 1);
    3 : TexttoHTML.UnWordWrap(FData, 1, 1);
  end;
  TagAll;
  StandardDoc;
  SimpleHTML(@PDTextBlocks)
end;

procedure TRBILParser.CopyrightSection;
begin
  TextToHtml.UnWordWrap(FData, 99, 1);
  TagAll;
  FData.Strings[1] := TextToHtml.Tag(FData.Strings[1], 'heading-warning', 'span');
  StandardDoc;
  SimpleHTML(@PDTextBlocks);
end;

procedure TRBILParser.AddressesSection;
var
  I : integer;

begin
  TagAll;
  I := 1;
  FData.Strings[I] := TextToHtml.BeginTag(FData.Strings[I], 'intro-text');
  Inc(I);
  while I < FData.Count do begin
    if (Trim(FData.Strings[I]) = '' ) then begin
      // ignore blank lines;
    end else if (Trim(FData.Strings[I]) <> '' ) and (Trim(FData.Strings[I-1]) = '' )then begin
      FData.Strings[I]:=TextToHtml.Tag(FData.Strings[I],'address-attn');
      FData.Insert(I, TextToHtml.BeginTag('', 'address-block'));
      FData.Insert(I, TextToHtml.EndTag(''));
      I:=I + 2;
    end else
       FData.Strings[I]:=TextToHtml.Tag(FData.Strings[I],'address-where');
    Inc(I);
  end;
  FData.Add(TextToHtml.EndTag(''));
  TextToHtml.RemoveEmptyLines(FData);
  StandardDoc;
  SimpleHTML(@PDUnchanged, false);
end;

procedure TRBILParser.TrademarksSection;
var
  I, X, O : integer;
  S,R :String;
  K, A, B : String;
begin
  TextToHtml.RemoveEmptyLines(FData);
  I := 2;
  FData.Strings[1] := TextToHtml.Tag(FData.Strings[1], 'intro-text');
  while I <= FData.Count - 1 do begin
    S:=FData.Strings[I];
    // Last line is wrapped so fix it. Also de-wrap any line that doesn't contain
    // is, are or was. (Unless it starts with Microsoft)
    if Pos('Microsoft', S) = 1 then begin
      FData.Strings[I] := S + ' ' + Trim(FData.Strings[I+1]);
      FData.Delete(I+1);
      S:=FData.Strings[I];
    end;

    if ((Pos(' is ', S) <= 0) and (Pos(' are ', S) <= 0) and (Pos(' was ', S) <= 0))
    or (I>=FData.Count-1) then begin
      FData.Strings[I-1] := FData.Strings[I-1] + ' ' + Trim(S);
      FData.Delete(I);
    end else
      Inc(I);
  end;

  I := 2;
  while I <= FData.Count - 2 do begin
    S:=FData.Strings[I];
    R:='';
    if Pos('registered', S) >0 then begin
      R:='registered-';
      K := '&reg;';
    end else
      K := '&trade;';
    X := Pos(' is ', S);
    if X <= 0 then X := Pos(' are ', S);
    if X <= 0 then X := Pos(' was ', S);
    O := Pos(' of ', S, X);
    A := Trim(Copy(S, 1, X));
    B := Trim(Copy(S, O + 4, Length(S)));
    S := Trim(Copy(S, X, O - X + 1));
    A := ReplaceAll(', ', K + ', ', A);
    A := ReplaceAll(' and ', K + ' and ', A);
    FData.Strings[I]:= '<span class="' + R + 'trademark">' + A + K + '</span> ' +
     S + ' <span class="trademark-holder">' + B + '</span>';
    Inc(I);
  end;

  FData.Insert(2, '');
  FData.Insert(FData.Count -1, '');
  StandardDoc;
  SimpleHTML(@PDTextBlocks);
end;

procedure TRBILParser.QuotesSection;
var
  I, P, L : integer;
  S, X : String;
begin
  I := 2;
  TextToHtml.UnWordWrap(FData, 0, 2);
  FData.Strings[1] := TextToHtml.Tag(FData.Strings[1], 'intro-text');
  while (I < FData.Count) do begin
    S := FData.Strings[I];
    P := Pos(TextToHTML.UTF.DoubleQuote, S);
    if P > 0 then begin
      X := TextToHtml.Tag('', 'quote-text');
      Insert(X, S, P);
      L := -1;
      while P > 0 do begin
        L := P;
        inc(p);
        P := Pos(TextToHTML.UTF.DoubleQuote, S, P);
      end;
      if (L <> -1) and (L < Length(S)) then
        insert(TextToHtml.EndTag('')+'<br>', S, L + Length(TextToHTML.UTF.DoubleQuote))
      else
        S := S + TextToHtml.EndTag('');
    end;
    if (trim(S) <> '') then
      S := '<span class="quote-tribute">'+trim(S)+'</span>';
    FData.Strings[I]:=S;
    Inc(I);
  end;
  FData.Insert(2, '');
  TagAll;
  StandardDoc;
  SimpleHTML(@PDTextBlocks);
end;

procedure TRBILParser.FlagsSection;
var
  I : integer;
begin
  ProcessList(FFlags, 2);
  FFlagsShort := FFlags;
  for I := Low(ShortFlags) to High(ShortFlags) do
     FFlagsShort[Ord(ShortFlags[I,0][1])] := ShortFlags[I,1];
  RawSection;
end;

procedure TRBILParser.CategoriesSection;
begin
  ProcessList(FCategories, 1);
  RawSection;
end;

procedure TRBILParser.TablesSection;
var
  I : integer;
  M : Integer;
  S : String;
begin
  M := 0;
  I := 1;
  while I < FData.Count do begin
     S := FData.Strings[I];
     if Trim(S) = '' then begin
        if M <> 0 then begin
          S := TextToHtml.EndTag('', 'div');
          M := 0;
        end
     end else
       case M of
           0: begin
             FData.Insert(I, TextToHtml.BeginTag('', 'tables-group', 'div'));
             M := 1;
             Inc(I);
             Continue;
           end;
           1: begin
             S := TextToHtml.Tag(FData.Strings[I],'tables-heading');
             M := 2;
           end;
           2 : S := TextToHtml.Tag(FData.Strings[I], 'tables-item');
       end;
     FData.Strings[I] := S;
     Inc(I);
  end;
  if M <> 0 then
    FData.Add(TextToHtml.EndTag('', 'div'));
  indent;
  TagSeeAlso('tables' + HTMLExt);
  outdent;
  TagAll;
  FData.Insert(1, '<pre>');
  FData.Add('</pre>');
  SimpleHTML('', @PDUnchanged, True);
end;

procedure TRBILParser.TitlesSection;
begin
  FGroup := '';
  FClass := 'Note';
  FTItle := 'Title Notes';
  FAltTitle := '';
  RawSection;
end;

procedure TRBILParser.HistorySection;
begin
  TagAll;
  FData.Strings[1] := TextToHtml.Tag(FData.Strings[1], 'intro-text');
  StandardDoc;
  SimpleHTML(@PDTextBlocks);
end;

procedure TRBILParser.AbbreviationsSection;
var
  I : integer;
  A, B : String;
begin
  Indent;
  if FPreprocess then begin
    I := 3;
    While I < FData.Count do begin
      B := FData.Strings[I];
      FData.Delete(I);
      if Trim(b) = '' then continue;
      if (Copy(B,1,1) <> #9) and (Copy(B,1,2) <> '  ') then continue;
      B := Trim(B);
      if Pos(#9, B) > 0 then
          A := Copy(B, 1, Pos(#9, B) - 1)
      else
          A := Copy(B, 1, Pos(#32, B) - 1);
      B := Trim(Copy(B, Length(A) + 1, Length(B)));
      if A = 'TopView' then
        B := ReplaceAll('/', ', ', B);
      FAbbr.Add(A, B);
      A := FilterExclude(A, '+');
      if FIndexLevel > 0 then
        FIndexer.Add(A, '99'); // Less than 100 are counted, if real < min
                               // So if 1 or more existas, then is indexed.
    end;
    for I := 0 to FAbbr.Count - 1 do
      Message(3, FAbbr.Names[I] + ' > ' + FAbbr.Values[I]);

  end else if (ExtractFileName(FFileName) <> CONFIG_FILE) and
  (ExtractFileName(FFileName) <> USER_FILE) then begin
    FData.Strings[1] := TextToHtml.Tag(FData.Strings[1], 'intro-text');
    I := 2;
    While I < FData.Count do FData.Delete(I);
    FData.Add('</p><p><table class="abbr-tbl">');
    for I := 0 to FAbbr.Count - 1 do
      FData.Add( TextToHtml.Tag(
        TextToHtml.Tag(FAbbr.Names[I], 'abbr-id', 'td', 'id="' +
          TextToHtml.asURL(lowercase(FAbbr.Names[I])) + '"') +
        TextToHtml.Tag('&nbsp;', 'abbr-sep', 'td') +
        TextToHtml.Tag(FAbbr.Values[I], 'abbr-desc', 'td')
        ,'abbr-row', 'tr'));
    FData.Add('</table>');
    TagAll;
    StandardDoc;
    SimpleHTML(@PDUnchanged);
  end;
  Outdent;
end;

procedure TRBILParser.GlossarySection;
var
  I, X : integer;
  S, N, V : String;
begin
  if FPreProcess then begin
    Indent;
    X := 3;
    I := 3;
    if (ExtractFileName(FFilename) = CONFIG_FILE) or
    (ExtractFileName(FFilename) = USER_FILE) then begin
      I := 1;
      X := 0;
    end;

    N := '';
    V := '';
    while I < FData.Count do begin
      S := Trim(FData[I]);
      FData.Delete(I);
      if S='' then begin
        if (N<>'') and (V <> '') then FDef.Add(N, V);
        X := 0;
        N := '';
        V := '';
      end else if X = 1 then
        N := S
      else
        V := WhenTrue(V, V + ' ') + S;
      Inc(X);
    end;
    if (N <> '') and (V <> '') then FDef.Add(N, V);

    for I := 0 to FDef.Count - 1 do begin
      X := Pos(')', FDef.Values[I]);
      if X > 40 then X := -1;
      if X > 0 then Inc(X, 8) else Inc(X, 20);
      V := '...';
      if Length(FDef.Values[I]) < X + 5 then begin
        X := Length(FDef.Values[I]);
        V := '';
      end;
      if FIndexLevel > 0 then begin
        FIndexer.Add(FilterExclude(FDef.Names[I], '+'), '99');
      end;
      Message(3, FDef.Names[I] + ' > ' + Copy(FDef.Values[I], 1, X) + V);
    end;
    outdent;
  end else if (ExtractFileName(FFilename) <> CONFIG_FILE) and
  (ExtractFileName(FFilename) <> USER_FILE) then begin
    I := 1;
    While I < FData.Count do FData.Delete(I);
    for I := 0 to FDef.Count - 1 do begin
      FData.Add(TextToHTML.Tag(FDef.Names[I], 'glossary-term', 'span',
        'id="' + TextToHTML.asURL(Lowercase(FDef.Names[I])) + '"'));
      FData.Add(TextToHTML.Tag(FDef.Values[I], 'glossary-definition'));
    end;
    TagAll;
    StandardDoc;
    SimpleHTML(@PDUnchanged);
  end;
end;

procedure TRBILParser.NoteSection;
begin
  if FPreProcess then exit;
  indent;
  FAltTitle := '';
  FTitle := 'Notes';
  Message(3, FGroup + ' / ' + FTitle);
  outdent;
  RawSection(WebSafeName(FGroup));
end;

procedure TRBILParser.WebPageSection;
var
  S, V : String;
  I : integer;
begin
  if FPreProcess then Exit;
  FData.Delete(0);
  while (FData.Count > 0) and (Trim(FData[0]) = '') do FData.Delete(0);
  while (FData.Count > 0) and (Trim(FData[FData.Count - 1]) = '') do
    FData.Delete(FData.Count - 1);
  if FData.Count < 1 then Exit;
  S := Uppercase(FClass) + Trim(FData[0]);
  FData.Delete(0);
  while (FData.Count > 0) and (Trim(FData[0]) = '') do FData.Delete(0);
  V := '';
  for I := 0 to FData.Count - 1 do
    V := V + FData[I] + CRLF;
  if (S <> '') and (V <> '') then
    FWebPages.Add(S, V);
end;

procedure TRBILParser.RawTextSection;
var
  HSec : integer;
  HID, HGroup, HClass, HTitle : String;
begin
  // Filter out commonfile headers
  if FComments.Count > 1 then begin
    if (
    (Pos('RELEASE', Uppercase(FComments[0])) > 1) or
    (Pos('EXPANSION', Uppercase(FComments[0])) > 1) or (
      (Pos('PART ', Uppercase(FComments[0])) > 1) and
      (Pos(' OF ', Uppercase(FComments[0])) > 1)
      )
    )  and (Pos('COPYRIGHT', Uppercase(FComments[1])) > 0) then begin
      FComments.Delete(0);
      FComments.Delete(0);
    end;
  end;
  While (FComments.Count > 0) and (Trim(FComments[0]) = '') do FComments.Delete(0);
  if FComments.Count = 0 then Exit;
  if FPreProcess then begin
    Inc(FSecCount);
    exit;
  end;
  HSec := FSecLine;
  HClass := FClass;
  HTitle := FTitle;
  HGroup := FGroup;
  HID := FSection;
  FTitle := 'Notes';
  if FGroup = 'OVERVIEW' then begin
    FTitle := 'Title Notes';
    FGroup := '';
  end
  else
    FGroup := WebSafeName(FGroup);
  FSecLine := 0;
  indent;
  FClass := 'Note';
  Message(1, 'RAW Text: ' + FGroup + ' / ' + FClass + ' / ' + FTitle);
  while (Length(FComments[0]) < 80) do FComments[0] := FComments[0] + ' ';
  FComments.Insert(0,'Raw Comments');
  TagAll(FComments);
  FComments.Insert(1, '<pre>');
  FComments.Add('</pre>');
  FComments.Insert(1, '<div class="rawdoc-outer"><div class="rawdoc-inner"><div class="rawdoc">');
  FComments.Add('</div></div></div>');
  SimpleHTML(FComments, FGroup, @PDUnchanged, false);
  outdent;
  FComments.Clear;
  FSecLine := HSec;
  FTitle := HTitle;
  FClass := HClass;
  FGroup := HGroup;
  FSection := HID;
  Inc(FSecCount);
end;

procedure TRBILParser.OpcodesSection;
begin
  FTitle := 'Opcodes List';
  TagOpcodes('opcode/other.html');
  if not FPreProcess then begin
    TagSeeAlso('opcode/other.html');
    RawSection('opcode');
  end;
end;

procedure TRBILParser.IndexIncludeSection;
var
  I : integer;
  S : String;
begin
  if (FIndexLevel = 0) or (not FPreProcess) then exit;
  for I := 1 to FData.Count - 1 do begin
    S := Trim(FData[I]);
    if S <> '' then
      FIndexer.Add(S, '99');
  end;
end;

procedure TRBILParser.IndexExcludeSection;
var
  I : integer;
  S : String;
begin
  if (FIndexLevel = 0) or (not FPreProcess) then exit;
  for I := 1 to FData.Count - 1 do begin
    S := Trim(FData[I]);
    if S <> '' then
      FIgnore.Add(Uppercase(S));
  end;
end;

procedure TRBILParser.CorrectionSection;

var
  I : integer;
  F : boolean;
  S, M, A, B, C, T, V: String;
begin
  if not FPreProcess then exit;
  F := False;
  for I := 1 to FData.Count - 1 do begin
    S := Trim(FData[I]);
    if S = '' then
      Continue;
    if HasLeading('rbil:', S, false) then begin
       if F then OutDent;
       F := Trim(ExcludeLeading('rbil:', S, false)) =
         Trim(Copy(FRelease_Version, Pos(' ',FRelease_Version)));
       if F then begin
         Message(2, 'Corrections for RBIL ' + FRelease_Version);
         Indent;
       end;
    end;
    if not F then Continue;
    M := PopCSV(S);
    case M of
      '#' : Message(3, S);
      '?' : begin
        indent;
        While Trim(S) <> '' do begin
          A := PopCSV(S);
          B := PopCSV(S);
          Message(4,'Global: ' + A + '-->' + B);
          Corrections.Add(A, B);
        end;
        outdent;
      end;
      '+', '-' : begin
        indent;
        T := WhenTrue(M = '+', '  Post:', '   Pre:') + ' ';
        T := T + LeftPad(GetCSV(S), 5, ' ') + ',';
        A := M + PopCSV(S) + ',';
        T := T + RightPad(GetCSV(S), 14);
        A := A + PopCSV(S);
        V := S;
        B := PopCSV(S);
        T := T + RightPad('(' + B + ')', 12);
        if Uppercase(B) <> 'DELETE' then begin
          C := PopCSV(S);
          T := T + 'Delim=' + C + ' "' + Copy(S, 1, Pos(C, S) -1) + '" --> ' +
            '"' + Copy(S, Pos(C, S) +1) + '"';
        end;
        Message(4, T);
        Corrections.Add(A,V);
        outdent;
      end;
    end;
  end;
  if F then OutDent;
end;

procedure TRBILParser.UnicodeTables;
const
  FileMax = $1000;

var
  Max : integer;
  FN, W : integer;

  function Skip(Current, Next, Steps : integer) : String;
  var
    T : String;
    I : integer;
  begin
    T := '<tr class="skip" id="cide-' + lowercase(HexStr(Current, W)) +'">' +
      '<th>' + HexStr(Current, W) + '</th>' +
      '<td></td>';
    for I := 1 to Steps - 2 do
      T := T + '<td></td>';
    T := T + '<td>' +  HexStr(Next - 1, W) + '</td>';
    Result := T + '<tr>';
  end;

  function SkipThis(var ANumber: integer; Steps : Integer): boolean;
  var
    I, E, N : integer;
  begin
    Result := False;
    for I := Low(UnicodeSkipRanges) to High(UnicodeSkipRanges) do begin
      // range check dev code remove when ranges are completed
      if (I > Low(UnicodeSkipRanges)) and
      (UnicodeSkipRanges[I -1][1] > UnicodeSkipRanges[I][0] ) then
        raise Exception.Create('range ' + HexStr(UnicodeSkipRanges[I][0], W) + ' out of order');
      if (UnicodeSkipRanges[I][1] <= UnicodeSkipRanges[I][0] ) then
        raise Exception.Create('invalid range ' + HexStr(UnicodeSkipRanges[I][0], W));

      // end dev code
      if InRange(ANumber, UnicodeSkipRanges[I][0], UnicodeSkipRanges[I][1] -1) and
      (UnicodeSkipRanges[I][1] - ANumber >= Steps) then begin
        N := I;
        repeat
          E:=UnicodeSkipRanges[N][1];
          Inc(N);
        until (N > High(UnicodeSkipRanges)) or (E <> UnicodeSkipRanges[N][0]);
        E := (E div Steps) * Steps;
        FData.Add( Skip(ANumber, E, Steps));
        ANumber := E;
        Result:=True;
        Break;
      end;
    end;
  end;

  function NullChar(ANumber: integer): boolean;
  var
    I : integer;
  begin
    Result := False;
    for I := Low(UnicodeSkipRanges) to High(UnicodeSkipRanges) do
      if InRange(ANumber, UnicodeSkipRanges[I][0], UnicodeSkipRanges[I][1] -1) then begin
        Result:=True;
        Break;
      end;
  end;

  function Table(AStart, AEnd, Steps : integer) : boolean;
  var
    S, N, J, Nav : String;
    X, Y : integer;
  begin
    Result := False;
    Nav := '<br><div class="table-nav">';
    if (FN > 0) then
      Nav := Nav + '<span class="table-nav-prev"><a href="unicode-' + lowercase(HexStr(FN - 1, 2)) +
      '.html">Previous</a></span>';
    if (AEnd < (Max + 1) * FileMax - 1) then
      Nav := Nav + '<span class="table-nav-next"><a href="unicode-' + lowercase(HexStr(FN + 1, 2)) +
        '.html">Next</a></span>';
    Nav := Nav + '</div><br>';
    FData.Add( '<div class="table-outer"><div class="table-inner">');
    FData.Add( Nav);
    FData.Add('<div class="tabledoc"><table class="huge-table">');
    Y := AStart - Steps;
    while Y <= AEnd do begin
      if (Y >=AStart) and FSkipMany and SkipThis(Y, Steps) then Continue;
      if Y = AStart-Steps then
        S := '<tr class="row-head">'
      else if ((Y -AStart) div Steps) mod 2 = 0 then
        S := '<tr class="row-even">'
      else
        S := '<tr class="row-odd">';
      for x := -1 to Steps -1 do begin
        if (X =-1) and (Y=AStart-Steps) then
          S := S + '<th class="table-null"></th>'
        else if X=-1 then
          S := S + '<th class="column-head">' + HexStr(Y, W) + '</th>'
        else if Y=AStart-Steps then
          S := S + '<th class="column">0x' + HexStr(X, 1) + '</th>'
        else begin
          J := '';
          if NullChar(X + Y) then N := 'no-' else begin
            N := '';
            if X >= Steps div 2 then
              J := 'left' else J := 'right';
            J := '<div class="char-' + J + '"><div class="jumbo">&#x' +
              hexStr(Y + X, W) +';</div><div class="code">x' +
              hexStr(Y + X, W) +'</div></div>';
            Result := True;
          end;
          S := S + '<td class="' + N + 'unicode" id="uid-x' + lowercase(HexStr(X + Y, W)) +'">' +
            '<div class="' + N + 'char">&#x' +
            hexStr(Y + X, W) + ';' + J +'</div><div class="' + N + 'code">x' +
            hexStr(Y + X, W) + '</div></td>' ;
        end;
      end;
      S := S + '</tr>';
      FData.Add(S);
      Inc(Y, Steps);
    end;
    FData.Add('</table></div>');
    FData.Add( Nav);
    FData.Add( '</div></div>');
  end;

 procedure MakeTable(AStart, AEnd : integer);
 var
   P, T, DT, SD : String;
 begin
   Inc(FSecCount);
   SD := IncludeTrailingPathDelimiter('chars');
   P := CheckDirectory(FOutDir,SD);
   if P = '' then Exit;
   SD := SD + IncludeTrailingPathDelimiter('unicode');
   P := CheckDirectory(FOutDir,SD);
   if P = '' then Exit;
   if AStart = 0 then
     SetDocTitle(SD, 'UTF-8 &amp; Unicode');
   W := 4;
   if AEnd > $FFFF then W := 5;
   if AEnd > $FFFFF then W := 6;
   if AEnd > $FFFFFF then W := 7;
   if AEnd > $FFFFFFF then W := 8;
   T := ' (' + HexStr(AStart, W) +
     '-' + HexStr((AEnd div $10) * $10 + $f, W) + ')';
   DT:='Characters' + T;
   FData.Clear;
   FClass := 'unicode-' + lowercase(HexStr(FN, 2));
   FData.Add(FClass);

   FTitle := 'UTF-8 &amp; Unicode ' + DT;
   FFileName := 'null';
   FSecLine := -1;
   FSection := '';
   if FSkipMany then
   FData.Add('<span class="heading-note">'+ UnicodeSkipInfo + '</span>');
   if Table(AStart, AEnd, $10) then begin
     // SetDocTitle(SD + FClass + '.html', DT);
     SimpleHTML(SD, @PDUnchanged, false);
     // TextToHTML.InsertBefore(P + FClass + HTMLExt,
     //  '<style>' + CRLF + CSSUnicode + CRLF + '</style>', '', '</head>', true);
     Inc(FN);
   end else FData.Clear;
 end;

 var
   K : integer;

begin
  Indent;
  Message(1, 'generating UTF8/Unicode tables');
  FN := 0;
  Max := (UnicodeSkipRanges[High(UnicodeSkipRanges)][1] div FileMax) - 1;
  if Max * FileMax >= UnicodeSkipRanges[High(UnicodeSkipRanges)][0] + FileMax - 1 then
    Max := (UnicodeSkipRanges[High(UnicodeSkipRanges)][0] - 1) div FileMax;
  for K := 0 to Max do
    MakeTable(K * FileMax, (K + 1) * FileMax - 1);
  OutDent;
end;

procedure TRBILParser.AsciiTable(ACodePage: integer);
var
   P, S, J, D, V, LR, C, SD: String;
   X, Y, N : integer;
   U, A : TUTF_Translator;
begin
  Inc(FSecCount);
  U := TUTF_Translator.Create(ACodePage);
  A := TUTF_Translator.Create(ACodePage);
  U.Flavor:='DISPLAY';
  A.Flavor:='ALTERNATE';
  Indent;
  Message(1, 'generating ASCII table');
  SD := IncludeTrailingPathDelimiter('chars');
  P := CheckDirectory(FOutDir, SD);
  if P = '' then raise Exception.Create('unable to open directory');
  SD := SD + IncludeTrailingPathDelimiter('codepage');
  P := CheckDirectory(P, 'codepage');
  if P = '' then raise Exception.Create('unable to open directory');
  SetDocTitle(SD, 'DOS ASCII');
  FData.Clear;
  FClass := 'cp-' + IntToStr(ACodePage);
  FTitle := 'ASCII and Control characters';
  FData.Add(FClass);

  FTitle := 'Codepage ' + IntToStr(ACodePage);
  FFileName := 'null';
  FSecLine := -1;
  FSection := '';
  FData.Add('<span class="heading-note">(Codepage ' + IntToStr(ACodePage) + ')<br></span>');
  FData.Add('<span class="heading-note"><br></span>');
  FData.Add('<div class="table-outer"><div class="table-inner">');
  FData.Add('<div class="tabledoc"><table class="huge-table">');
  for Y := -1 to 15 do begin
    if Y = -1 then
      S := '<tr class="row-head">'
    else if Y mod 2 = 0 then
      S := '<tr class="row-even">'
    else
      S := '<tr class="row-odd">';
    for x := -1 to 15 do begin
      if (X = -1) and (Y = -1) then
        S := S + '<th class="table-null"></th>'  + CRLF
      else if X=-1 then
        S := S + '<th class="column-head">0x' + HexStr(Y * $10, 2) + '</th>' + CRLF
      else if Y = -1 then
        S := S + '<th class="column">0x' + HexStr(X, 2) + '</th>' + CRLF
      else begin
        N := Y * 16 + X;
        V := '0x' + HexStr(N, 2);
        D := U.asHTML(Chr(N));
        C := '0x' + hexStr(N, 2);

        if (N <> $22) and (Chr(N) <> D) then
          C := C + '<br>&amp;' + Copy(D, 2, Length(D));

        J := '<table><tr><td><div class="jumbo">' +
           D +'</div><div class="code">' +
           C +'</div></td>';
        if D <>  A.asHTML(Chr(N)) then begin
          case N of
            0, $20, $ff : D := A.asHTML(Chr(N));
          else
            D := D + '<br>'+A.asHTML(Chr(N));
          end;
          C := '0x' + hexStr(N, 2);
          if Chr(N) <> A.asHTML(Chr(N)) then
            C := C + '<br>&amp;' + Copy(A.asHTML(Chr(N)), 2, Length(A.asHTML(Chr(N))));
            J := J + '<td><div class="jumbo">' +
             A.asHTML(Chr(N)) +'</div><div class="code">' +
             C +'</div></td>';
        end;
        J := J + '</tr></table>';
        if X >= 8 then
          LR := 'left' else LR := 'right';
         S := S + '<td class="ascii"><div class="char" id="aid-x' + copy(V, 3, 2) + '">' +
           D + '<div class="char-' + LR + '">' +
           J + '</div>' +'</div><div class="code">' +
           V + '</div></td>' + CRLF ;
      end;
    end;
    S := S + '</tr>';
    FData.Add(S);
  end;
  FData.Add('</table></div>');
  FData.Add('</div></div>');

  SimpleHTML(SD, @PDUnchanged, false);

  // TextToHTML.InsertBefore(P + FClass + HTMLExt,
  //  '<style>' + CRLF + CSSUnicode + CRLF + '</style>', '', '</head>', true);

  OutDent;
  FreeAndNil(A);
  FreeAndNil(U);
end;

procedure TRBILParser.AsciiTables;
begin
  AsciiTable(437);
end;

procedure TRBILParser.ProcessList(var ATable: TStringArray; AColon: integer);
// Basically to process Flags and Categories sections.
var
  I, P : integer;
  S, N, D : String;
  X : integer;
begin
  ClearArray(ATable, 256);
  X := 0;
  Indent;
  for I := 0 to FData.Count - 1 do begin
    S := FData.Strings[I];
    if X < AColon then begin
      P := Pos(':', S);
      if P > 0 then begin  // List follows first line after :
        Inc(X);
        Delete(S, 1, P);
        S := Trim(S);
      end
    end;
    S := Trim(S);
    while (X >= AColon) and (S <> '') do begin
      P := Pos(',', S) - 1;
      if P <= 0 then P := Length(S);
      N:=Copy(S, 1, P);
      Delete(S, 1, P);
      if (Pos('(', N) > 0) and (Pos(')', N) <= 0) then begin
         P := Pos(')', S);
         if P <= 0 then P := Length(S);
         N:=N+Copy(S, 1, P);
         Delete(S, 1, P);
      end;
      if (Length(S) > 0) and (S[1] = ',') then
         Delete(S, 1, 1);
      S := Trim(S);
      if N <> '' then begin
        D:=Trim(Copy(N, 1, Pos('-', N) - 1));
        N:=Trim(Copy(N, Pos('-', N) + 1, Length(N)));
        if (Length(D) <> 1) or (N = '') then
          raise Exception.Create(FClass + ' list processing error');
        ATable[Ord(D[1])]:=N;
        if FPreProcess then Message(3, D + ' > ' + N);
      end;
    end;
  end;
  Outdent;
end;

procedure TRBILParser.ItemBlock;
var
  Kind : String;
begin
  while (FData.Count > 1) and (Trim(replaceAll('-', '', FData[1])) = '') do FData.Delete(1);
  if FData.Count > 1 then begin
    if Uppercase(GetWord(FData[1], 3)) = 'THIS IS DOC' then
      Kind := 'OPCODE'
    else
      Kind := Uppercase(Trim(FData[1]));
    if Pos(' ', Kind) > 0 then
      Kind := Copy(Kind, 1, Pos(' ', Kind) - 1);
    case Kind of
      'INT',
      'I2C',
      'CMOS',
      'MSR',
      'MEM',
      'CALL',
      'PORT' : IntItem(Kind);
      'OPCODE' : OpcodesSection;
    else
      if not FPreprocess then
        RawSection(lowercase(Kind), True);
    end;
  end;

end;

procedure TRBILParser.IntItem(AKind : String);
{$IFOPT D+}
const
  MAX = 20;
  MCX : array of record
    Name : ShortString;
    Remain : integer;
  end = (
    (Name:'INT'; Remain:Max)
  );

var
  MCI : Integer;
{$ENDIF}
var
  SD, FN,
  TT,
  TK, TN, TF, TFL, TC, TS, TD : String;
  I : integer;
begin
  FN := WebSafeName(FTitle);
  if Pos(Copy(FN, 2, 1), '_-') > 0 then
    FN := Copy(FN, 3, Length(FN));
  if FN = '' then FN:='unknown';
  if FData.Count > 1 then
    TT := trim(replaceall(#9, ' ', FData[1]))
  else
    TT := '';
  TC := '';
  TS := '';
  TD := '';

  if Pos('-', TT) > 0 then begin
    TC := Trim(Copy(TT, Pos(' - ', TT) + 3, Length(TT)));
    SetLength(TT, Pos(' - ', TT) - 1);
    if LastPos(' - ', TC) > 0 then begin
      TD := Trim(Copy(TC, LastPos(' - ', TC) + 3, Length(TC)));
      SetLength(TC, LastPos(' - ', TC) - 1);
    end;
    if Pos(' - ', TC) > 0 then begin
      TS := Trim(Copy(TC, Pos(' - ', TC) + 3, Length(TC)));
      SetLength(TC, Pos(' - ', TC) - 1);
    end;
  end;

  TK := PopWord(TT);

  if AKind <> TK then
    Message(4, 'Possible Item Kind missmatch (probably fine)' );

  if TK = 'I2C' then
    TN := PopWord(TT, '-_:/')
  else
    TN := PopWord(TT, '-_:');

  if (Length(TN) = 9) and (lowercase(TN[9]) = 'h') then begin
    TN[9] := lowercase(TN[9]);
    Insert('h:', TN, 5);

  end;
  TF := PopWord(TT);

  TFL := '';
  for I := 1 to Length(TF) do
    TFL := TFL + WhenTrue(TFL, ', ') +
      WhenTrue(FFlagsShort[Ord(TF[I])], FFlagsShort[Ord(TF[I])], TF[I] + '?');

  TFL := WhenTrue(TFL, '{' + TFL + '}');

  if  Copy(FN, 2, 7) = 'xxxxxxx' then
    FN := WebSafeName(FN + '_' + {WhenTrue(TC <> '', TC , TD)}
      WhenTrue(TC, ' - ' + TC) +
      WhenTrue(TS, ' - ' + TS) +
      WhenTrue(TD, ' - ' + TD)
    );

  FTitle :=
    WhenTrue(TK, TK, 'ERROR') +
    WhenTrue(TN, ' ' + TN) +
    WhenTrue(TC, ' - ' + TC) +
    WhenTrue(TS, ' - ' + TS) +
    WhenTrue(TD, ' - ' + TD) +
    WhenTrue(TFL, ' ' + TFL);

  if TK = '' then begin
    indent;
    Message(2, 'Error (Skipped):');
    indent;
    Message(2, FData[1]);
    Message(2, TT);
    outdent;
    outdent;
    exit;
  end;

  SD := IncludeTrailingPathDelimiter(WebSafeName(FGroup)) +
    IncludeTrailingPathDelimiter(WebSafeName(FClass));
{
  if TC <> '' then begin
    SD := SD + IncludeTrailingPathDelimiter(WebSafeName(TC));
    SetDocTitle(SD, TC);
  end; }
{  if TS <> '' then begin
    SD := SD + IncludeTrailingPathDelimiter(WebSafeName(TS));
    SetDocTitle(SD, TS);
  end;    }
{  if TD <> '' then begin
    SD := SD + IncludeTrailingPathDelimiter(WebSafeName(TD));
    SetDocTitle(SD, TD);
  end;}
  if FPreProcess then begin
    FInts.Add(lowercase(FilterExclude(FN, '_')), SD + FN + HTMLExt +EOLn +FTitle + EOLn + IntToStr(FSecCount));
    ScanForTables(SD + FN + HTMLExt);
    // Find tables and such
  end else begin
    {$IFOPT D+}
    if FDebug then begin
      for MCI := Low(MCX) to High(MCX) do
        if MCX[MCI].Name = TK then
          Break;
      if MCX[MCI].Name <> TK then begin
        SetLength(MCX, Length(MCX) + 1);
        MCI := High(MCX);
        MCX[MCI].Name := TK;
        MCX[MCI].Remain := MAX;
      end;
      if (MCX[MCI].Remain < 1) then Exit;
      Dec(MCX[MCI].Remain);
    end;
    {$ENDIF}

    Indent;

    Message(4, IntToStr(FSecCount) + ',' + SD + ':' + FTitle);

    {$IFOPT D+}
    {
    FData.Add(
        '<hr>' +
        FGroup + '/' +
        FClass + '/' +
        FN
    );
    FData.Add(
      '(' + TK + ')' + CRLF + '(' + TN +')' + CRLF + '(' + TC + ')' + CRLF +
      '(' + TS + ')' + CRLF + '(' + TD + ')' );
    }
    {$ENDIF}

    SetIntFuncData;
    TagTables;
    TagSeeAlso(SD + FN + HTMLExt);
    TagAll;

    I := 1;
    while (I < FData.Count) and (Trim(FData[I]) = '') do Inc(I);

    if I < FData.Count then begin
      FData[I] := TextToHtml.Tag(RightPad(FData[I], 80), 'part-title');
    end;

    FData.Insert(1,
    '<div class="intdoc-outer"><div class="intdoc-inner"><div class="intdoc">' + CRLF +
    '<div class="group-' + lowercase(FGroup) +'">' +
    '<div class="kind-' + lowercase(FClass) +'">' +
    '<div class="int-' + FN +'">' +
     '<pre>');
    FData.Add('</pre></div></div></div>' + CRLF + '</div></div></div>');

    InterruptHTML(SD, FN);
    Outdent;
  end;
end;

function TRBILParser.PDDefault(Line:integer; S: String): String;
begin
  if Line = 0 then
    Result := S
  else
    Result := S + '<br>';
end;

function TRBILParser.PDUnchanged(Line: integer; S: String): String;
begin
  if Line = 0 then   // Stop telling me it is unused!
    Result := S      // It is required for the PDTextBlocks Filter
  else
    Result:=S;
end;

function TRBILParser.PDTextBlocks(Line: integer; S: String): String;
begin
  if Trim(S) = '' then
    Result := '</p>' + CRLF + '<p>'
  else if (Line < FData.Count - 1) and (Trim(FData.Strings[Line+1]) <> '') then
    Result := S + '<br>'
  else
    Result := S;
end;

procedure TRBILParser.SimpleHTML(var AData: TStringList; ASubDir: String;
  ATextProcess: TTextProcess; AParagraphs: boolean);
var
  P, N, H, XF, XSF : String;
  I : integer;
  E : boolean;
  S : TStringList;
begin
  if FPreProcess then Exit;
  if not Assigned(ATextProcess) then
    ATextProcess:=@PDDefault;
   P := CheckDirectory(FOutDir, ASubDir);
   if P = '' then raise Exception.Create('unable to open directory');
   N := P + lowercase(FClass) + HTMLExt;
   E := FileExists(N);
   H := '<hr>';
   if E then begin
     if (FClass = 'CONTACT_INFO') or (FClass = 'DISCLAIMER') then begin
        Message('ignore additional ' + FClass + ' section');
        exit;
     end;
   end else begin
     ASubDir := IncludeTrailingPathDelimiter(DirectorySeparator + ASubDir);
     H :=
       TextToHTML.Tag('&nbsp;', 'page-headspace', 'div') + CRLF +
       TextToHTML.Tag(WhenTrue(FAltTitle, FAltTItle, TitleStr), 'page-heading', 'div');
     TextToHtml.CreateHTML(N, TitleStr, '', False);
     Inc(FPages);
     TextToHtml.AddHeader(N, Header);
     SetDocTitle(ASubDir + lowercase(FClass) + HTMLext, FTitle);
     TextToHtml.AddFooter(N, Footer);
     TextToHtml.AppendHTML(N, H, '');
     H := '';
   end;

   if ExtractFileName(FFilename) = CONFIG_FILE then
     XF := CONFIG_FILE
   else if ExtractFileName(FFilename) = USER_FILE then
     XF := CONFIG_FILE
   else
     XF := Copy(FFilename, Length(FRBILDir) + 1);

   if Not E then
     TextToHTML.AddHead(N,
       TextToHTML.Comment(DOCID_TITLE, FTitle) + CRLF +
       TextToHTML.Comment(DOCID_OUTFILE, ExcludeTrailingPathDelimiter(ASubDir) +
         WhenTrue(ASubDir, DirectorySeparator) + lowercase(FClass) + HTMLExt)
     );
   XSF:='';
   if FSubFunc <> '' then XSF:=TextToHTML.Comment(DOCID_SUBFUNC, FSubFunc) + CRLF;
   if FClass <> 'index' then
     TextToHTML.AddHead(N,
       XSF +
       TextToHTML.Comment(DOCID_SECTION, IntToStr(FSecCount) + WhenTrue(FSection, ', ' + FSection)) + CRLF +
       TextToHTML.Comment(DOCID_SRCFILE, XF) + CRLF +
       TextToHTML.Comment(DOCID_SRCLINE, IntToStr(FSecLine)) // + CRLF +
       // TextToHTML.Comment(DOCID_TITLE, FTitle)
     );

   S := TStringList.Create;
   S.Add(TextToHtml.BeginTag('', 'section', 'div',
     WhenTrue(FClass <> 'index', 'id="sect-' + IntToStr(FSecCount) + '"')));
   if AParagraphs then S.Add(TextToHtml.BeginTag('', '', 'p'));
   for I := 1 to AData.Count - 1 do begin
     S.Add(ATextProcess(I, AData.Strings[I]));
   end;
   if AParagraphs then S.Add(TextToHtml.EndTag('', 'p'));
   S.Add(TextToHtml.EndTag('', 'div'));
   IndexTag(N, S);
   TagRefItems(N, S);
   TextToHtml.AppendHTML(N,S.Text,H);
   FreeAndNil(S);
end;

procedure TRBILParser.SimpleHTML(ASubDir: String; ATextProcess: TTextProcess;
  AParagraphs: boolean);
begin
  SimpleHTML(FData, ASubDir, ATextProcess, AParagraphs);
end;

procedure TRBILParser.SimpleHTML(ATextProcess: TTextProcess;
  AParagraphs: boolean);
begin
  SimpleHTML('', ATextProcess, AParagraphs);
end;

procedure TRBILParser.SimpleHTML(ATextProcess: TTextProcess);
begin
  SimpleHTML(ATextProcess, true);
end;

procedure TRBILParser.InterruptHTML(ASubDir, AFileName : String);
var
  P, N, H, XF, XSF : String;
  I : integer;
  E : boolean;
  S : TStringList;
begin
   P := CheckDirectory(FOutDir, ASubDir);
   if P = '' then raise Exception.Create('unable to open directory');
   if AFileName = '' then
     raise Exception.Create('cannot create null file name interrupt item html file.');
   N := P + AFileName + HTMLExt;
   E := FileExists(N);
   H := '<hr>';
   if not E then begin
     TextToHtml.CreateHTML(N, TitleStr, 'alt', False);
     Inc(FPages);
     TextToHtml.AddHeader(N, Header);
     SetDocTitle(ASubDir + AFilename + HTMLExt, FTitle);
     TextToHtml.AddFooter(N, Footer);
     H := '';
   end;

   if ExtractFileName(FFilename) = CONFIG_FILE then
     XF := CONFIG_FILE
   else if ExtractFileName(FFilename) = USER_FILE then
     XF := CONFIG_FILE
   else
     XF := Copy(FFilename, Length(FRBILDir) + 1);

   if Not E then
     TextToHTML.AddHead(N,
       TextToHTML.Comment(DOCID_TITLE, FTitle) + CRLF +
       TextToHTML.Comment(DOCID_OUTFILE, ExcludeTrailingPathDelimiter(ASubDir) +
         WhenTrue(ASubDir, DirectorySeparator) + lowercase(FClass) + HTMLExt)
     );

   XSF:='';
   if FSubFunc <> '' then XSF:=TextToHTML.Comment(DOCID_SUBFUNC, FSubFunc) + CRLF;

   TextToHTML.AddHead(N,
     XSF +
     TextToHTML.Comment(DOCID_SECTION, IntToStr(FSecCount) + WhenTrue(FSection, ', ' + FSection)) + CRLF +
     TextToHTML.Comment(DOCID_SRCFILE, XF) + CRLF +
     TextToHTML.Comment(DOCID_SRCLINE, IntToStr(FSecLine))
   );

   S := TStringList.Create;
   S.Add(TextToHtml.BeginTag('', 'section', 'div', 'id="sect-' + IntToStr(FSecCount) + '"'));

   for I := 1 to FData.Count - 1 do
     S.Add(FData.Strings[I]);

   S.Add(TextToHtml.EndTag('', 'div'));
   IndexTag(N, S);
   TagRefItems(N, S);
   TextToHtml.AppendHTML(N,S.Text,H);
   FreeAndNil(S);
end;

procedure TRBILParser.MakeCSS;
begin
  if FCSSDATA = '' then
    raise Exception.create('No CSS DATA found. Cannot export to ' +
    ExtractFIleName(TextToHTML.CSS))
  else
    SaveAsFile(TextToHTML.CSS, FCSSDATA);
end;

procedure TRBILParser.SetDocTitle(AFileName, ATitle: String);
begin
  AFileName := ExcludeLeading(BaseURL, AFileName);
  AFileName:=IncludeLeading(DirectorySeparator, AFileName);
  Message(4, 'Set ' + AFileName + ' title as "' + ATitle + '"');
  FDocTitles.Add(AFileName, ATitle);
end;

function TRBILParser.GetDocTitle(AFileName: String): string;
var
  X : integer;
  S : String;
begin
  AFileName := ExcludeLeading(BaseURL, AFileName);
  AFileName:=IncludeLeading(DirectorySeparator, AFileName);
  X := FDocTitles.Find(AFileName);
  if X < 0 then begin
    message(3, 'Lazy programmer, missing title for ' + AFileName);
    S := '';
    if not FileExists(FOutDir + AFilename) then begin
       S := ExtractFileName(ExcludeTrailingPathDelimiter(AFileName));
       if S = '' then S := 'home';
       S := TitleCase(replaceAll('_', ' ', S));
    end else
      S := TextToHTML.FetchComment(FOutDir + AFilename,DOCID_TITLE,false);
    if S = '' then
      S := TitleCase(Copy(ExtractFileName(AFileName), 1,
      Length(ExtractFileName(AFileName)) - Length(ExtractFileExt(AFileName))));
    Result := S;
  end else
    Result := FDocTitles.Values[X];
end;

function TRBILParser.GetDocFunc(AFileName: String): string;
begin
  AFileName := ExcludeLeading(BaseURL, AFileName);
  AFileName:= IncludeLeading(DirectorySeparator, AFileName);
  if FileExists(FOutDir + AFilename) then
    Result := TextToHTML.FetchComment(FOutDir + AFilename,DOCID_SUBFUNC,false)
  else
    Result := '';
end;

procedure TRBILParser.ForFiles(AFileProcess: TFileProcess; ARecurse: boolean;
  ASubDir: string);
var
  R : integer;
  Search : TSearchRec;
  Files : TStringList;
begin
  if ASubDir = '' then begin
    indent;
    AFileProcess(nil,'');
  end else
    ASubDir := IncludeTrailingPathDelimiter(ASubDir);

  if ARecurse then begin
    R := FindFirst(FOutDir + ASubDir + '*', faAnyFile, Search);
    while R = 0 do begin
      if (Search.Name <> '.') and (Search.Name <> '..') and
      (Search.Attr and faDirectory = faDirectory) then
        ForFiles(AFileProcess, ARecurse, ASubDir + Search.Name);
      R := FindNext(Search);
    end;
    FindClose(Search);
  end;

  Files := TStringList.Create;
  R := FindFirst(FOutDir + ASubDir + '*' + HTMLExt, faAnyFile, Search);
  while R = 0 do begin
    if (Search.Attr and faDirectory <> faDirectory) and (Search.Name <> 'index' + HTMLExt) and
    (ExtractFileExt(Search.Name) = HTMLExt) then
      Files.Add(Search.Name);
    R := FindNext(Search);
  end;
  FindClose(Search);
  Files.Sort;

  AFileProcess(Files, ASubDir);
  FreeAndNil(Files);

  if ASubDir = '' then outdent;
end;

procedure TRBILParser.ForFiles(AFileProcess: TFileProcess; ASubDir: string);
begin
  ForFiles(AFileProcess, true, ASubDir);
end;

procedure TRBILParser.FPIndex(Files: TStringList; ASubDir: String);
var
  I : integer;
  R : integer;
  Search : TSearchRec;
  S, DT, DSF : String;
begin
  if not Assigned(Files) then begin
    Message(1, 'Adding HTML index files');
    Exit;
  end;
  if FileExists(FOutDir + ASubDir + 'index' + HTMLExt) then
    Exit;
  indent;
  Message(2, 'index for ' + IncludeTrailingPathDelimiter(ASubDir));
  indent;
  FData.Clear;
  R := FindFirst(FOutDir + ASubDir + '*', faAnyFile, Search);
  while R = 0 do begin
    if (Search.Name <> '.') and (Search.Name <> '..') and
    (Search.Attr and faDirectory = faDirectory) then
      FData.Add(Search.Name);
    R := FindNext(Search);
  end;
  FindClose(Search);
  FData.Sort;
  FClass := 'index';
  FTitle := GetDocTitle(ASubDir);
  I := 0;
  while I < FData.Count do begin
    S := IncludeTrailingPathDelimiter(FData[I]);
    S := '<a href="' + S + 'index' + HTMLExt + '">' + GetDocTitle(ASubDir + S) + '</a>';
    Inc(FNavLinks);
    FData[I] := S;
    Inc(I);
  end;
  FData.Insert(0,FClass);
  if FData.Count > 1 then begin
    FData.Insert(1, '<div class="index-dirs">');
    FData.Add('</div>');
   end;
  if Files.Count > 0 then begin
    FData.Add('<div class="index-files">');
    for I := 0 to Files.Count - 1 do begin
      S := Files[I];
      DT := GetDocTitle(ASubDir + S);
      DSF := GetDocFunc (ASubDir + S);
      if DSF <> '' then DT:=DT + ' <span class="doc-index-function">(' + DSF + ')</span>';
      S := '<a href="' + S + '">' + DT + '</a>';
      // WriteLn (S, ' -> ', DT);
      FData.Add(S);
    end;
    FData.Add('</div>');
  end;
  TagAll;
  SimpleHTML(ASubDir, @PDTextBlocks, false);
  outdent;
  outdent;
end;

procedure TRBILParser.FPNavigation(Files: TStringList; ASubDir: String);

var
  I : integer;
  NavUp, NavHome, NavNext, NavPrev, NavTree : String;
  NMC, NMM : integer;

  function NavLink(AURL, AText : String; AActive : boolean) : String;
  begin
   if AActive and (AURL <> '' ) then begin
     Result := '<span class="nav-active"><a href="' + AURL + '">' + AText + '</a></span>';
     Inc(NMC);
   end
   else
     Result := '<span class="nav-inactive">' + AText + '</span>';
  end;

  function NavBar(FileName : String) : String;
  var
    FF, LF, CL : String;
  begin
    NMC := 0;
    if Files.Count > 0 then begin
      FF := Files[0];
      LF := Files[Files.Count - 1];
    end else begin
      FF := '';
      LF := '';
    end;
    CL := Navigation_Seperator + '<wbr> ' +
          NavLink(FileName, GetDocTitle(ASubDir + FileName), False);
    if FileName = 'index' + HTMLext then CL := '';
    Result :=
      '<div class="navigation"><div class="nav-bar">'  +
        '<div class="nav-left">' +
          NavLink(FF, Navigate_First, NavPrev <> '') +
          NavLink(NavPrev, Navigate_Previous, NavPrev <> '') +
        '</div>' +
        '<div class="nav-middle">' +
          NavLink(NavUp, Navigate_Up, NavUp <> '') +
        '</div>'  +
        '<div class="nav-middle">' +
          NavLink(NavHome, Navigate_Home, True) +
        '</div>'  +
        '<div class="nav-text">' +
          NavTree + CL +
        '</div>'  +
        '<div class="nav-right">' +
          NavLink(NavNext, Navigate_Next, NavNext <> '') +
          NavLink(LF, Navigate_Last, NavNext <> '') +
        '</div>' +
      '</div></div>';
    Inc(FNavLinks, NMC + NMM);
  end;

  function NavDirs(AEnabled : boolean = true) : String;
  var
    S, L, R, IFN : String;
    P : integer;
  begin
    NMM := 0;
    S := BaseURL + ASubDir;
//    WriteLn(S);
    IFN := 'index' + HTMLExt;
    L := IFN;
    R := '';
    repeat
      if AEnabled then Inc(NMM);
      // WriteLn('+', ':', S, ' -> ', L);
      R := Navigation_Seperator + '<wbr> ' +
        NavLink(L, GetDocTitle(IncludeTrailingPathDelimiter(S)), AEnabled) + '<wbr> ' + R;
      AEnabled := True;
      L := '..' + DirectorySeparator + L;
      P := LastPos(DirectorySeparator, S, Length(S) - 1);
      if P > 1 then
        Delete(S, P, Length(S))
      else
        Break;
      if Length(IncludeTrailingPathDelimiter(S)) < Length(BaseURL) then
        begin
          L := ExcludeTrailing(DirectorySeparator + IFN, L);
          // WriteLn(L);
        end;
    until (S = '');
    Result := R;
  end;

// var
//   BaseTree : string;

begin
  if not Assigned(Files) then begin
    Message(1, 'Adding HTML navigation');
    Exit;
  end;
  indent;
  Message(2, 'navigation for ' + IncludeTrailingPathDelimiter(ASubDir));
  indent;

  if BaseURL + ASubDir = '' then
    NavHome := ''
  else
    NavHome := ReplaceAll(DirectorySeparator, '..' + DirectorySeparator,
      FilterInclude(BaseURL + ASubDir, DirectorySeparator));
  NavUp := 'index' + HTMLExt;
  if BaseURL='' then
    NavHome := NavHome + NavUp;
  NavTree := NavDirs(True);
  for I := 0 to Files.Count - 1 do begin
    message(4, Files[I]);
    if I > 0 then  NavPrev := Files[I -1] else NavPrev := '';
    if I < Files.Count - 1 then  NavNext := Files[I+1] else NavNext := '';
      TextToHTML.PrependHTML(FOutDir + ASubDir + Files[I],
        '<div id="' + cidStartOfDocument + '"></div>' +  NavBar(Files[I]), '',True);
      TextToHTML.AppendHTML(FOutDir + ASubDir + Files[I],
        '<div id="' + cidEndOfDocument + '"></div>','',True);
  end;

  if FileExists(FOutDir + ASubDir + 'index' + HTMLExt) then begin
    message(4, 'index' + HTMLExt);
    NavPrev := '';
    NavNext := '';
    if ASubDir <> '' then
      NavUp := '..' + DirectorySeparator + 'index' + HTMLext
    else if BaseURL <> '' then
      NavUp := '..'
    else
      NavUp := '';
    NavTree := NavDirs(False);
    TextToHTML.PrependHTML(FOutDir + ASubDir + 'index' + HTMLExt,
      '<div id="' + cidStartOfDocument + '"></div>' +  NavBar('index' + HTMLExt), '',False);
    TextToHTML.AppendHTML(FOutDir + ASubDir + 'index' + HTMLExt,
      '<div id="' + cidEndOfDocument + '"></div>','',True);
  end;

  outdent;
  outdent;
end;

procedure TRBILParser.ProcessWebPages(APreIndex : boolean);
var
  I : Integer;
  FC, FN, FD, FW, FT : String;

  function TextBlock(AKind : String) : String;
  var
    P : boolean;
    J : integer;
    R : String;
  begin
    R := FD;
    if FC <> 'ASISTEXT' then
      for J := 0 to FWebVars.Count - 1 do
        R := replaceAll(FWebVars.Names[J],FWebVars.Values[J], R);
    FData.Clear;
    FData.Add(FN);
    FData.AddText(
    '<div class="' + AKind + '-outer">' + CRLF +
    '<div class="' + AKind + '-inner">' + CRLF +
    '<div class="' + AKind + '">' + CRLF +
     R + CRLF +
     '</div>' + CRLF +
    '</div>' + CRLF +
    '</div>');
    if FC <> 'ASISTEXT' then begin
      if FC = 'WEBTEXT' then
        TagAll
      else begin
        TextToHtml.TagHighlight(FData, 'highlight', 'span', 1);
        TextToHtml.TagGroup(FData, '(', ')', 'parenthesis', 'span', true, 1);
        TextToHtml.TagGroup(FData, '[', ']', 'bracket', 'span', true, 1);
        TextToHtml.TagGroup(FData, '{', '}', 'brace', 'span', true, 1);
        TextToHtml.TagGroup(FData, TextToHtml.UTF.DoubleQuote, TextToHtml.UTF.DoubleQuote, 'quote', 'span', 1);
        TextToHtml.TagDictionary(FData, FDef, 'definition', 'span', 1);
        TextToHtml.TagDictionary(FData, FAbbr, 'abbrev', 'span', 1);
      end;

      R:='';
      P := true;
      J := FData.Count - 1;
      While J > 0 do begin
        if Trim(FData[J]) = '' then begin
          if P = false then begin
            P := True;
            FData[J] := '</p>';
            FData.Insert(J, '<p>');
          end;
        end else P := False;
        Dec(J);
      end;
      if FData.Count > 1 then begin
        FData.Add('</p>');
        FData.Insert(1, '<p>');
      end;
    end;
    for J := 1 to FData.Count - 1 do
      R := R + FData[J] + CRLF;
    Result:=R;
  end;

begin
  indent;
  I := 0;
  while (FWebPages.Count > I) do begin
    FN := FWebPages.Names[I];
    FD := FWebPages.Values[I];
    Inc(I);
    FC := PopWord(FN);
    if Pos(' ', FN) > 0 then begin
       FW := Trim(Copy(FN, Pos(' ', FN)));
       SetLength(FN, Pos(' ', FN) - 1);
    end else
      FW := 'append';
    if Pos(' ', FW) > 0 then begin
       FT := Trim(Copy(FW, Pos(' ', FW)));
       SetLength(FW, Pos(' ', FW) - 1);
    end else
      FT := '';
    if ExcludeTrailingPathDelimiter(FN) <> FN then
      FN := FN + 'index' + HTMLExt;
    if Copy(FN, 1, 1) = DirectorySeparator then Delete(FN, 1, 1);
    if FN = '' then Continue;
    FN := lowercase(FN);
    FW := lowercase(FW);

    case FW of
      'index-files', 'index-dirs' : begin
          if APreIndex then Continue;
          message(3, FN + ' > ' + FW + WhenTrue(FT, ' - ' + FT));
          TextToHTML.InsertBefore(FOutDir + FN, TextBlock('textblock'), '', '<div class="' + FW + '">', true);
      end;
      'create', 'append' : begin
         if APreIndex and (FW = 'append') then Continue;
         if (not APreIndex) and (FW = 'create') then Continue;
         message(3, FN + ' > ' + FW + WhenTrue(FT, ' - ' + FT));
         TextBlock('textblock');
         FClass := ExtractFileName(FN);
         SetLength(FClass, Length(FClass) - Length(ExtractFileExt(FClass)));
         FN := ExtractFilePath(FN);
         FTitle := WhenTrue(FT, FT, FClass);
         SimpleHTML(FN, @PDUnchanged, FW = 'append');
      end;
    end;
  end;
  outdent;
end;

procedure TRBILParser.PrepIndexScan;
var
  I : Integer;
begin
  if FIndexLevel = 0 then Exit;
  for I := Low(IndexIgnore) to High(IndexIgnore) do
    FIgnore.Add(Uppercase(IndexIgnore[I]));
  for I := Low(IndexInclude) to High(IndexInclude) do
    FIndexer.Add(IndexInclude[I], '99');
end;

procedure TRBILParser.IndexScan;

{$IFOPT D+}
  const
    Max : integer = 200;
{$ENDIF}

  procedure AddItem(AStr : String; Weight : integer = 1);
  var
    I, V, E, J : integer;
  begin
    AStr := ReplaceAll(SPACE + SPACE, SPACE, AStr);
    AStr := FilterExclude(AStr, '?{}[]()');
    AStr := LTrim(RTrim(AStr, SPACE + TAB + '-_'),SPACE + TAB + '-_');
    if Length(AStr) < LenForIndex then Exit;
    if (Not FIndexLower) and IsAlpha(FilterExclude(AStr, '_-')) and
    (AStr = Lowercase(AStr)) then Exit;

    I := 0;
    for J := Low(IndexSimilar) to High(IndexSimilar) do
      if (I < 1) and HasTrailing(IndexSimilar[J], AStr, false) then
        I := FIndexer.Find(ExcludeTrailing(IndexSimilar[J], AStr, false));

    if I < 1 then
      I := FIndexer.Find(AStr);

    if I >= 0 then begin
      Val(GetLine(FIndexer.Values[I]), V, E);
      if E <> 0 then raise
        Exception.Create('Internal error: invalid numeric value ' +
          FIndexer.Values[I] + ' found in Index creation list.');
      FIndexer.Delete(I);
      Inc(V, Weight);
    end else
      V := Weight;
    FIndexer.Add(AStr, IntToStr(V));
  end;

var
  I : integer;
  W, S, T, F, L : String;

 procedure Slipper(AFrom : String; ATo : String = '');
 begin
   repeat
     W := Trim(Excise(S, AFrom, ATo));
     if W <> '' then begin
       S := S + ' - ' + W;
     end;
   until W = '';
 end;

 procedure Tripper(AFrom : String; ATo : String = '');
 begin
   repeat
     W := Trim(Excise(S, AFrom, ATo));
     if W <> '' then begin
       S := S + ' ' + ReplaceAll(' ', '_', W);
     end;
   until W = '';
 end;

begin
  if FIndexLevel < 2 then Exit;
  {$IFOPT D+}
    if FDebug then begin
      if Max <= 0 then Exit;
      Dec(Max);
    end;
  {$ENDIF}

  // Title gets special handling
  I := 1;
  if FCategory <> '!' then begin
     While (I < FData.Count - 1) and (Trim(FData[I]) = '') do Inc(I);
     if I < FData.Count - 1 then begin
       S := ReplaceAll(TextToHTML.UTF.DoubleQuote, '"', FData[I]);
       S := ReplaceAll('+', SPACE, S);

       Slipper('"', '"');
       Slipper('(', ')');
       Slipper(',');

       While S <> '' do begin
         W := Trim(PopDelim(S, ' - '));
         S := Trim(S);
         AddItem(W, 2);
       end;
     end;
  end else
    case Uppercase(FClass) of
         'ADMIN', 'SECTION' : exit;
    end;

   if FIndexLevel < 3 then Exit;

  S := '';
  for I := 1 to FData.Count - 1 do begin
    W := Trim(ReplaceAll('+', SPACE, FData[I]));
    if (Uppercase(GetWord(W, 1, ':')) = 'SEEALSO:') then Continue;
    S := WhenTrue(Length(S) > 0, S + ' ') + W;
  end;

  Tripper('"', '"');
  Tripper('(', ')');
  Tripper('[', ']');
  Tripper('{', '}');

  S := FilterWords(S);
  While S <> '' do begin
    W := Trim(ReplaceAll('_', SPACE, PopWord(S)));
    T := GetWord(W);
    if IsHex(T) or IsNumber(T) then
      PopWord(W)
    else if Pos('-', T) > 0 then begin
      F := PopWord(T, '');
      L := ReplaceAll('-', '', PopWord(T, ''));
      if T = '' then begin
        if (IsHex(F) and IsHex(L)) or (IsNumber(F) and IsNumber(L)) then
          PopWord(W);
      end;
    end;
    if (Length(W) >= LenForIndex) and (FIgnore.IndexOf(Uppercase(W)) < 0) then
      AddItem(W);
  end;
end;

procedure TRBILParser.SimplifyIndex;
var
  TD : TDictionary;
  I, J, X : integer;
  VI, VT : String;
  CI, CT : integer;

begin
  if FIndexLevel = 0 then Exit;
  TD := TDictionary.Create;
  TD.Duplicates:=dupAccept;
  for J := Low(IndexSimilar) to High(IndexSimilar) do begin
    I := 1;
    While I < FIndexer.Count - 1 do begin
      if (Uppercase(FIndexer.Names[I -1] + IndexSimilar[J]) = Uppercase(FIndexer.Names[I])) or
      (Uppercase(FIndexer.Names[I -1]) = Uppercase(FIndexer.Names[I])) then begin
         Message(4, 'Index Combine: ' + FIndexer.Names[I -1] + ' (' + GetLine(FIndexer.Values[I-1]) + ')' +
         ' & ' + FIndexer.Names[I] + ' (' + GetLine(FIndexer.Values[I]) + ')');
         TD.Add(FIndexer.Names[I -1], FIndexer.Values[I]);
         FIndexer.Delete(I);
      end else
        Inc(I);
    end;
    for I := 0 to TD.Count - 1 do begin
      X := FIndexer.Find(TD.Names[I]);
      if X < 0 then Continue; // Can't happen, we only have this cause it exists
      VI := FIndexer.Values[X];
      FIndexer.Delete(X);
      CI := StrToInt(PopLine(VI));
      VT := TD.Values[I];
      CT := StrToInt(PopLine(VT));
      // WriteLn('Replace: ', X, ', ', TD.Names[I], '(', CI + CT, ')');
      FIndexer.Add(TD.Names[I], IntToStr(CI + CT) + CRLF + VI + VT);
    end;
    TD.Clear;
  end;

  FreeAndNil(TD);
end;

procedure TRBILParser.IndexPrune(ASecondPass: boolean);
var
  N :String;
  I, V, E, M, L : integer;
  K : boolean;
begin
  if FIndexLevel = 0 then Exit;
  SimplifyIndex;
  case ASecondPass of
    False: begin
      if FIndexLevel >= 4 then Exit;
      I := 0;
      if FIndexer.Count > 0 then
        While I < FIndexer.Count - 1 do begin
          N := Uppercase(FIndexer.Names[I]);
          K := FIgnore.Find(N,E);
          Val(GetLine(FIndexer.Values[I]), V, E);
          if (V < MinForIndex) or (E <> 0) or K then
             FIndexer.Delete(I)
          else
            Inc(I);
        end;
    end;
    True: begin
      if FIndexLevel >= 5 then
        M := 1
      else
        M := MinForIndex;
      I := 0;
      if FIndexer.Count > 0 then
        While I < FIndexer.Count - 1 do begin
          K := FIgnore.Find(Uppercase(FIndexer.Names[I]),V);
          Val(GetLine(FIndexer.Values[I]), V, E);
          L := LineCount(FIndexer.Values[I]) - 1;
          if (E <> 0) or (V < 100) then
            V := L;
          if (V < M) or (L = 0) or K then
             FIndexer.Delete(I)
          else
            Inc(I);
        end;
    end;
  end;
end;

procedure TRBILParser.IndexTag(AFileName : String; FD : TStringList);
var
  D : TDictionary;
  S, W, O :String;
  I, P : integer;
  IIT : integer;

  procedure TagText;
  var
    U, N, V : String;
    L, X, C, M : integer;
  begin
    D.Clear;
    U := Uppercase(W);
    L := Length(O);
    M := 2;
    while (L > 0) and (M > 0) do begin
      Dec(L);
      if (Copy(O, L, Length(CRLF)) = CRLF) then
        Dec(M);
    end;
    X := 0;
    while X < FIndexer.Count - 1 do begin
      N := FIndexer.Names[X];
      C := Pos(Uppercase(N), U);
      if C > 0 then begin
        if (C > 1) and (IsAlphaNum(Copy(U, C-1,1))) then begin
          inc(X);
          Continue;
        end;
        V := FIndexer.Values[X];
        FIndexer.Delete(X);
        Inc(FIdxId);
        D.Add(N, V + CRLF + AFileName + '#idx'+IntToStr(FIdxId));
        V := '<span class="tag" id="idx' +IntToStr(FIdxId) +'"></span>';
        if L < 1 then begin
           O := V + O;
           L := 1;
        end else if L > Length(O) then
          O := O + V
        else
          Insert(V, O, L);
        Inc(L, Length(V));
      end else
        Inc(X);
    end;
    for X := 0 to D.Count - 1 do
      FIndexer.Add(D.Names[X], D.Values[X]);
  end;

begin
  if FIndexLevel = 0 then Exit;
  if FPostProcess then Exit;
  case FClass of
    'GLOSSARY', 'ABBREVIATIONS', 'CREDITS', 'ADDRESSES', 'TRADEMARKS', 'QUOTES',
    'DISCLAIMER', 'FLAGS', 'CATEGORIES', 'TABLES', 'AVAILABILITY' : exit;
  end;

  AFileName := MakeRelative(FOutDir, AFileName);
  if AFileName = 'note' + htmlext then exit;
  // WriteLn(FClass);
//  AFileName := MakeRelative(DirectorySeparator + 'zindex' + DirectorySeparator +
//    'index' + HTMLExt, DirectorySeparator + AFileName);
  S := '';
  O := '';
  Message(3, 'Indexing ' + AFilename);
  D := TDictionary.Create;
  For I := 0 to FD.Count - 1 do
    S := S + ReplaceAll(CRLF, '', FD[I]) + CRLF;
  FD.Clear;
  IIT := 0;
  while S > '' do begin
    P := Pos('<', S);
    if P < 1 then P := Length(S) + 1;
    W := Copy(S, 1, P -1);
    if IIT = 0 then TagText;
    O := O + W;
    Delete(S, 1, P - 1);
    P := Pos('>', S);
    if P < 1 then P := Length(S) + 1;
    W := Copy(S, 1, P);
    O := O + W;
    if IIT = 0 then begin
       if Pos('class="definition', W) > 0 then
         IIT := 1;
    end else begin
      if copy(W, 1, 2) = '</' then
        Dec(IIT)
      else
        Inc(IIT);
    end;
    Delete(S, 1, P);
  end;
  FD.AddText(O);
  FreeAndNil(D);
end;

procedure TRBILParser.MakeExtraViews;
begin
  CheckDirectory(FOutDir, 'zaspacer');
  MakeByIndex;
  MakeByTitle;
  MakeByInterrupt;
end;

procedure TRBILParser.MakeByIndex;
var
  I, X : integer;
  TD : TDictionary;
  S, L, N, R, H : String;
begin
  if FIndexLevel = 0 then Exit;
  IndexPrune(True);
  FFileName := 'null';
  FSecLine := -1;
  FSection := '';
  FAltTitle := '';

  TD := TDictionary.Create;

  for I := 0 to FIndexer.Count - 1 do begin
      FData.Clear;
      FData.Add(FIndexer.Values[I]);
      FData.Add('<div class="index-titles">');
      S := FIndexer.Values[I];
      PopLine(S);
      while S <> '' do begin
        L := PopLine(S);
        N := Copy(L, 1, Pos('#', L) - 1);
        R := Copy(L, Pos('#', L) );
        N := GetDocTitle(N);
        L := MakeRelative(DirectorySeparator + 'zindex' + DirectorySeparator +
            'index' + HTMLExt, DirectorySeparator + L);
        X := TD.Find(N);
        if X < 0 then
          TD.Add(N, L)
        else begin
          H := TD.Values[X];
          TD.Delete(X);
          TD.Add(N, H + CRLF + L);
        end;

        Inc(FIdxCnt);
      end;
      for X := 0 to TD.Count - 1 do begin
        N := TD.Names[X];
        S := TD.Values[X];
        L := GetLine(S);
        R := Copy(L, Pos('#', L) );
        FData.Add('<div class="doc-index-group">');
        FData.Add('<span class="doc-index-title">');
        FData.Add('<a href="' + L + '">' + N +
           WhenTrue(LineCount(S) = 1, ' <span class="doc-index-idx">{' + R + '}</span>') +
          '</a></span>');
         if LineCount(S) > 1 then begin
            H := '<span class="doc-index-more">';
            While S <> '' do begin
              L := PopLine(S);
              R := Copy(L, Pos('#', L) );
              H := H + '<span class="doc-index-idx"><a href="' + L + '">' +
               '{' + R + '}</a></span>';
            end;
            FData.Add(H);
            FData.Add('</span>');
         end;
         FData.Add('</div>');
      end;
      FData.Add('</div>');
      FTitle := FIndexer.Names[I];
      FClass := 'idx-' + WebSafeName(FIndexer.Names[I]);
      TagAll;
      SimpleHTML('zindex', @PDUnchanged, False);
      TD.Clear;
  end;

  CheckDirectory(FOutDir, 'zindex');
  for I := 0 to FIndexer.Count - 1 do begin
    TD.Add(FIndexer.Names[I] + ' (' + IntToStr(LineCount(FIndexer.Values[I]) - 1) + ')',
      'idx-' + WebSafeName(FIndexer.Names[I]) + HTMLExt );
  end;
  MakeBrowseBy(TD, 'zindex');
  FreeAndNil(TD);
end;

procedure TRBILParser.MakeByInterrupt;
var
   P, S, V, SD, A, B, C, D, IX: String;
   X, Y, N : integer;

   procedure MakeIntList(ID : String);
   begin
     if (FData.Count = 0) or (ID = '') then Exit;
       Inc(FSecCount);
     FClass := 'index_' + ID;
     FAltTitle := FPreTitle + 'Browse Interrupt 0x' + Uppercase(ID);
     FTitle := 'INT ' + Uppercase(ID);
     FData.Insert(0, FClass);
     FData.Insert(1,'<div class="index-ints">');
     FData.Add('</div>');
     TagAll;
     SimpleHTML(SD, @PDUnchanged, false);
     FData.Clear;
   end;

begin
  FFileName := 'null';
  FSecLine := -1;
  FSection := '';
  FAltTitle := '';

  Message(1, 'Creating Browse by Interrupt');
  Indent;
  SD := IncludeTrailingPathDelimiter('zint');
  P := CheckDirectory(FOutDir, SD);
  if P = '' then raise Exception.Create('unable to open directory');
  S := '';
  FData.Clear;
  for X := 0 to FInts.Count - 1 do begin
    C := FInts.Values[X];
    A := Popline(C);
    N := Pos('interrup' + DirectorySeparator, A);
    if N < 1 then continue;
    if (N = 2) and (A[1] <> DirectorySeparator) then
      Continue
    else if (N > 2) and (Copy(A, 1, N - 1) <> FOutDir) then begin
      WriteLn('Oops: ', A);
      Halt;
    end;
    V := ExtractFileName(A);
    N := Pos('.', A);
    if N > 0 then
      SetLength(V, N - 1);
    B := PopLine(C);
    IX := lowercase(Copy(V, 1, 2));
    if IX < S then begin
      WriteLn('Error: rollback from ' + S + ' to ' + IX);
      halt;
    end else if IX > S then begin
      MakeIntList(S);
    end;
    S := IX;
    if not FileExists(FOutDir + A) then Continue;
    Inc(FGoodLinks);
    A := MakeRelative(SD + FCLASS + HTMLExt, A + WhenTrue(C, '#' + C));
    D := GetIntFuncData(C);
    // WriteLn ('++', C, ' -> ', D);
    if D <> '' then
      D := '<span class="doc-index-function"> (' + D + ')</span>';
    FData.Add('<span class="doc-index-group">' +
      '<span class="doc-index-ints"><a href="' + A + '" id="int-"' +
      FInts.Names[X] + '">' + B + D + '</a></span>' +
      '</span>');
    // WriteLn(FInts.Names[X], ' - ', S);
  end;
  MakeIntList(S);

  Inc(FSecCount);
  FTitle := 'Browse by Interrupt';
  FClass := 'index';
  FAltTitle := '';

  FData.Clear;
  FData.Add(FClass);
  FData.Add('<span class="heading-note">Interrupt Table<br></span>');
  FData.Add('<span class="heading-note"><br></span>');
  FData.Add('<div class="table-outer>"><div class="table-inner">');
  FData.Add('<div class="tabledoc"><table class"intnum-table"');
  for Y := 0 to 15 do begin
    S := '<tr class="intnum-row">';
    for x := 0 to 15 do begin
      N := Y * 16 + X;
      V := HexStr(N, 2);
      if FileExists(P + 'index_' + lowercase(V) + HTMLExt) then begin
        V := '<a href="' + 'index_' + lowercase(V) + HTMLExt + '">' + V + '</a>';
        Inc(FNavLinks);
      end;
      S := S + '<td class="intnum-col"><div class="intnum">' +
         V + '</div></td>' + CRLF ;
    end;
    S := S + '</tr>';
    FData.Add(S);
  end;
  FData.Add('</table></div>');
  FData.Add('</div></div>');

  SimpleHTML(SD, @PDUnchanged, false);

  OutDent;
end;

procedure TRBILParser.MakeByTitle;
var
  TD : TDictionary;
  // N,
  U, T, S, TX, TT, TM, TS : String;
  I, TV : integer;
begin
  Inc(FSecCount);
  TD := TDictionary.Create;
  for I := 0 to FInts.Count - 1 do
    begin
      // N := FInts.Names[I];
      S := FInts.Values[I];
      U := PopLine(S);
      T := PopLine(S);
      TS := GetIntFuncData(PopLine(S));
      TX := '';
      While Pos(' - ', T) > 0 do begin
        TT := Trim(Copy(T, Pos(' - ', T) + 2));
        TM := Trim(Copy(T, 1, Pos(' - ', T)));
        TV := Pos(' - ', TT);
        if TV < 1 then TV := Length(TT) + 1;
        if InRange(Pos('"', TT), 1, TV) then Break;
        if InRange(Pos(TextToHTML.UTF.DoubleQuote, TT), 1, TV) then Break;
        if InRange(Pos('?', TT), 1, TV) then Break;
        TX := WhenTrue(TX, TX + ', ') + TM;
        T := TT;
      end;
      if TS <> '' then
        TX := WhenTrue(TX, TX + ', ') + TS;
      TX := T + ' <span class="doc-index-title-id">(' + TX + ')</span>';
      {$IFOPT D+}
      TM := U;
      if Pos('#', U) > 0 then
        SetLength(TM, Pos('#', TM) -1 );
      if FileExists(FOutDir + TM) then
      {$ENDIF}
      TD.Add(TX, MakeRelative('/ztitle/index.html',
        DirectorySeparator + U + WhenTrue(S, '#' + S)));
      { else
        WriteLn(U); }
    end;

  MakeBrowseBy(TD, 'ztitle');
  FreeAndNil(TD);

end;

procedure TRBILParser.MakeBrowseBy(ALinks : TDictionary; ASubDir : String);
const
  MaxPages = 26;
var
  TC, S, G : String;
  I, IX, PP, NL, LC, CP : integer;
  LF : boolean;
  ATitle : String;

  function TitleNavBar(Page : Integer = -1) : string;
  var
     I, CV : integer;
     TN : String;
  begin
    CV := 0;
    NL := 0;
    Result := '';
    for I := 0 to ALinks.Count -1 do begin
      TN := Uppercase(Copy(ALinks.Names[I], 1, 1));
      if InRange(TN, 'A', 'Z') and (Ord(TN[1]) > CV) then begin
         CV := Ord(TN[1]);
         Result := Result + '<span class="title-jump-' +
           WhenTrue(I div PP = Page, 'this', 'other') +
           '"><a href="index' +
           WhenTrue(I div PP > 0, '_' + lowercase(HexStr(I div PP, 2))) +
           HTMLExt + '#first' + TN[1] +'">&#x' +
           HexStr(Navigate_AplhaSet - $41 + CV, 5) + ';</a></span>';
         Inc(NL);
      end;
    end;
    Result := '<div class="title-jump-bar-outer"><div class="title-jump-bar-inner"><div class="title-jump-bar">' + Result + '</div></div></div>';
  end;

  function PrevLink(Page : Integer = -1) : string;
  begin
    if Page < 1 then begin
      Result := '';
      Exit;
    end;
    Result := '<div class="nav-prev-outer"><div class="nav-prev-inner"><div class="nav-prev">' +
      '<a href="index' + WhenTrue(Page > 1, '_' +
      Lowercase(HexStr(Page - 1, 2))) + HTMLExt +'">' + PreviousPageLink +
      '</a></div></div></div>';
  end;

  function NextLink(Page : Integer = -1) : string;
  begin
    if (Page + 1) * PP > ALinks.Count then begin
      Result := '';
      Exit;
    end;
    Result := '<div class="nav-next-outer"><div class="nav-next-inner"><div class="nav-next">' +
    '<a href="index_' + Lowercase(HexStr(Page + 1, 2)) +
      HTMLExt +'">' + NextPageLink + '</a></div></div></div>';
  end;


begin
  FFileName := 'null';
  FSecLine := -1;
  FSection := '';
  ATitle := '?';
  for I := Low(RBIL_Groups) to High(RBIL_Groups) do
    if RBIL_Groups[I][0] = ASubDir then
      ATitle := RBIL_Groups[I][1];
  Message(1,'Create ' + ATitle);
  indent;
  CheckDirectory(FOutDir, ASubDir);
  ASubDir := IncludeTrailingPathDelimiter(ASubDir);

  PP := ALinks.Count div MaxPages;
  While (PP * MaxPages) < ALinks.Count do Inc(PP);
  // PP := ((PP div 5) + 1) * 5;  // Round them up? IDK.
  if PP < 50 then PP := 50;

  I := 0;
  while I < ALinks.Count - 1 do begin
    FTitle := ATitle;
    FClass := 'index';
    if I div PP > 0 then begin
        FTitle := 'Page ' + IntToStr((I div PP) + 1);
        FAltTitle := WhenTrue(FPreTitle, FPreTitle + ' - ') +
          ATitle + ', ' + FTitle;
        FClass := FClass + '_' + lowercase(HexStr(I div PP, 2));
    end;
    FData.Clear;
    FData.Add(ASubDir);
    CP := I div PP;
    FData.Add(TitleNavBar(CP));
    Inc(FNavLinks, NL);
    S := PrevLink(CP);
    if S <> '' then begin
      Inc(FNavLinks);
      FData.Add(S);
    end;
    FData.Add('<div class="index-titles">');
    IX := 0;
    LC := 0;
    while (IX < PP) and (I + IX < ALinks.Count - 1) do begin
        TC := Uppercase(Copy(ALinks.Names[I+IX], 1, 1));
        LF := InRange(TC, 'A', 'Z') and (Ord(TC[1]) > LC);
        if LF then LC := Ord(TC[1]);
        if ALinks.Values[I+ix] = '' then
          G := '<span class="doc-index-text"' +
          WhenTrue(LF, ' id="first' + TC[1] + '"') +
         '>' + ALinks.Names[I+IX] + '</span>'
        else
          G := '<span class="doc-index-title"' +
          WhenTrue(LF, ' id="first' + TC[1] + '"') +
          '><a href="' + ALinks.Values[I+ix] +
            '">' + ALinks.Names[I+IX] + '</a></span>';
        Fdata.Add('<span class="doc-index-group">' + G + '</span>');
        Inc(IX);
    end;
    Inc(FGoodLinks, IX);
    Inc(I, IX);
    FData.Add('</div>');
    S := NextLink(CP);
    if S <> '' then begin
      Inc(FNavLinks);
      FData.Add(S);
    end;
    FData.Add(TitleNavBar(CP));
    Inc(FNavLinks, NL);
    TagAll;
    SimpleHTML(ASubDir, @PDUnchanged, false);

  end;
  outdent;
  FAltTitle := '';
end;

procedure TRBILParser.OutputIndexWords;
var
  F : Text;
  I : integer;
  AFIleName : String;
begin
  SimplifyIndex;
  if Not FSaveIndex then exit;
  AFileName := 'index-words-level' + IntToStr(FIndexLevel) + WhenTrue(FIndexLower, '-plus') +'.txt';
  Message(1, 'Create Index Word List: ' + AFilename);
  System.Assign(F, AFileName);
  rewrite(F);
  for I := 0 to FIndexer.Count - 1 do
    WriteLn(F, FIndexer.Names[I]);
  Close(F);

end;

function TRBILParser.OnTagRefInt(ARel, AStr: String): String;
const
  { based on when this is called, some of these won't actually ever be present }
  WordBreak = SPACE + TAB + ':;/<>"[]{}()';
  Term = 'INT' + SPACE;
var
  P, E, M, I, R : integer;
  V, F, X, L, C, O : String;
begin
  P := 1;
  while P > 0 do begin
    P := Pos(Term, AStr, P);
    if (P = 1) or ((P > 1) and (Pos(AStr[P-1], WordBreak) > 0)) then begin
       E := P + Length(Term);
       // Next must be hex number
       V := Trim(GetWord(AStr, 1, E));
       if (Length(V) < 4) and (isHex(V) or isHex(V + 'h')) then begin
         E := Pos(V, AStr, E) + Length(V);
         V := ExcludeTrailing('H', V, false);
       end
       else begin
         Inc(P);
         Continue;
       end;
       // Next may be function + Hex Function number
       F := Trim(GetWord(AStr, 1, E + 1));
       if (Uppercase(F) = 'FUNCTION') or (Uppercase(F) = 'FUNCTIONS') or
        (F = 'AX') or (F = 'AH') then begin
         M := Pos(F, AStr, E) + Length(F);
         X := Trim(Getword(AStr, 1, M + 1));
         if isHex(X) or isHex(X + 'h') then begin
            E := Pos(X, AStr, M) + Length(X);
            X := ExcludeTrailing('H', X, false);
         end
         else begin
            F := '';
            X := '';
         end;
       end else if (Copy(AStr, E, 1) = '/') and (isHex(F) or isHex(F + 'h')) then begin
         indent;
         Message(2, 'vague extension reference "' + TERM + V + '/' + F + '", truncating');
         F := '';
         outdent;
       end;
         M := 0;
         R := -1;
         L := Lowercase(V + X); // INT has no leading character
         for I := 0 to FInts.Count - 1 do
           if FInts.Names[I] = L then
             R := I
           else if HasLeading(L, FInts.Names[I]) then
             Inc(M);
         // M > 0 More than One Possible
         // M = 0 No Alternates found
         // R = -1 Not Found at all
         // R >= 0 Found Exact (unless M = 0 could be incorrect)
         if (M > 0) or (R = -1) then begin
           // Point to Browse by Index INT # page
           C := 'ref-idx';
           L := DirectorySeparator + 'zint' + DirectorySeparator + 'index_' +
             Lowercase(V) + HTMLExt;
         end else begin
           // Point to only possible match
           C := 'ref-int';
           L := IncludeLeading(DirectorySeparator, GetLine(FInts.Values[R]));
         end;

         O :=Copy(AStr, P, E - P);
         L := '<span class="' + C + '"><a href="' + MakeRelative(ARel, L) + '">'+
           O + '</a></span>';
         Inc(FGoodLinks);
         Insert(L, AStr, P);

         Inc(P, Length(L));
         Delete(AStr, P, Length(O));
    end;
    if P > 0 then Inc(P);
  end;
  Result := AStr;
end;

procedure TRBILParser.PostRun;
{$IFOPT D+}
var
  T : Text;
  I : integer;
{$ENDIF}
begin
  Message(1, '');
  Message(1, 'General statistics:');
  Indent;
  Message(1,IntToStr(FSecCount) + ' total sections');
  Message(1, IntToStr(FIndexer.Count)+ ' index terms');
  OutDent;
  Message(1, '');
  Message(1, 'HTML statistics:');
  Indent;
  Message(1,IntToStr(FPages) + ' total pages');
  Message(1,IntToStr(FInts.Count) + ' targets');
  Message(1,IntToStr(FTables.Count) + ' tables');
  Message(1,IntToStr(FNavLinks) + ' navigation links');
  Message(1,IntToStr(FGoodLinks) + ' inter-links');
  Message(1,IntToStr(FBadLinks) + ' ' +
    WhenTrue( FGoodLinks + FBadLinks > 0,
      FormatFloat('0.00', (FBadLinks * 100 / (FGoodLinks + FBadLinks))),
      '0.00')+
    '% unresolved');
  Message(1, IntToStr(FIdxCnt) + ' index links');
  OutDent;
  Message(1, '');
  {$IFOPT D+}
  exit;
  System.Assign(T, FOutDir + 'out.txt');
  rewrite(T);
  for I := 0 to FDocTitles.Count - 1 do
    WriteLn(T, FDocTitles.Names[I], ' --> ', FDocTitles.Values[I]);
   Close(T);
   {$ENDIF}
end;

procedure TRBILParser.SetIntFuncData;
var
  I, L, P : integer;
  S, T, C, V : String;
begin
  I := 1;
  While (I < FData.Count) and (Trim(FData[I]) = '') do Inc(I);
  L := I + 1;
  S := '';
  While (L < FData.Count) and (LTrim(FData[L]) <> FData[L]) do begin
    T := Trim(FData[L]);
    Inc(L);
    if Pos('RETURN', Uppercase(T)) > 0 then Break;
    P := Pos('=', T);
    if P < 1 then Continue;
    C := Trim(Copy(T, 1, P - 1));
    T := Trim(Copy(T, P + 1));
    if (Length(C) > 2) then begin
      if (Length(C) <> 5) or (Copy(C, 3, 1) <> ':') then
        Continue;
    end;
    V := '';
    while T <> '' do begin
      V := Trim(PopWord(T));
      if isHex(V) then
        break;
    end;
    if IsHex(V) then begin
      S := WhenTrue(S,S + '/') + C + '=' + V;
    end;
  end;
  if S = '' then Exit;
  FSubFunc:=S;
  // WriteLn(FSecCount, ':(', S, ') ', FData[I]);
  FIntFunc.Add(IntToStr(FSecCount), S);
end;

function TRBILParser.GetIntFuncData(ASection : string = ''): String;
var
  X : integer;
begin
  if ASection = '' then ASection := IntToStr(FSecCount);
  X := FIntFunc.Find(ASection);
  if X >= 0 then
    Result := FIntFunc.Values[X]
  else
    Result := '';
end;

end.

