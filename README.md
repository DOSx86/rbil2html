# RBIL2HTML

Lazarus project to convert Ralf Brown's Interrupt List to HTML.

The resulting CSS/HTML is static and portable. It can be uploaded to any website
directory, burned to CD-ROM or accessed locally.

The conversion program was developed piece by piece in the quickest and easiest
ways possible. It is neither fast nor very efficient. The conversion process is
slow and extremely resource intensive. The indexing process is especially
demanding. The default indexing level of 2 can take well over an hour for
conversion. Running at level 3 or higher demands so much RAM and CPU usage,
your OS may assume it is a "runaway process" and kill the conversion program
without warning.

